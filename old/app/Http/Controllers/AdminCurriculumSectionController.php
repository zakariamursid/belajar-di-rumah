<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminCurriculumSectionController extends CBController {


    public function cbInit()
    {


        $this->setTable("curriculum_section");
        $this->setPermalink("curriculum_section");
        $this->setPageTitle("Curriculum Section");

        $this->addSelectTable("Curriculum","curriculum_id",["table"=>"curriculum","value_option"=>"id","display_option"=>"title","sql_condition"=>"course_id = '".request()->segment(3)."' "]);
		$this->addText("Title","title")->strLimit(150)->maxLength(255);
		$this->addFile("Video","video")->required(false)->encrypt(true);
		$this->addTextArea("Desc","desc")->required(false)->strLimit(150);
		$this->addSelectOption("Access","access")->options(['PRIVATE'=>'PRIVATE','PUBLIC'=>'PUBLIC']);
		$this->addSelectOption("Permission","permission")->options(['WAITING ADMIN APPROVAL'=>'WAITING ADMIN APPROVAL','APPROVED'=>'APPROVED','REJECTED'=>'REJECTED','REPORTED'=>'REPORTED']);
		$this->addNumber("Short Number","sort_number")->required(false);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

    }
}
