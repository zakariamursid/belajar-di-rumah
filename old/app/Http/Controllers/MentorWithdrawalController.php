<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use App\Model\ModelCurriculumSection;
use App\Model\ModelWallet;
use App\Model\ModelWithdraw;
use Illuminate\Http\Request;

class MentorWithdrawalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $auth =  Auth::user();
        if($auth == null)
        {   
            return redirect('user/login');            
        }
        else {
             $debit = 0; // this is store all sum value so first assign 0
        $kredit = 0; // this is store all sum value so first assign 0
        $data = [];
        $balance = ModelWallet::getByUser(Auth::user()->id);
        foreach ($balance as $value) {
            // echo $value->id;
            if($value->type == "DEBIT")
            {
                $debit += $value->balance;             
            }
            else if($value->type == "KREDIT")
            {
                $kredit += $value->balance;             
            }
            
            // $final = $plus-$minus;
        }
        // echo $final;
        // dd($debit-$kredit);
        $data['withdraw'] = ModelWithdraw::getByUsersId(Auth::user()->id);
        $data['pending'] = ModelWithdraw::getByUsersIdWherePending(Auth::user()->id);
        // dd($data);
        $data['balance'] = $debit-$kredit;
        $data['page_title'] = "Withdraw";
        $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
        $data['bank'] = DB::table('bank')->get();
        return view('mentor.withdraw', $data);
                // return view(getThemePath('mentor.withdraw'), $data);

            
        }

       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function request(Request $request)
    {
        // dd($request);
        $save = new ModelWithdraw;
        $save->setUsers(Auth::user()->id);
        $save->setBank($request->bank);
        $save->setBranch($request->branch);
        $save->setAmount($request->amount);
        $save->setAccountName($request->account_name);
        $save->setAccountNumber($request->account_number);
        $save->add();
        
        return redirect('/user/withdraw');



    }    
}
