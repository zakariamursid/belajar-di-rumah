<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use App\Model\ModelPaymentOptions;

class AdminPaymentOptionsController extends CBController {


    public function cbInit()
    {
        $this->setTable("payment_options");
        $this->setPermalink("payment_options");
        $this->setPageTitle("Payment Options");

        $this->addSelectTable("Bank","bank_id",["table"=>"bank","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addText("Account Name","account_name")->strLimit(150)->maxLength(255);
		$this->addText("Account Number","account_number")->strLimit(150)->maxLength(255);
		$this->addText("Branch","branch")->strLimit(150)->maxLength(255);
		$this->addSelectOption("Status","status")->options(['Primary'=>'Primary','Non Primary'=>'Non Primary']);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
    
        $this->hookAfterUpdate(function($id) {
        redirect("/user/payment_options/".$id ."/update")->send();
        exit;
    });

    }

    public function update($id)
    {
        $check = ModelPaymentOptions::checkById($id);
        if($check->status == "Primary")
        {
            ModelPaymentOptions::updateStatus($id);
            return redirect("user/payment_options");
        }
        else {
            return redirect("user/payment_options");            
        }
    }
    
}
