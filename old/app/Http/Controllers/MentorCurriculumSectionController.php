<?php

namespace App\Http\Controllers;
use App\Model\ModelCurriculum;
use App\Model\ModelCourse;
use App\Model\ModelCurriculumSection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\Web;
use Storage;

class MentorCurriculumSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          	$save = new ModelCurriculumSection;
		$save->setCurriculum($request->section_curriculum);
		$save->setTitle($request->section_title);
		$save->setDesc($request->section_desc);
        $save->setAccess($request->section_access);
        // if ($request->hasFile("video")) {
        //     $save->setVideo(Web::UploadFile("video","video"));
        // }        
		// $save->setPermission($request->permission);
        $save->add();
        
        return response()->json(['success' => 'Data Added successfully.', 'data' => $request->curriculum]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
    * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          if(request()->ajax())
        {
            $data = ModelCurriculumSection::getBy("id",$id);
            return response()->json(['data' => $data]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

            
            $save = new ModelCurriculumSection;
            $save->setTitle($request->section_title);
            $save->setCurriculum($request->section_curriculum);
            $save->setAccess($request->section_access);
            $save->setDesc($request->section_desc);
            // $save->setVideo($request->video);
            if ($request->hasFile("video")) {
                Storage::delete($request->old_video);
                $save->setVideo(Web::UploadFile("video","video"));
            }                    
            $save->edit("id",$request->section_id);

        //    @dd($save);
            return response()->json(['success' => 'Data Added successfully.']);

    }
    public function destroyVideo($id)
    {
        
       $delete = ModelCurriculumSection::getById($id);
       Storage::delete($delete->video);
    $affected = DB::table('curriculum_section')
              ->where('id', $delete->id)
              ->update(['video' => NULL]);

    

        //    @dd($save);
            return response()->json(['success' => "Updated"]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = ModelCurriculumSection::getById($id);
        Storage::delete($delete->video);

       $del = ModelCurriculumSection::remove("id",$id);
        return response()->json(['success' => $delete]);        
    }

    public function sortContent(Request $request)
    {
        // $tasks = DemoTask::all();

        // foreach ($tasks as $task) {
        //     $task->timestamps = false; // To disable update_at field updation
        //     $id = $task->id;

        //     foreach ($request->order as $order) {
        //         if ($order['id'] == $id) {
        //             $task->update(['order' => $order['position']]);
        //         }
        //     }
        // }
        
        // return response('Update Successfully.', $request->order);
        return response('Update Successfully.', 'Jaka');
        // return response('Update Successfully.', 200);
    }
}
