<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminCurriculumController extends CBController {


    public function cbInit()
    {
        $this->setTable("curriculum");
        $this->setPermalink("curriculum");
        $this->setPageTitle("Curriculum");

        $this->addSelectTable("Course","course_id",["table"=>"course","value_option"=>"id","display_option"=>"title","sql_condition"=>""]);
		$this->addText("Title","title")->strLimit(150)->maxLength(255);
		$this->addNumber("Short Number","short_number")->required(false);
		$this->addSelectOption("Access","access")->options(['PRIVATE'=>'PRIVATE','PUBLIC'=>'PUBLIC']);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

    }
}
