<?php

namespace App\Http\Controllers;
use App\Model\ModelPayments;
use App\Model\ModelBank;
use App\Model\ModelCourse;
use App\Model\ModelOrders;
use App\Model\ModelAdminWallet;
use App\Model\ModelCommission;
use App\Model\ModelField;
use App\Model\ModelFAQ;
use App\Model\ModelCoupon;
use App\Model\ModelSettings;
use App\Model\ModelTags;
use App\Model\ModelUsers;
use App\Model\ModelVisitor;
use App\Model\ModelPaymentOptions;
use App\Model\ModelTncPengajar;
use App\Model\ModelCurriculum;
use App\Model\ModelWallet;
use App\Model\ModelCurriculumSection;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Helpers\Web;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class PublicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // dd($data);

        $data = [];   


        $data['topic'] = ModelField::get();
        $data['course'] = ModelCourse::getWithMentorForIndex();        
        $data['top_course'] = ModelCourse::getTopCourseForIndex();        
        // dd($data);

        $auth =  Auth::user();
        if($auth != null)
        {   
            $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
        }
    
        // $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
  
        return view('public.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $auth =  Auth::user();
        $data['session'] = null;
        $data['course'] = ModelCourse::getWithMentorBy("course.id",$id);
        // $data['count'] = ModelOrders::countOrderSuccess( $data['course']->id);


        $uniqueHashUser = md5(request()->ip() . request()->userAgent() );
        //  echo $mac;  
        $cek = ModelVisitor::cek($uniqueHashUser,$id);
        if($cek == null)
        {
            $visitor = new ModelVisitor();
            $visitor->setUniqueHash($uniqueHashUser);
            $visitor->setCourse($id);
            $visitor->add();
        }
        

        if($auth != null)
        {   
        $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
        $data['order'] = ModelOrders::cekOrderSuccess( $data['course']->id,Auth::user()->id);
         }
         $data['faq'] = ModelFAQ::get();
        //  dd($data);
        $data['count_visitor'] = ModelVisitor::countBy("course_id",$id);
        $data['page_title '] = "Detail Course";
        $tags = ModelTags::getBy("course_id",$id)->toArray();
        $data['simmiliar_course'] = [];

        if($tags != null || (!empty($tags)) || $tags != '')
        {

        foreach ($tags as $key => $value)
            {
                $tags_topic_id [] = $value->topic_id;
            }
            if(!empty($tags_topic_id))
            {
            $topic_id = $tags_topic_id[array_rand($tags_topic_id)];
            $data['simmiliar_course'] = ModelTags::getRecomendation($topic_id,$id);                
            }

        }

        // $tags_id = $tags->id;


        // echo $tags_topic_id[array_rand($tags_topic_id)];

        // dd($data);
        // $data['simmiliar_course'] = ModelCourse::getSimmiliarCourse($data['course']->id,$data['course']->topic_id);
        // $data['content'] = ModelCurriculum::with('section')->get();
        
        $data['content'] = ModelCurriculum::getByCourseId($data['course']->id);        
        $data['course_also'] = DB::table('course')
        ->select('course.*', 'users.id as user_id','users.name as user_name')
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })
        ->get();
        // $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
        // return view('test', $data);
        // dd($data);
        return view('public.course.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function checkout($id)
    {
        $data = [];
        $data['unique_code'] = rand(100,999);
        $kupon =
            [
                "code" => NULL,
                "discount" => NULL,
            ];
        $data['coupon'] =  (object)$kupon;

        if(isset($_GET['coupon'])) {
          $coupon = ModelCoupon::check($_GET['coupon'], $id);
        
        $date_now = Web::DateNow();
        $date_now = $date_now->format('Y-m-d'); // for example

        // if($coupon->valid_until < $date_now)
        // {
        //     echo 'Tidak Valid';
        // }
        // else
        // {
        //     echo 'Valid';
        // }

        // dd('d');

            if($coupon != null)
            {
                if($coupon->coupon_left == 0)
                {
                    $kupon =
                    [
                        "code" => $_GET['coupon'],
                        "discount" => "Jumlah kupon telah habis digunakan",
                    ];
                    $data['coupon'] = (object)$kupon;                
                }

                else if($coupon->valid_until < $date_now)
                {
                    $kupon =
                    [
                        "code" => $_GET['coupon'],
                        "discount" => "Masa Berlaku Kupon telah Habis",
                    ];
                    $data['coupon'] = (object)$kupon;    
                }
                else if($coupon->valid_until > $date_now && $coupon->coupon_left >= 1)
                {

                    if($coupon->status == 'Aktif')
                    {
                        $data['coupon'] = $coupon;

                        if($data['coupon']->discount == 100 && $data['coupon']->type == '%' && $coupon->status == 'Aktif')
                            {
                                $data['unique_code'] = 0;
                            }   
                    }

                    else if($coupon->status == 'Tidak Aktif')
                    {
                        $kupon =
                        [
                            "code" => $_GET['coupon'],
                            "discount" => "Tidak Aktif",
                        ];
                        $data['coupon'] = (object)$kupon;  
                    }
                 
                }

            }    
            else if($coupon == null)
            {
                $kupon =
                [
                    "code" => $_GET['coupon'],
                    "discount" => "NOTFOUND",
                ];
                $data['coupon'] = (object)$kupon;
            }    
        
        }

        // dd($data['coupon']->discount);
        // $auth =  Auth::user();
        if(cb()->session()->id() != null)
        {    

        $data['session'] = DB::table('users')->where('id', cb()->session()->id())->first();
        // dd($data);
        // dd($data);

        $data['course'] = ModelCourse::getWithMentorBy("course.id",$id);
        return view('public.course.checkout', $data);
        // return view('course.checkout', compact('data'));        

        }
        else {
            return redirect('user/login/');
        }
    }    

    public function placeorder(Request $request, $id)
    {
        $check = ModelOrders::checkDuplicate($request->course_id, cb()->session()->id());
        if(!empty($check))
        {
            if($check->status == "Pending" && date("Y-m-d H:i:s") < $check->void_at)
            {
                // dd("jaka");
                return redirect('/order');
            }
        }

        // dd($request->coupon);


        $save_order = new ModelOrders;
        $save_order->setCourse($request->course_id);
        $save_order->setBuyer(Auth::user()->id);
        $course = ModelCourse::getById($request->course_id);
        $save_order->setCoursePrice($course->price);

        $coupon_is_valid = ModelCoupon::findBy("code",$request->coupon);
        $grand_total = $request->price;

        if($coupon_is_valid != null)
        {
            $save_order->setCoupon($coupon_is_valid->code);
                if($coupon_is_valid->type == "%")
                {
                    $discount = $course->price * ($coupon_is_valid->discount/100);
                }
                else if($coupon->type == "-")
                {  
                    $discount = $coupon_is_valid->discount;
                }      
            $save_order->setCouponDiscount($discount);
        }


        $save_order->setStatus("Pending");
        $save_order->setGrandTotal($grand_total);
        $save_order->add();
        $latest_order = ModelOrders::getLatestId();
        // dd($latest_order);
        $course = ModelCourse::getById($latest_order->course_id);

        if($latest_order->grand_total == 0 && $latest_order->status == 'Pending')
        {
            $save_payment = new ModelPayments;
            $save_payment->setBank('Free Course');
            $save_payment->setBranch('Free Course');
            $save_payment->setOrder($latest_order->id);
            $save_payment->setAccountName('System');
            $save_payment->setAccountNumber(0);
            $save_payment->setStatus('Approved');
            $save_payment->setProof('uploads/proof/system.jpg');
            $save_payment->add();

            $update_order = new ModelOrders();
            $update_order->setStatus('Success');
            $update_order->updateStatus($latest_order->id);

            $mutasi_admin = ModelAdminWallet::cek($latest_order->id, "DEBIT");
            $mutasi_mentor = ModelWallet::cek($latest_order->id, "DEBIT");

            if(empty($mutasi_mentor) || $mutasi_mentor == null)
            {
  
            $desc = "Pembelian : ".$course->title." - Menggunakan kupon potongan 100% ";
        
            $wallet = new ModelWallet;
            $wallet->setBalance(0);
            $wallet->setUser($course->user_id);
            $wallet->setOrder($latest_order->id);
            $wallet->setDesc($desc);            
            $wallet->setType("DEBIT");
            $wallet->add();

                

            }


            if(empty($mutasi_admin) || $mutasi_admin == null)
            {

            $desc = "Pembelian : ".$course->title." - Menggunakan kupon potongan 100% ";


            $admin_wallet = new ModelAdminWallet;
            $admin_wallet->setBalance(0);
            $admin_wallet->setDesc($desc);
            $admin_wallet->setType("DEBIT");
            $admin_wallet->setOrder($latest_order->id);
            $admin_wallet->add();


            }                
            // $latest = ModelOrders::getLatestId();

            // dd($latest);
                                // return view('public.order.waiting', $data);                   


            return redirect('/order');
        }
        

        return redirect("/order/".$latest_order->id."/payment");

    }

    public function order($id)
    {
        if(cb()->session()->id() != null)
        {   
            $data['bank'] = ModelBank::get();
            $data['session'] = DB::table('users')->where('id', cb()->session()->id())->first();
            $data['order'] = ModelOrders::getById($id);
            $data['order_detail'] = ModelCourse::getById($data['order']->course_id);
            $check_status = ModelPayments::getBy("order_id",$id);

            if(!empty($check_status))
            {
                dd($check_status);
                if($check_status->status ==  "Pending")
                {
                    $data['payment_status'] = $check_status->status;
                    return view('public.order.waiting', $data);                   
                }
                else if($check_status->status ==  "Approved")
                {
                    return redirect('/couurse/'.$data['order']->course_id.'/watch');
                // return view('public.order.waiting', $data);                   
                }
            }
            
            else
            {
                $data['bank_account'] = ModelPaymentOptions::getPrimary();
                return view('public.course.payment', $data);    
            }
        }
         else
         {
             return redirect('/');
         }        
   
    }

    public function payment($id)
    {
        $data['bank'] = ModelBank::get();
        $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
        $data['order'] = ModelOrders::getById($id);
        $data['order_detail'] = ModelCourse::getById($data['order']->course_id);
        $check_status = ModelPayments::getBy("order_id",$id);
        if(!empty($check_status))
        {
            // dd($check_status);
            if($check_status->status ==  "Pending")
            {
                $data['payment_status'] = $check_status->status;
                return view('public.order.waiting', $data);                   
            }
            if($check_status->status ==  "Approved")
            {
                return redirect('/mycourses/'.$check_status->order_id);
             // return view('public.order.waiting', $data);                   
            }

        }
        

        else
        {

        // dd($data);
         return view('public.course.payment', $data);    
        }
   
    }

    public function order_content($id)
    {
        $data = [];
        $data['order'] = ModelOrders::getById($id);
        $data['content'] = ModelCurriculum::getByCourseId($data['order']->course_id);        
        $data['course'] = ModelCourse::getWithMentorBy("course.id",$data['order']->course_id);        
        // if()
        if(isset($_GET['id'])) {

        $data['section_id'] = $_GET['id'];
        if($data['section_id'] == "")
        {
              $data['section']  = [
                  'video' => $data['course']->teaser,
              ];
            //   $data['section'] = object($section);
            // dd($section);

        }

        else
        {
        $data['section'] = ModelCurriculumSection::getById($data['section_id']);
        }
        // dd($data['section_id']);

        // dd($data);
        // dump($a);
        $data['page_title'] = $data['course']->title;
        return view('public.content', $data);           

        // dd($data);
        }
     

        // dd($data);
   
    }

    public function checkcoupon(Request $request)
    {
        $coupon = ModelCoupon::getBy("code",$request->coupon);

        if($coupon != null)
        {
        return Redirect::route('clients.show, $id')->with( ['data' => $data] );
            // dd($coupon);
        }        
    }


    public function get_be_mentor()
    {
        if($_GET['user_id'] != null)
        {
            $data = [];
            $data['tnc'] = ModelSettings::findBy("name","TNC Pengajar");
            $auth =  Auth::user();
            if($auth != null)
            {   
            $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
            }            
            return view('public.menjadipengajar', $data);
            // echo $_GET['user_id'];
        }


        else {
            return redirect('/');
        }

    }

    public function post_be_mentor(Request $request)
    {
        $upgrade = new ModelUsers;
        $upgrade->updatePrivelege($request->user_id);
        return redirect("user/");
    }
     public function postContinueRegister($token) {
        if($token = Cache::get("register_".$token)) {
            DB::table("users")
                ->insert([
                    "created_at"=>date("Y-m-d H:i:s"),
                    "name"=> $token['name'],
                    "email"=> $token['email'],
                    "password"=>Hash::make($token['password']),
                    "cb_roles_id"=> getSetting("register_as_role")
                ]);

            return cb()->redirect(cb()->getAdminUrl("login"),"Thank you for register. Now you can login to start your session :)","success");
        } else {
            return cb()->redirect(cb()->getAdminUrl("login"),"It looks like the URL has been expired!");
        }
    }
    
    public function landing()
    {

        $data['topic'] = ModelField::get();
        $data['course'] = ModelCourse::getWithMentorForIndex();        
        $data['top_course'] = ModelCourse::getTopCourseForIndex();        
        // dd($data);

        $auth =  Auth::user();
        if($auth != null)
        {   
            $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
        }
    
        // $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
  
        return view('public.landing', $data);
    }


}
