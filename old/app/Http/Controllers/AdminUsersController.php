<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminUsersController extends CBController {


    public function cbInit()
    {
        $this->setTable("users");
        $this->setPermalink("users");
		$this->setPageTitle("Users");
		
  // To modify default index query
    $this->hookIndexQuery(function($query) {
        // Todo: code query here

        // You can make query like laravel db builder
		$query->where("cb_roles_id", "!=" , 1)
		->orderBy("users.created_at", "Desc");

        // Don't forget to return back
        return $query;
	});
	    // Hide delete button with a condition
		$this->setButtonDelete(false);

		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
        $this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addEmail("Email","email");
		$this->addDatetime("Email Verified At","email_verified_at")->showIndex(false)->showEdit(false);
		$this->addText("Password","password")->showIndex(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addText("Remember Token","remember_token")->showIndex(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addDatetime("Updated At","updated_at")->showIndex(false)->showEdit(false)->required(false)->showAdd(false)->showEdit(false);
		$this->addImage("Photo","photo")->required(false)->showIndex(false)->encrypt(true);
		$this->addSelectTable("Role","cb_roles_id",["table"=>"cb_roles","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addText("Ip Address","ip_address")->showIndex(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addText("User Agent","user_agent")->showIndex(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addDatetime("Login At","login_at")->showIndex(false)->showEdit(false);
		
$this->button_edit = false;


    }
}
