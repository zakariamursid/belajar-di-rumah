<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use crocodicstudio\crudbooster\controllers\partials\ButtonColor;
use App\Model\ModelCourse;
use App\Model\ModelTopic;
use App\Model\ModelTags;
use App\Model\ModelSettings;
use Auth;
use App\Helpers\Web;
use DateTime;
use Storage;
use Image;

class AdminCourseController extends CBController {


    public function cbInit()
    {

 // To modify default index query
    $this->hookIndexQuery(function($query) {
        // Todo: code query here

        // You can make query like laravel db builder
        $query->where("mentor_id", cb()->session()->id());

        // Don't forget to return back
        return $query;
    });


 // To inject user's post data on before insert action
    $this->hookBeforeInsert(function($data) {
        // Todo: code here
  
	    $data['status'] = "Pending";

        // Don't forget to return back
        return $data;
	});


        $this->setTable("course");
        $this->setPermalink("course");
        $this->setPageTitle("Course");

        $this->addText("Title","title")->strLimit(150)->maxLength(255);
        // $this->addText("Status","status")->showIndex(true);
        // $this->addSelectOption("Level","level")->options(['Beginner'=>'Beginner','Intermediate'=>'Intermediate','Expert'=>'Expert']);
      
		// $this->addSelectTable("Topic","topic_id",["table"=>"topic","value_option"=>"id","display_option"=>"name","sql_condition"=>""])->showAdd(false);
        // $this->addImage("Poster (.jpg - Aspect Ratio 16:9)","poster")->resize(4);
            $this->addImage("Poster","poster");

		$this->addFile("Teaser (.mp4 - Max 1 Menit)","teaser")->showIndex(false)->encrypt(false);
		// $this->addTextArea("Short Desc (100 Karakter)","short_desc")->strLimit(100)->showIndex(false);
        // $this->addTextArea("Desc (500 Karakter)","desc")->strLimit(500)->showIndex(false);
        $this->addTextArea("Short Desc","short_desc")->strLimit(100)->showIndex(false);

		$this->addWysiwyg("Desc","desc")->strLimit(500)->showIndex(false);
        
        $this->addMoney("Price (Rp.)","price");
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false)->showIndex(false);
        // $this->addSelectTable("Mentor","mentor_id",["table"=>"users","value_option"=>"id","display_option"=>"name","sql_condition"=>"cb_roles_id=2"])->showAdd(false)->showIndex(false);		  
        // $this->addCustom("Mentord Id")->setHtml(
        //         "<input type='text' hidden value='".cb()->session()->id()."' name='mentor_id'/>"
        //     )->showAdd(true);

        $this->addHidden("Mentor","mentor_id")->defaultValue(cb()->session()->id())->showIndex(false);
//  $this->addActionButton("Set Status Success", function($row) {
// 		    return action("FooBarController@getChangeSuccess",["id"=>$row->primary_key]);
//         }, function($row) {
// 		    return $row->status == "Waiting";
//         }, "fa fa-pencil", ButtonColor::LIGHT_BLUE, true);
$this->addActionButton("Content",function($row) {
  return url("user/course/".$row->primary_key."/content");
}, function($row) { return true;}, "fa fa-tasks", ButtonColor::YELLOW, false);


$this->addActionButton("Kupon",function($row) {
  return url("user/course/".$row->primary_key."/coupon");
}, function($row) { return true;}, "fa fa-money", ButtonColor::LIGHT_BLUE, false);

    }

    public function postAdd(Request $request)
    {
      
        $valid = [
            "title" => "required",
            "level" => "required|numeric",
            "topic" => "required",
            "price" => "required|numeric",
            "short_desc" => "required",
            "teaser" => "required",
            "desc" => "required",
            "poster" => "required|image",
        ];   

        $message = [
            "title.required" => "Title tidak boleh kosong",
            "level.required" => "Level tidak boleh kosong",
            "topic.required" => "Topic tidak boleh kosong",
            "price.required" => "Price tidak boleh kosong",
            "short_desc.required" => "Short Desc tidak boleh kosong",
            "desc.required" => "Desc tidak boleh kosong",
            "teaser.required" => "teaser tidak boleh kosong",
            "poster.required" => "Poster tidak boleh kosong",
        ];   
        $valid = Validator::make($request->all(),$valid,$message);

        if ($valid->fails()) {
        }
        else
        {
        date_default_timezone_set('Asia/Jakarta');

        $data['created_at'] = Web::DateNow();
        $data['title'] = $request->title;
        $data['level'] = $request->level;
        $data['price'] = $request->price;
        $data['status'] = "Pending";
        $data['short_desc'] = $request->short_desc;
        $data['desc'] = $request->desc;
        $data['mentor_id'] = cb()->session()->id();
        // $data['poster'] = Web::UploadThumbnail("poster", 722, 422);
        $data['teaser'] = Web::UploadFile("teaser","teaser");
        $insert = ModelCourse::store($data);
        // dd($data);
        
        if($insert)
        {
            
        $course = ModelCourse::getIdLatestCourse(cb()->session()->id());

            // dd($request->topic);
        foreach ($request->topic as $key => $value) {
            // echo $value;
            $value = ucfirst($value);
            $cek = ModelTopic::findBy("name",$value);
            if($cek == null)
            {
                $topic = new ModelTopic();
                $topic->setName($value);
                $topic->add();
            }
            $cek = ModelTopic::findBy("name",$value);
            $check = ModelTags::check($course->id,$cek->id);
                if($check == null)
                {
                    $tag = new ModelTags();
                    $tag->setCourse($course->id);
                    $tag->setTopic($cek->id);
                    $tag->add();           
                } 
            }
        

            return redirect('/user/course');
        }
    
        else
        {
            return redirect('/user/course');
        }
                
    }

    
    // $image = $request->file('poster');
    // $nameImage = $request->file('poster')->getClientOriginalName();

    // $thumbImage = Image::make($image->getRealPath())->resize(750, 422);
    // $thumbPath = public_path() . '/images/thum' . $nameImage;
    // $thumbImage = Image::make($thumbImage)->save($thumbPath);

    // $oriPath = public_path() . '/images/ori' . $nameImage;
    // $oriImage = Image::make($image)->save($oriPath);

    // $mouseImage = new Images;
    // $mouseImage->imgname = $nameImage;
    // $mouseImage->normal_imgpath = $oriPath;
    // $mouseImage->thumbnail_imgpath = $thumbPath;
    // $mouseImage->save();
    }

        public function postEdit($id, Request $request)
    {
      
        $valid = [
            "title" => "required",
            "level" => "required|numeric",
            "topic" => "required",
            // "price" => "required|numeric",
            "short_desc" => "required",
            "desc" => "required",
        ];   

        $message = [
            "title.required" => "Title tidak boleh kosong",
            "level.required" => "Level tidak boleh kosong",
            "topic.required" => "Topic tidak boleh kosong",
            "price.required" => "Price tidak boleh kosong",
            "short_desc.required" => "Short Desc tidak boleh kosong",
            "desc.required" => "Desc tidak boleh kosong",
        ];   
        $validator = Validator::make($request->all(),$valid,$message);
 
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else
        {
            $course = new ModelCourse();

            if($request->price == NULL || $request->price == "0")
            {
             $price = 0;
            }

            else
            {
            $price = $request->price;
               
            }


            $course->setTitle($request->title);
            $course->setLevel($request->level);
            $course->setShort_desc($request->short_desc);
            $course->setPrice($price);
            $course->setDesc($request->desc);

            if($request->poster != null || $request->poster != '')
            {
                $course->setPoster(Web::UploadThumbnail("poster", 750, 422));
            }
            if($request->teaser != null || $request->teaser != '')
            {      
                $course->setTeaser(Web::UploadFile("teaser","teaser"));
            }
        
           $update = $course->edit("id",$id);
            // dd($request->price);

    
        if($update)
        {
            


        $tags = ModelTags::getBy("course_id",$id);
        $new_tags = $request->topic;
        $old_tags = [];


        foreach ($tags as $key => $value) {
                    $old_tags [] = $value->topic_id;
        }

        // dd($old_tags);

           $tag['old'] = $old_tags;
           $tag['new'] = $request->topic; 
            
           $delete_this  = array_diff_assoc($old_tags,$new_tags);    
           $insert_this = array_diff_assoc($new_tags,$old_tags); 
                       
           foreach ($delete_this as $key => $value) {
               ModelTags::remove($id,$value);
           }
        //    is_numeric()
        //    dd($tag);


           foreach ($insert_this as $key => $value) {
            $check_topic = ModelTopic::findBy("id",$value);      
                    // dd($check_topic);

            if($check_topic != null)
            {
                    $tag = new ModelTags();
                    $tag->setCourse($id);
                    $tag->setTopic($check_topic->id );
                    $tag->add();   
            }

            else if($check_topic == null)
            {               
                $topic = new ModelTopic();
                $topic->setName($value);
                $topic->add();   
                $check_topic = ModelTopic::findBy("name",$value);                
                $check_tag = ModelTags::check($id,$check_topic->id);
                if($check_tag == null)
                {
                    $tag = new ModelTags();
                    $tag->setCourse($id);
                    $tag->setTopic($check_topic->id);
                    $tag->add();           
                }                  
            }     
               

           }
        

            return redirect('/user/course/edit/'.$id);
        }
    
        else
        {
            return redirect('/user/course/edit/'.$id);
        }
                
    }
}



    public function getAdd()
    {
        $data['page_title'] = "Course";
        $data['topics'] = ModelTopic::get();
        $data['levels'] = ModelSettings::getBy("name","Level");
        // dd($data);
		return view('mentor.course.add', $data);
    }
    public function getEdit($id)
    {
        $data['page_title'] = "Course";
        $data['topics'] = ModelTopic::get();
        $data['tags'] = ModelTags::getBy("course_id",$id);
        $data['levels'] = ModelSettings::getBy("name","Level");
        $data['course'] = ModelCourse::getById($id);
        // dd($data);
		return view('mentor.course.form', $data);
    }
	

        public function all()
        {
            $data = [];
            $data['session'] = Auth::user();
            $data['page_title'] ="All Course";
            $data['course'] = ModelCourse::get();
            // dd($data); 
            
            return view('admin.courses',$data);

        }

        public function index()
        {
            $data = [];
            $data['course'] = ModelCourse::getAllAproved();
            // dd($data);
            return view('public.course.all',$data);
        }

        public function pending()
        {
        $data['session'] = Auth::user();
            $data['page_title'] ="All Course";
        $data['status'] = [
            'Pending','Approved','Rejected'
        ];
            $data['course'] = ModelCourse::getPending();
            // dd($data); 
            
            return view('admin.pendingcourse',$data);

        }

        public function approve($id)
        {
            $update = new ModelCourse;
            $update->setStatus('Approved');
            $update->updateStatus($id);
            return redirect()->back();
        }

        public function takedown($id)
        {
            $update = new ModelCourse;
            $update->setStatus('Takedown');            
            $update->updateStatus($id);
            return redirect()->back();
        }

        public function destroyThumbnail($id)
        {
            $course = ModelCourse::getById($id);
            Storage::delete($course->poster);
            $affected = DB::table('course')
                    ->where('id', $course->id)
                    ->update(['poster' => '']);

            return response()->json(['success' => "Berhasil dihapus"]);            
        }
        public function destroyTeaser($id)
        {
            $course = ModelCourse::getById($id);
            Storage::delete($course->teaser);
            $affected = DB::table('course')
                    ->where('id', $course->id)
                    ->update(['teaser' => '']);

            // return response()->json(['success' => $course->id]);            
            return response()->json(['success' => "Berhasil dihapus"]);            
        }

}
