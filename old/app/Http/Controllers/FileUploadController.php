<?php

namespace App\Http\Controllers;
use DB;
use App\Model\ModelCurriculumSection;
use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    public function fileUpload($id)
    {
        $data = [];
        $data['content'] = ModelCurriculumSection::getWithCourseBy("curriculum_section.id",$id);

        $data['page_title'] = 'Course Content';
        $data['id'] = $id;
                 return view('mentor.course.curriculum.video', $data);    
                //  return view('fileupload', $data);    

    }
    
    public function fileStore(Request $request)
    {
        $request->validate([
            'file' => 'required',
        ]);
        
       $fileName = request()->file->getClientOriginalName();

        $content = ModelCurriculumSection::getWithCourseBy("curriculum_section.id",$request->id);

        $url = "uploads/video/$content->course_id";

                
        if(!is_dir($url)) {
        mkdir($url, 0777, true);
        }


        $name = $url .'/'.$fileName;
        // $name = "uploads/video/" .$fileName;

       $request->file->move(public_path($url.'/'), $fileName);
           $affected = DB::table('curriculum_section')
              ->where('id', $request->id)
              ->update(['video' => $name]);

    //    return redirecg

        // return response()->json(['success'=>$name]);
        return response()->json(['success'=>'File Uploaded Successfully']);
    }
    
}
