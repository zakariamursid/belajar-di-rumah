<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use App\Model\ModelCurriculum;
use App\Model\ModelOrders;
use App\Model\ModelCourse;
use App\Model\ModelPayments;
use crocodicstudio\crudbooster\controllers\partials\ButtonColor;
use Illuminate\Http\Request;
use App\Helpers\Web;
class AdminPaymentsController extends CBController {


    public function cbInit()
    {
        $this->setTable("payments");
        $this->setPermalink("payments");
        $this->setPageTitle("Payments");

        $this->addSelectTable("Order","order_id",["table"=>"orders","value_option"=>"id","display_option"=>"id","sql_condition"=>""]);
		$this->addText("Bank","bank")->strLimit(150)->maxLength(255);
		$this->addText("Branch","branch")->strLimit(150)->maxLength(255);
		$this->addText("Account Name","account_name")->strLimit(150)->maxLength(255);
		$this->addText("Account Number","account_number")->strLimit(150)->maxLength(255);
		$this->addSelectOption("Status","status")->options(['Pending'=>'Pending','Approved'=>'Approved','Failed'=>'Failed','Fraud'=>'Fraud']);
		$this->addImage("Proof","proof")->encrypt(true);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		   
        $this->hookAfterUpdate(function($id)
        {
            redirect("/user/payments/".$id ."/update")->send();
            exit;
        });

	}
	
    public function store(Request $request)
       {   
         $save = new ModelPayments;
         $save->setBank($request->bank);
         $save->setBranch($request->branch);
         $save->setOrder($request->order_id);
         $save->setAccountName($request->account_name);
         $save->setAccountNumber($request->account_number);
        //  $save->setNominal($request->nominal);
         $save->setStatus("Pending");
        if ($request->hasFile("proof"))
        {
            $save->setProof(Web::UploadFile("proof","proof"));
        }               
		$save->add();
        return redirect()->back();
		// return redirect('/order/'.$request->order_id);
       }
}
