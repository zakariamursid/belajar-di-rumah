<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;
use App\Model\ModelWithdraw;
use App\Model\ModelWallet;
use Illuminate\Http\Request;
class AdminWithdrawalController extends CBController {


    public function cbInit()
    {
        $this->setTable("withdrawal");
        $this->setPermalink("withdrawal");
        $this->setPageTitle("Withdrawal");

        $this->addSelectTable("User","users_id",["table"=>"users","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addMoney("Amount","amount");
		$this->addText("Account Name","account_name")->strLimit(150)->maxLength(255);
		$this->addNumber("Account Number","account_number");
		$this->addSelectOption("Status","status")->options(['Pending'=>'Pending','Done'=>'Done']);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		
		// To make an additional action after update action
    $this->hookAfterUpdate(function($id) {
		// Todo: code here
		redirect("/admin/withdrawal/".$id ."/update")->send();
		exit;

		});
    }

    public function setwallet($id)
    {
        $check = ModelWithdraw::getBy("withdrawal.id",$id);

        if($check->status == "Done")
        {     
         
        $ret = DB::table("wallet")
        ->where('transaction_id', $check->id)
        ->where('users_id', $check->users_id)
        ->where('balance', $check->amount)
        ->where('type', "KREDIT")
        ->first();

        if(!empty($ret)) 
        {
            return redirect('/user/withdrawal');  
        }

        else {
        
            $wallet = new ModelWallet;
            $wallet->setBalance($check->amount);
            $wallet->setUser($check->users_id);
            $wallet->setOrder($check->id);
            $wallet->setType("KREDIT");
            $wallet->add();

            return redirect('/user/withdrawal');  
            }
        }

        else
        {
            return redirect('/user/withdrawal');
        }
        
        // $save = new ModelWallet;
    }

}
