<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use App\Model\ModelCourse;
use App\Model\ModelTopic;

class AdminCoursesController extends CBController {


    public function cbInit()
    {
        $this->setTable("course");
        $this->setPermalink("course");
        $this->setPageTitle("Course");

        $this->addText("Title","title")->strLimit(150)->maxLength(255);
		$this->addSelectOption("Level","level")->options(['Beginner'=>'Beginner','Intermediate'=>'Intermediate','Expert'=>'Expert']);
		$this->addSelectTable("Topic","topic_id",["table"=>"topic","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addImage("Banner","banner")->encrypt(true);
		$this->addTextArea("Short Desc","short_desc")->strLimit(150);
		$this->addTextArea("Desc","desc")->strLimit(150);
		$this->addTextArea("Preparation","preparation")->strLimit(150);
		$this->addTextArea("Include","include")->strLimit(150);
		$this->addMoney("Price","price");
		$this->addSelectTable("Mentor","mentor_id",["table"=>"users","value_option"=>"id","display_option"=>"name","sql_condition"=>"cb_roles_id=2"]);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

    }

    // public function getAdd()
    // {

    // // $data['post'] = '';
    // $data['topics'] = ModelTopic::get();
    // $data['levels'] = [
    //     'Beginner','Medium','Expert'
    // ];

    // $data['url'] = 'mentor/course/add';


    // $data['page_title'] = 'Add Course';
    // return view('course.add', $data);        
    // }

    public function getAddPricing()
    {


    $data['topics'] = ModelTopic::get();
    $data['levels'] = [
        'Beginner','Medium','Expert'
    ];

    $data['url'] = 'mentor/course/add';


    $data['page_title'] = 'Add Course';
    return view('course.add2', $data);        
    }



    public function postAdd(Request $request)
    {
     $valid = [
            "title" => "required",
            "short_desc" => "required",
            "desc" => "required",
            "topic_id" => "required",
            "level" => "required",
        ];

        $valid = Validator::make($request->all(),$valid);

            if ($valid->fails()) {
            // return Web::withError("course/add",$request->all(['title','short_desc',"desc",'topic_id','level']),$valid->errors());
        }else{

            $save = new ModelCourse;
            $save->SetTitle($request->title);
            $save->setLevel($request->level);
            $save->setTopic_id($request->topic_id);
            $save->setShort_desc($request->short_desc);
            $save->setDesc($request->desc);
            $save->setMentor_id($request->mentor_id);
            $save->add();
        }
        // $link ="{{ url('/admin/course/add-pricing') }}";
        // $id = 

        // $id = 
        $latest_course_id = ModelCourse::getIdLatestCourse(cb()->session()->id())->id;

        
        return redirect("/mentor/course/add-pricing")->with( ['data' => $latest_course_id] );;
        // return redirect({{url('')}});
    }


}
