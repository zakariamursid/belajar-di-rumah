<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Auth;
use App\CourseSyllabus;
use App\CourseSyllabusItem;


class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = [];
        $data['course'] = DB::table('course')
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })        
        ->get();
        $auth =  Auth::user();
        if($auth != null)
        {
        $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
        return view('test', compact('data'));
        }
        else {
            return redirect('user/login/') ->with('status', 'Record successfully Added !');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = [];
        $data['course'] = DB::table('course')
        ->select('course.*', 'users.id as user_id','users.name as user_name')
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })
        ->where('course.id',$id)        
        ->first();

        $data['course_also'] = DB::table('course')
        ->select('course.*', 'users.id as user_id','users.name as user_name')
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })
        ->get();
        $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
        return view('show', compact('data'));        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function coursecontent($id)
    {
        $data = [];
        $data['course'] = DB::table('course')
        ->select('course.*', 'users.id as user_id','users.name as user_name')
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })
        ->where('course.id',$id)        
        ->first();

        $data['course_syllabus'] = DB::table('course_syllabus')
        ->select(DB::raw('count(*) as num'))
        // ->select('course_syllabus.title as syllabus_title','course_syllabus_item.*')
        ->join('course_syllabus_item', function ($join){
        $join->on('course_syllabus_item.course_syllabus_id', '=', 'course_syllabus.id');
        })
        // ->where('course_syllabus.course_id',$id)
        // ->groupBy('course_syllabus_item')
        ->get();

        // ->groupBy('course_syllabus_item.course_syllabus_id')

        
        $data['course_syllabus'] = CourseSyllabus::with('syllabusitem')->get();
     
        $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
         
        return view('course.content', compact('data'));        
    }

    public function checkoutpage()
    {
        $data = [];
        $id = 1;
    

        $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();

        $data['course'] = DB::table('course')
        ->select('course.*', 'users.id as user_id','users.name as mentor_name')
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })
        ->where('course.id',$id)        
        ->first();

        return view('course.checkout', compact('data'));        

    }
}
