<?php

namespace App\Http\Controllers;
use App\Model\ModelCoupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MentorCouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          if(request()->ajax())
        {
            $data = ModelCoupon::findBy("id",$id);
            return response()->json(['data' => $data]);
        }    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
		$originalDate = str_replace('/', '-', $request->expired);
$newDate = date("Y-m-d", strtotime($originalDate));

            $save = new ModelCoupon;
            $save->setCode($request->code);
            $save->setQty($request->qty);
            $save->setStatus($request->status);
            $save->setValid_until($newDate);
                      $save->edit("id",$request->id);
            return response()->json(['success' => 'Data Added successfully.']);
            
            // return response()->json(['success' => $request->valid_until]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $del = ModelCoupon::remove("id",$id);
        return response()->json(['success' => $id]);            
    }
}
