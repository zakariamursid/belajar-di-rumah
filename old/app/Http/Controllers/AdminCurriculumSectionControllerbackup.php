<?php namespace App\Http\Controllers;
use Auth;
use DB;
use crocodicstudio\crudbooster\controllers\CBController;
use App\Model\ModelCourse;
class AdminCurriculumSectionController extends CBController {


    public function cbInit()
    {

    // $this->hookIndexQuery(function($query) {
    //     // Todo: code query here

	// 	echo cb()->session()->id();
    //     // You can make query like laravel db builder
	// 	$query
    //     ->join('curriculum','curriculum.id','=','curriculum_section.curriculum_id')
    //     ->join('course','course.id','=','curriculum.course_id')
	// 	->where("course.mentor_id", cb()->session()->id());

	// 	// DD($query);
    //     // Don't forget to return back
    //     return $query;
	// });
			
		// $id = request('id');
        // $this->addSelectTable("Curriculum","curriculum_id",["table"=>"curriculum","value_option"=>"id","display_option"=>"title","sql_condition"=>(request('id'))?"curriculum.course_id = $id" : ""]);

		// $session_id = cb()->session()->id();
// dd($get);
			// $test = cb()->session()->id();
			// echo cb()->session()->id();
// $get = DB::table('course')->where("mentor_id",cb()->session()->id())->get();
// dd($get);
// dd(cb()->session()->id());

        $this->setTable("curriculum_section");
        $this->setPermalink("curriculum_section");
		$this->setPageTitle("Curriculum Section");


        $this->addSelectTable("Curriculum","curriculum_id",["table"=>"curriculum","value_option"=>"id","display_option"=>"title","sql_condition"=>""]);
        // $this->addSelectTable("Curriculum","curriculum_id",["table"=>"curriculum","value_option"=>"id","display_option"=>"title","sql_condition"=>"curriculum.course_id = $id"]);
		$this->addText("Title","title")->strLimit(150)->maxLength(255);
		$this->addFile("Video","video")->required(false)->encrypt(true);
		$this->addTextArea("Desc","desc")->required(false)->strLimit(150);
		$this->addSelectOption("Access","access")->options(['PRIVATE'=>'PRIVATE','PUBLIC'=>'PUBLIC']);
		$this->addSelectOption("Permission","permission")->options(['WAITING ADMIN APPROVAL'=>'WAITING ADMIN APPROVAL','APPROVED'=>'APPROVED','REJECTED'=>'REJECTED','REPORTED'=>'REPORTED']);
		$this->addNumber("Short Number","short_number")->required(false);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		
		// dd(Auth::user());

    }
}
