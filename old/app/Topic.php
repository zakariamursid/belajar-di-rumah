<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Topic extends Model
{
    private static $table_name = "trans";
    private $name;
    private $field_id;

    function setName($name) { 
        $this->name = $name; 
    }
    function getName() { 
        return $this->name; 
    }    
    function setField($field_id) { 
        $this->field_id = $field_id; 
    }
    function getField() { 
        return $this->field_id; 
    }
 

    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->get();

        return $ret;
    }

    // TODO SAVE DATA
    public function add()
    {
        $save = DB::table(static::$table_name)
            ->insert([
                "name" => $this->getName(),
                "field_id" => $this->getField(),
            ]);

        return $save;
    }    
    

}
