<?php

namespace App\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Image;
use DateTime;

class Web
{

    public static function UploadFile($name, $path = null) {
        if (empty($path)) {
            $date = date('Y-m');
            $path = 'uploads/'.$date;
        }else{
            $date = date('Y-m');
            $path = 'uploads/'.$path;
        }

        if (app('request')->hasFile($name)){
            $file = app('request')->file($name);
            $filename = $file->getClientOriginalName();
            $ext = strtolower($file->getClientOriginalExtension());

            if($filename && $ext) {
                Storage::makeDirectory($path);
                $destinationPath = public_path($path);
                $extension        = $ext;
                $names            = rand(11111,99999).'.'.$extension;
                app('request')->file($name)->move($destinationPath, $names);
            }

            return $path.'/'.$names;
        }

    }

    public static function file($path = null)
    {
        $check_public = base_path('public/' . $path);
        $check_storage = storage_path('app/' . $path);

        if ($path == null) {
            return '';
        } elseif (file_exists($check_public)) {
            return url($path);
        } elseif (file_exists($check_storage)) {
            return url($path);
        } else {
            return '';
        }
    }

    public static function imagePath($path = null)
    {
        if (Web::file($path)) {
            return Web::file($path);
        }else{
            return Web::file("pearl-ui/images/favicon.png");
        }
    }    

    public static function UploadThumbnail($name, $width = null, $length = null) {
    
    $ext = strtolower(app('request')->file($name)->getClientOriginalExtension());
    $nameImage  = rand(11111,99999).'.'.$ext;


    if($width && $length != null)
    {
        $thumbImage = Image::make(app('request')->file($name)->getRealPath())->resize($width, $length);
    }
    else
    {
        $thumbImage = Image::make(app('request')->file($name)->getRealPath());
    }

    $thumbPath = 'uploads/thumbnail/' . $nameImage;
    $thumbImage = Image::make($thumbImage)->save(public_path() .'/' .$thumbPath);
    
    return $thumbPath;

    }

     public static function DateNow()
    {
        date_default_timezone_set('Asia/Jakarta');
        return new dateTime();
        
    }

        


  
}