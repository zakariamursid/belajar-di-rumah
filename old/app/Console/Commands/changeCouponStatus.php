<?php

namespace App\Console\Commands;
use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

class changeCouponStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coupon:changestatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Coupon Status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::table('coupon')->where('valid_until',Carbon::now())->update(['status' => "Tidak Aktif"]);

    }
}
