<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Model\ModelCommission;
use DateTime;

class ModelAdminWallet extends Model
{
    private static $table_name = "admin_wallet";
    private $balance;
    private $order_id;
    private $type;
    private $desc;

    function setBalance($balance) { $this->balance = $balance; }
    function getBalance() { return $this->balance; }
    function setOrder($order_id) { $this->order_id = $order_id; }
    function getOrder() { return $this->order_id; }

    function setType($type) { $this->type = $type; }
    function getType() { return $this->type; }

    function setDesc($desc) { $this->desc = $desc; }
    function getDesc() { return $this->desc; }

    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->get();

        return $ret;
    }

        // TODO SAVE DATA
    public function add()
    {
        $data['balance'] = $this->getBalance();
        $data['type'] = $this->getType();
        $data['desc'] = $this->getDesc();
        $data['transaction_id'] = $this->getOrder();
        $data['created_at'] = new DateTime();

       
            // dd($data);
        $save = DB::table(static::$table_name)
            ->insert($data);

        return $save;

    }

     public static function countAllDebit()
    {
        $ret = DB::table(static::$table_name)
            ->where('type','DEBIT')
            ->sum('balance');

        return $ret;
    }   

    public static function cek($transactionid, $type)
    {
        $ret = DB::table(static::$table_name)
            ->where("transaction_id",$transactionid)
            ->where("type",$type)
            ->first();

        return $ret;        
    }    
    
// TODO ACC PENDING COURSE
    public function updateStatus($id)
    {
        $data['updated_at'] = new dateTime();
        $data['desc'] = $this->getDesc();

        $save = DB::table(static::$table_name)
            ->where("id",$id)
            ->update($data);
            
        return $save;
    }
         
   // TODO DELETE DATA
    public static function remove($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->delete();
        return $save;
    }           

}
