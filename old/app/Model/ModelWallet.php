<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use DateTime;
class ModelWallet extends Model
{
    private static $table_name = "wallet";
    private $order_id;
    private $users_id;
    private $balance;
    private $type;
    private $desc;

    function setOrder($order_id) { $this->order_id = $order_id; }
    function getOrder() { return $this->order_id; }

    function setUser($users_id) { $this->users_id = $users_id; }
    function getUser() { return $this->users_id; }

    function setBalance($balance) { $this->balance = $balance; }
    function getBalance() { return $this->balance; }

    function setType($type) { $this->type = $type; }
    function getType() { return $this->type; }

    function setDesc($desc) { $this->desc = $desc; }
    function getDesc() { return $this->desc; }

     public static function getByUser($val)
    {
        $ret = DB::table(static::$table_name)
            ->join('users','users.id','=','wallet.users_id')
            ->where('wallet.users_id', $val)
            ->get();

        return $ret;
    }   

     public static function getEarnDetailByUser($val)
    {
        $ret = DB::table(static::$table_name)
            ->select('wallet.created_at as created','wallet.balance as earn', 'course.title as course_title', 'users.name as buyer_name')
            ->join('orders','orders.id','=','wallet.transaction_id')
            ->join('course','orders.course_id','=','course.id')
            ->join('users','users.id','=','orders.users_id')
            ->where('wallet.users_id', $val)
            ->where('wallet.type','DEBIT')
            ->get();

        return $ret;
    }   

     public static function countEarn($val)
    {
        $ret = DB::table(static::$table_name)
            ->where('type','DEBIT')
            ->where('users_id', $val)
            ->sum('balance');

        return $ret;
    }   

     public static function countAllDebit()
    {
        $ret = DB::table(static::$table_name)
            ->where('type','DEBIT')
            ->sum('balance');

        return $ret;
    }   


  // TODO SAVE DATA
    public function add()
    {

        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => new dateTime(),
                "transaction_id" => $this->getOrder(),
                "users_id" => $this->getUser(),
                "desc" => $this->getDesc(),
                "balance" => $this->getBalance(),
                "type" => $this->getType(),
            ]);

        return $save;
    }    

    public function withdraw()
    {

        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => new dateTime(),
                "transaction_id" => $this->getOrder(),
                "users_id" => $this->getUser(),
                "balance" => $this->getBalance(),
                "type" => "KREDIT",
            ]);

        return $save;        
    }


    public static function cek($transactionid, $type)
    {
        $ret = DB::table(static::$table_name)
            ->where("transaction_id",$transactionid)
            ->where("type",$type)
            ->first();

        return $ret;        
    }    
    
// TODO ACC PENDING COURSE
    public function updateStatus($id)
    {
        $data['updated_at'] = new dateTime();
        $data['status'] = "Approved";

        $save = DB::table(static::$table_name)
            ->where("id",$id)
            ->update($data);
            
        return $save;
    }    
     
   // TODO DELETE DATA
    public static function remove($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->delete();
        return $save;
    }

}
