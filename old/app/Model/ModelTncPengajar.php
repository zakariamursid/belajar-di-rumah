<?php

namespace App\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use DateTime;

class ModelTncPengajar extends Model
{
   private static $table_name = "tnc_pengajar";
   private $content;

   function setContent($content) { $this->content = $content; }
   function getContent() { return $this->content; }


   public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->first();

        return $ret;
    }

// TODO ACC PENDING COURSE
    public function updateTnc()
    {
        $data['updated_at'] = new dateTime();
        $data['content'] = $this->getContent();

        $save = DB::table(static::$table_name)
            ->update($data);
            
        return $save;
    }    
     
    
}
