<?php

namespace App\Model;
use DateTime;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class ModelCommission extends Model
{
  private $admin;
    private $mentor;

    function setAdmin($admin) { $this->admin = $admin; }
    function getAdmin() { return $this->admin; }


    function setMentor($mentor) { $this->mentor = $mentor; }
    function getMentor() { return $this->mentor; }

    private static $table_name = "commission";

    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->first();

        return $ret;
    }


 // TODO UPDATE DATA
    public function edit()
    {
        $data['updated_at'] = new dateTime();
  
        if ($this->getAdmin()) {
            $data['admin'] = $this->getAdmin();
        }
       
        if ($this->getMentor()) {
            $data['mentor'] = $this->getMentor();
        }

        $save = DB::table(static::$table_name)
            ->update($data);
            
        return $save;
    }


}

    