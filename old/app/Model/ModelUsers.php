<?php

namespace App\Model;
use Illuminate\Support\Facades\DB;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class ModelUsers extends Model
{
    private static $table_name = "users";
    private $status;
    
    function setStatus($status) { $this->status = $status; }
    function getStatus() { return $this->status; }

    public static function get()
    {
        $ret = DB::table(static::$table_name)
        ->select('users.*','cb_roles.name as role'
         ,DB::raw("((SELECT count(b.id) FROM course as b WHERE b.mentor_id = users.id  AND b.status = 'Approved')) as upload,
         ((SELECT SUM(c.balance) FROM wallet as c join withdrawal as b on c.transaction_id = b.id WHERE b.users_id = users.id AND status='Done' AND c.transaction_id = b.id  AND c.type = 'DEBIT')) as sum ")            
        )        
        ->join('cb_roles','cb_roles.id','=','users.cb_roles_id')
        ->where("cb_roles_id", "!=", 1)
        ->orderBy('users.created_at', 'desc')        
        ->get();
   
        return $ret;
    }
    public static function getLatest()
    {
        $ret = DB::table(static::$table_name)
        ->select('users.*','cb_roles.name as role'
         ,DB::raw("((SELECT count(b.id) FROM course as b WHERE b.mentor_id = users.id  AND b.status = 'Approved')) as upload,
         ((SELECT SUM(c.balance) FROM wallet as c join withdrawal as b on c.transaction_id = b.id WHERE b.users_id = users.id AND status='Done' AND c.transaction_id = b.id  AND c.type = 'DEBIT')) as sum ")            
        )        
        ->join('cb_roles','cb_roles.id','=','users.cb_roles_id')
        ->where("cb_roles_id", "!=", 1)
        ->orderBy('users.created_at', 'desc')  
        ->limit(10)      
        ->get();
   
        return $ret;
    }
    public static function getBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->where($key, $val)
        ->first();
   
        return $ret;
    }



// TODO ACC PENDING COURSE
    public function updatePrivelege($id)
    {
        $data['updated_at'] = new dateTime();
        $data['cb_roles_id'] = 2;

        $save = DB::table(static::$table_name)
            ->where("id",$id)
            ->update($data);
            
        return $save;
    }     

    public function disabled($id)
    {
        $data['updated_at'] = new dateTime();
        $data['status'] = "Disabled";

        $save = DB::table(static::$table_name)
            ->where("id",$id)
            ->update($data);
            
        return $save;
    }     
    public function changeStatus($id)
    {
        $data['updated_at'] = new dateTime();
        $data['status'] = $this->getStatus();

        $save = DB::table(static::$table_name)
            ->where("id",$id)
            ->update($data);
            
        return $save;
    }     
}
