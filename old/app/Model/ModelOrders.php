<?php

namespace App\Model;
use Illuminate\Support\Facades\DB;
use DateTime;
use App\Model\ModelCoupon;
use App\Model\ModelCourse;
use App\Model\ModelCommission;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\Web;

class ModelOrders extends Model
{
    private $id;
    private $course_id;
    private $users_id;
    private $coupon;
    private $course_price;
    private $status;
    private $grand_total;
    private $coupon_discount;

    function setCourse($course_id) { $this->course_id = $course_id; }
    function getCourse() { return $this->course_id; }
    
    function setBuyer($users_id) { $this->users_id = $users_id; }
    function getBuyer() { return $this->users_id; }

    function setStatus($status) { $this->status = $status; }
    function getStatus() { return $this->status; }

    function setCoursePrice($course_price) { $this->course_price = $course_price; }
    function getCoursePrice() { return $this->course_price; }   
    
    function setCoupon($coupon) { $this->coupon = $coupon; }
    function getCoupon() { return $this->coupon; }    

    function setGrandTotal($grand_total) { $this->grand_total = $grand_total; }
    function getGrandTotal() { return $this->grand_total; }    

    function setCouponDiscount($coupon_discount) { $this->coupon_discount = $coupon_discount; }
    function getCouponDiscount() { return $this->coupon_discount; }    

    private static $table_name = "orders";


    public static function getById($id)
    {
        $ret = DB::table(static::$table_name)->where('id',$id)
            ->first();

        return $ret;
    }
    

    public static function get()
    {
        $ret = DB::table(static::$table_name)
        ->select('users.name as buyer_name','course.title as course_title','orders.*')
        ->join('course','course.id','=','orders.course_id')
        ->join('users','users.id','=','orders.users_id')
        ->get();

        return $ret;
    }

    public static function getLatest()
    {
        $ret = DB::table(static::$table_name)
        ->select('users.name as buyer_name','course.title as course_title','orders.*'
        ,DB::raw("((SELECT status FROM payments as b WHERE b.order_id = orders.id)) as payment_status")
        )
        ->join('course','course.id','=','orders.course_id')
        ->join('users','users.id','=','orders.users_id')
        ->orderBy('orders.created_at', 'desc')          
        ->limit(10)      
        ->get();

        return $ret;
    }

    public static function getBy($val, $key)
    {
        $ret = DB::table(static::$table_name)
        ->select('orders.id as order_id', 'orders.status as order_status', 'course.*' ,'users.name as mentor_name')
        ->join('course', function ($join){
        $join->on('course.id', '=', 'orders.course_id');
        }) 
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })       
        ->where($val,$key)
            ->get();

        return $ret;
    }

    public static function getByUserAndStatus($userid, $status)
    {
        $ret = DB::table(static::$table_name)
        ->select('orders.id as order_id', 'orders.status as order_status', 'course.*' ,'users.name as mentor_name')
        ->join('course', function ($join){
        $join->on('course.id', '=', 'orders.course_id');
        }) 
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })       
        ->where("orders.users_id",$userid)
        ->where("orders.status",$status)
            ->get();

        return $ret;
    }
     
    public static function getByWithBuyer($val, $key)
    {
        $ret = DB::table(static::$table_name)
        ->select('orders.id as order_id', 'orders.status as order_status', 'orders.created_at as order_created',
        'users.name as buyer_name', 'course.title as course_title', 'orders.grand_total as order_total')
        ->join('course', function ($join){
        $join->on('course.id', '=', 'orders.course_id');
        }) 
        ->join('users', function ($join){
        $join->on('users.id', '=', 'orders.users_id');
        })       
        ->where($val,$key)
            ->get();

        return $ret;
    }

    public static function checkDuplicate($course_id, $user)
    {
        $ret = DB::table(static::$table_name)
        ->where("course_id",$course_id)
        ->where("users_id",$user)
        ->first();

        return $ret;
    }

        public static function getLatestId()
    {
        $ret = DB::table(static::$table_name)
        ->where('users_id',cb()->session()->id())
        ->orderBy('created_at', 'desc')->first();
        return $ret;
    }


    // TODO SAVE DATA
    public function add()
    {
            $data['course_id'] = $this->getCourse();
            $data['users_id'] = $this->getBuyer();
            $data['course_price'] = $this->getCoursePrice();
            $data['grand_total'] = $this->getGrandTotal();
            $data['status'] = $this->getStatus();
        
            $startDate = time();
            $void_at = date('Y-m-d H:i:s', strtotime('+1 day', $startDate));

            // $datetime = new DateTime();
            // $void_at = $datetime->modify('+1 day');
            
            $data['created_at'] = Web::DateNow();

            $data['void_at'] = $void_at;
            // dd($data);
            if ($this->getCoupon()) {
                $data['coupon'] = $this->getCoupon();
                }    
            if ($this->getCouponDiscount()) {
                $data['coupon_discount'] = $this->getCouponDiscount();
                }    
            // dd($data);

            // $data['mentor_commission'] = $course->price;
            // $commission = ModelCommission::get();
            // $data['mentor_commission'] = ($course->price*($commission->mentor / 100));

            // dd($data);
        $save = DB::table(static::$table_name)
            ->insert($data);

        return $save;

    }


    public static function getUnpayedCourse($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->select('orders.id as order_id', 'users.name as mentor_name',
         'course.id as course_id','course.title as course_title','course.poster as course_poster')

        ->join('course', function ($join){
        $join->on('course.id', '=', 'orders.course_id');
        }) 
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })
        ->where($key, $val)
        ->get();

        return $ret;        
    }

// TODO UPDATE DATA
    public function edit($key, $id)
    {
        $data['updated_at'] = new dateTime();
  
        if ($this->getStatus()) {
            $data['status'] = $this->getStatus();
        }
       
  
        $save = DB::table(static::$table_name)
            ->where($key,$id)
            ->update($data);
            
        return $save;
    }        


// UPDATE Status
    public function updateStatus($id)
    {
        $data['updated_at'] = new dateTime();

        if ($this->getStatus()) {
            $data['status'] = $this->getStatus();
        }

        
        $save = DB::table(static::$table_name)
            ->where("id",$id)
            ->update($data);
            
        return $save;
    }       

    public static function cekOrderSuccess($id,$buyer_id)
    {
        $ret = DB::table(static::$table_name)
        ->where('course_id',$id)
        ->where('users_id',$buyer_id)
            ->first();

        return $ret;
    }

    public static function countOrderSuccess($id)
    {
        $ret = DB::table(static::$table_name)
        ->where('course_id',$id)
        ->where('status', 'Success')
        ->count();
        return $ret;
    }

    public static function isBuyer($id)
    {
        $ret = DB::table(static::$table_name)
        ->where('course_id',$id)
        ->where('status', 'Success')
        ->where('users_id', cb()->session()->id())
        ->count();
        return $ret;
    }

    public static function countBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->join('course','course.id','=','orders.course_id')
        ->where($key, $val)
        ->count();
        return $ret;
    }
    public static function countSuccessBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->join('course','course.id','=','orders.course_id')
        ->where($key, $val)
        ->where('orders.status', 'Success')
        ->count();
        return $ret;
    }
    
}
