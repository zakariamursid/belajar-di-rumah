<?php


namespace App\Model;
use DateTime;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ModelSyllabusItem extends Model
{
     private $course_syllabus_id;
     private $title;
     private $slug;
     private $desc;
     private $video_url;

    function setSyllabus($course_syllabus_id) { $this->course_syllabus_id = $course_syllabus_id; }
    function getSyllabus() { return $this->course_syllabus_id; }

    function setTitle($title) { $this->title = $title; }
    function getTitle() { return $this->title; }
    

    function setDesc($desc) { $this->desc = $desc; }
    function getDesc() { return $this->desc; }

    function setVideo($video_url) { $this->video_url = $video_url; }
    function getVideo() { return $this->video_url; }


    private static $table_name = "course_syllabus_item";
    
    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->get();

        return $ret;
    }
    public static function getByCourseId($id)
    {
        $ret = DB::table(static::$table_name)
            ->where('id',$id)
            ->get();
        // $ret = DB::table(static::$table_name)
        //     ->select('course_syllabus_item.*', 'course_syllabus.title as syllabus')
        //     ->join('course_syllabus', function ($join){
        //     $join->on('course_syllabus_item.course_syllabus_id', '=', 'course_syllabus.id');
        //     })
        //     ->where('course_syllabus.id',$id)
        //     ->get();

        return $ret;
    }
    public static function getById($id)
    {
        $ret = DB::table('course_syllabus')
            ->select('course_syllabus_item.*', 'course_syllabus.title as syllabus')
            ->join('course_syllabus_item', function ($join){
            $join->on('course_syllabus.id', '=', 'course_syllabus_item.course_syllabus_id');
            })
            ->where('course_syllabus.id',$id)
            ->get();
        // $ret = DB::table(static::$table_name)
        //     ->select('course_syllabus_item.*', 'course_syllabus.title as syllabus')
        //     ->join('course_syllabus', function ($join){
        //     $join->on('course_syllabus_item.course_syllabus_id', '=', 'course_syllabus.id');
        //     })
        //     ->where('course_syllabus.id',$id)
        //     ->get();

        return $ret;
    }


  // TODO SAVE DATA
    public function add()
    {

        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => new dateTime(),
                "title" => $this->getTitle(),
                "course_id" => $this->getCourse(),
            ]);

        return $save;
    }        
    

}
