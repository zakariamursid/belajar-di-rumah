<?php

namespace App\Model;
use DateTime;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class ModelPayments extends Model
{
    private static $table_name = "payments";
    private $order_id;
    private $bank;
    private $branch;
    private $account_name;
    private $account_number;
    private $nominal;
    private $status;
    private $proof;
    
    function setOrder($order_id) { $this->order_id = $order_id; }
    function getOrder() { return $this->order_id; }

    function setBank($bank) { $this->bank = $bank; }
    function getBank() { return $this->bank; }

    function setBranch($branch) { $this->branch = $branch; }
    function getBranch() { return $this->branch; }

    function setAccountName($account_name) { $this->account_name = $account_name; }
    function getAccountName() { return $this->account_name; }

    function setAccountNumber($account_number) { $this->account_number = $account_number; }
    function getAccountNumber() { return $this->account_number; }

    function setNominal($nominal) { $this->nominal = $nominal; }
    function getNominal() { return $this->nominal; }

    function setStatus($status) { $this->status = $status; }
    function getStatus() { return $this->status; }

    function setProof($proof) { $this->proof = $proof; }
    function getProof() { return $this->proof; }

     public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->get();

        return $ret;
    }   

     public static function getBy($val, $key)
    {
        $ret = DB::table(static::$table_name)
            ->where($val,$key)
            ->first();

        return $ret;
    }   

    // TODO SAVE DATA
    public function add()
    {
        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => new dateTime(),
                "order_id" => $this->getOrder(),
                "bank" => $this->getBank(),
                "branch" => $this->getBank(),
                "account_name" => $this->getAccountName(),
                "account_number" => $this->getAccountNumber(),
                "status" => $this->getStatus(),
                "proof" => $this->getProof(),
            ]);

        return $save;
    }

    public static function countPendingBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->join('orders','orders.id','=','payments.order_id')
        ->join('course','course.id','=','orders.course_id')
        ->where('payments.status','Pending')
        ->where($key, $val)
        ->count();
        return $ret;
    }


}
