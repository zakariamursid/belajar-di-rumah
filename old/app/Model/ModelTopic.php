<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use DateTime;
class ModelTopic extends Model
{
    private static $table_name = "topic";
    private $name;

    function setName($name) { $this->name = $name; }
    function getName() { return $this->name; }
    
    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->get();

        return $ret;
    }

    public static function getById($id)
    {
        $ret = DB::table(static::$table_name)->where('id',$id)
            ->first();

        return $ret;
    }
    public static function getBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->where($key,$val)
        ->get();

        return $ret;
    }
    public static function findBy($key,$val)
    {
        $ret = DB::table(static::$table_name)
        ->where($key, $val)
            ->first();

        return $ret;
    }

    // TODO SAVE DATA
    public function add()
    {
        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => new dateTime(),
                "name" => $this->getName(),
            ]);

        return $save;
    }    


}
