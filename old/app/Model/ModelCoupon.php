<?php

namespace App\Model;
use DateTime;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ModelCoupon extends Model
{
        private $id;
        private $course_id;
        private $mentor_id;
        private $code;
        private $qty;
        private $discount;
        private $type;
        private $status;
        private $valid_until;

    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    function setCourse($course_id) { $this->course_id = $course_id; }
    function getCourse() { return $this->course_id; }

    function setMentor($mentor_id) { $this->mentor_id = $mentor_id; }
    function getMentor() { return $this->mentor_id; }

    function setCode($code) { $this->code = $code; }
    function getCode() { return $this->code; }

    function setQty($qty) { $this->qty = $qty; }
    function getQty() { return $this->qty; }

    function setDiscount($discount) { $this->discount = $discount; }
    function getDiscount() { return $this->discount; }

    function setType($type) { $this->type = $type; }
    function getType() { return $this->type; }

    function setStatus($status) { $this->status = $status; }
    function getStatus() { return $this->status; }

    function setValid_until($valid_until ) { $this->valid_until = $valid_until; }
    function getValid_until() { return $this->valid_until; }

    private static $table_name = "coupon";

    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->get();

        return $ret;
    }

    public static function getCouponByCourseId($id)
    {
        $ret = DB::table(static::$table_name)->where('course_id',$id)
            ->get();

        return $ret;
    }
    public static function getBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->select('coupon.*', DB::raw("((SELECT COUNT(b.id) FROM orders as b WHERE b.coupon = coupon.code  AND b.status = 'Success')) as coupon_used"))
        ->where($key,$val)
        ->get();

        return $ret;
    }

    public static function findBy($val, $key)
    {
        $ret = DB::table(static::$table_name)->where($val,$key)
            ->first();
        // $ret = ModelCurriculum::with('section')->where('id',$id)->first();

        return $ret;
    }

    public static function check($code, $course_id)
    {
        $ret = DB::table(static::$table_name)
        ->select('coupon.*', DB::raw("(coupon.qty - (SELECT COUNT(b.id) FROM orders as b WHERE b.coupon = coupon.code  AND b.status = 'Success')) as coupon_left"))
        ->where("course_id",$course_id)
        ->where("code", $code)
            ->first();
        // $ret = ModelCurriculum::with('section')->where('id',$id)->first();

        return $ret;
    }
    

  // TODO SAVE DATA
    public function add()
    {

        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => new dateTime(),
                "course_id" => $this->getCourse(),
                "mentor_id" => $this->getMentor(),
                "code" => $this->getCode(),
                "qty" => $this->getQty(),
                "discount" => $this->getDiscount(),
                "type" => $this->getType(),
                "status" => $this->getStatus(),
                "valid_until" => $this->getValid_until(),
            ]);

        return $save;
    }    
    // TODO DELETE DATA
    public static function remove($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->delete();
        return $save;
    }

// TODO UPDATE DATA
    public function edit($key, $id)
    {
        $data['updated_at'] = new dateTime();
  
        if ($this->getCode()) {
            $data['code'] = $this->getCode();
        }
       
  
        if ($this->getQty()) {
            $data['qty'] = $this->getQty();
        }
       
        if ($this->getStatus()) {
            $data['status'] = $this->getStatus();
        }
        if ($this->getValid_until()) {
            $data['valid_until'] = $this->getValid_until();
        }
       

        $save = DB::table(static::$table_name)
            ->where($key,$id)
            ->update($data);
            
        return $save;
    }        

}
