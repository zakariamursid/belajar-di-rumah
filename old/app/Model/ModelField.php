<?php

namespace App\Model;
use DateTime;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class ModelField extends Model
{
    private static $table_name = "field";
    
    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->get();

        return $ret;
    }

    public static function getById($id)
    {
        $ret = DB::table(static::$table_name)->where('id',$id)
            ->first();

        return $ret;
    }
}
