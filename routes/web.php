<?php
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$base_mentor = "user";

Route::post('resize/resize_image', 'ResizeController@resize_image');


Route::get('/index','PublicController@index');

Route::get('/', 'PublicController@landing');
Route::get('/course/{id}', 'PublicController@show');
Route::get('/course/{id}/checkout', 'PublicController@checkout');
Route::post('/course/{id}/order', 'PublicController@placeorder');
Route::get('/bementor', 'PublicController@get_be_mentor');
Route::post('/post-bementor', 'PublicController@post_be_mentor');
Route::get('/order/{id}', 'PublicController@order');
Route::get('/order/{id}/payment', 'PublicController@payment');
Route::get('/user/orders/{id}/content', 'PublicController@order_content');
Route::get('/user/course/{id}/approve', 'AdminCourseController@approve');
Route::post('/order/{id}/validate', 'AdminPaymentsController@store');
Route::get('/user/withdraw', 'MentorWithdrawalController@index');
Route::get('/mentor/course', 'MentorCourseController@add_step_one');
Route::get('/order', 'UserCoursesController@index');
Route::get('/user/payments/{id}/update', 'AdminCommisionController@add');
Route::get('/user', 'AdminDashboardController@index');

// Mentor

// Course
Route::get("/test","PublicController@getBalance");
Route::get("/course","PublicController@index");
Route::get("/all-courses","PublicController@getAllCourses");

Route::get("$base_mentor/course/","AdminCourseController@getIndex");
Route::get("$base_mentor/course/{id}/coupon","MentorCourseController@pricing");
Route::get("$base_mentor/course/syllabus","AdminCourseSyllabusController@getIndex");
Route::get("$base_mentor/course/{id}/edit","AdminCourseController@getEdit({id})");


Route::get("$base_mentor/course/{id}/add-syllabus","AdminCourseSyllabusController@getAddNew");
Route::post("$base_mentor/course/{id}/pricing/add","AdminCouponController@postAdd");
Route::post("$base_mentor/course/add-syllabus","AdminCourseSyllabusController@postAdd");


Route::get("$base_mentor/course/{id}/add-content","AdminCourseSyllabusItemController@getAddNew");


Route::get("user/course/{id}/content", "MentorCurriculumController@index")->name('course-content');
Route::get("user/course/{id}/sort-content", "MentorCurriculumController@sort")->name('course-sort-content');

Route::resource('curriculum-ajax', 'MentorCurriculumController');
Route::get('curriculum-ajax/{id}/edit', 'MentorCurriculumController@edit')->name('curriculum-ajax.edit');
Route::post('curriculum-ajax/update', 'MentorCurriculumController@update')->name('curriculum-ajax.update');
Route::delete('curriculum-ajax/destroy/{id}', 'MentorCurriculumController@destroy')->name('curriculum-ajax.destroy');

Route::resource('section-ajax', 'MentorCurriculumSectionController');
Route::get('section-ajax/{id}/edit', 'MentorCurriculumSectionController@edit')->name('section-ajax.edit');
Route::post('section-ajax/update', 'MentorCurriculumSectionController@update')->name('section-ajax.update');
Route::delete('section-ajax/destroy/{id}', 'MentorCurriculumSectionController@destroy')->name('section-ajax.destroy');
Route::delete('section-ajax/destroy-video/{id}', 'MentorCurriculumSectionController@destroyVideo')->name('section-video.destroy');

Route::resource('coupon-ajax', 'MentorCouponController');
Route::get('coupon-ajax/{id}/edit', 'MentorCouponController@edit')->name('coupon-ajax.edit');
Route::post('coupon-ajax/update', 'MentorCouponController@update')->name('coupon-ajax.update');
Route::delete('coupon-ajax/destroy/{id}', 'MentorCouponController@destroy')->name('coupon-ajax.destroy');


Route::get("user/commission","AdminCommisionController@index");
Route::post('user/update-commission', 'AdminCommisionController@update');


Route::get("user/wallet","MentorWallet@index");

Route::post("/user/withdrawrequest", "MentorWithdrawalController@request");

Route::get('/admin/withdrawal/{id}/update', 'AdminWithdrawalController@setwallet');
Route::get('/course/{id}/watch', "UserCoursesController@show");

Route::get('/user/course/pending', 'AdminCourseController@pending');
Route::get('/user/course/add', 'AdminCourseController@postAdd');
Route::post('/user/course/{id}/update', 'AdminCourseController@postEdit');
Route::get('/user/revenue', 'AdminRevenueController@index');
Route::get('/user/user', 'AdminUserController@index');

Route::get('/user/course/{id}/takedown','AdminCourseController@takedown');

    Route::get('{id}/fileupload', 'FileUploadController@fileUpload');
    Route::get('/user/section/{id}/video', 'FileUploadController@fileUpload');
Route::post('fileupload', 'FileUploadController@fileStore');

Route::get('/verify/{token}','PublicController@postContinueRegister');
Route::get('/user/tnc','TncPengajarController@index');
Route::post('/user/tnc','TncPengajarController@update');


Route::get('/user/user/{id}/disable','AdminUserController@disable');
Route::get('/user/user/{id}/enable','AdminUserController@enable');

Route::delete('/user/course/destroy-thumbnail/{id}', 'AdminCourseController@destroyThumbnail')->name('course.destroy.thumbnail');
Route::delete('/user/course/destroy-teaser/{id}', 'AdminCourseController@destroyTeaser')->name('course.destroy.teaser');

Route::post('/user/course/sort-content','MentorCurriculumSectionController@sortContent');
Route::get('/user/payment_options/{id}/update','AdminPaymentOptionsController@update');
Route::get('/user/course/all', 'AdminCourseController@all');

Route::get('/user/curriculum/move-up/{id}', 'AdminCurriculumController@getNewSort');
Route::get('/user/curriculum/move-down/{id}', 'AdminCurriculumController@getNewSort');

Route::get('/user/curriculum-section/move-up/{id}', 'AdminCurriculumSectionController@getNewSort');
Route::get('/user/curriculum-section/move-down/{id}', 'AdminCurriculumSectionController@getNewSort');