@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')

      <div class="row">
        <div class="col-md-12 blog-main" >

<div class="box box-default">
        <div class="box-header">
            <h1 class="box-title"></h1>
        </div>
        <div class="box-body">
           <form action="{{ url('/user/tnc') }}" method="POST">
            @csrf
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Aggrement</label>
    <textarea  class="form-control" name="content" id="content" rows="5">
{{ $tnc->content }}

    </textarea>
  </div>

             <div class="pull-right">
                <button class="btn btn-primary">Save</button>    
            </div>         
           </form>
        </div>
    </div>

@push('bottom')
  <script src="{{ asset('/cb_asset/adminlte/plugins/summernote/summernote-bs4.min.js') }}"> </script>
<script>

</script>    

<script type="text/javascript">
  $(document).ready(function() {
    $('#content').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 
</script>
@endpush

@endsection