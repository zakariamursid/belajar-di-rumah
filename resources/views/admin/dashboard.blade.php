@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ $count_course }}</h3>

                <p>Courses</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                {{-- <h3>53<sup style="font-size: 20px">%</sup></h3> --}}
                <h3>{{ $count_mentor }}</h3>

                <p>Mentor</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                        <h3><?= "Rp " . number_format($count_mentor_revenue,0,',','.');  ?></h3>

                <p>Mentor Revenue</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>

            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                        <h3><?=  "Rp " . number_format($count_admin_revenue,0,',','.');  ?></h3>

                <p>Admin Revenue</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>

            </div>
          </div>


      <div class="row">
        <div class="col-md-12 blog-main" >
                <h3 class="mt-0"><b>Last Course Created</b></h3>

<div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Show Data</h1>
        </div>
        <div class="box-body">
            <table id="course" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Course</th>
                        <th>Harga</th>
                        <th>Mentor</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($course as $item)
                    <tr>
                        <td> {{ $item->course_created }} </td>
                        <td> {{ $item->course_title }} </td>
                        <td><?=  "Rp " . number_format($item->course_price,0,',','.');  ?></td>
                        <td> {{ $item->mentor_name }} </td>
                        {{-- <td>{{ $item->title }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->title }}</td> --}}
                    </tr>  
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

      <div class="row">
        <div class="col-md-12 blog-main" >
                <h3 class="mt-0"><b>Last User Join</b></h3>

<div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Show Data</h1>
        </div>
        <div class="box-body">
            <table id="user" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Role</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($user as $item)
                    <tr>
                        <td> {{ $item->created_at }} </td>
                        <td> {{ $item->name }} </td>
                        <td> {{ $item->role }} </td>
                        {{-- <td>{{ $item->title }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->title }}</td> --}}
                    </tr>  
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    </div>
    </div>

      <div class="row">
        <div class="col-md-12 blog-main" >
                <h3 class="mt-0"><b>Last Enroll</b></h3>

<div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Show Data</h1>
        </div>
        <div class="box-body">
            <table id="enroll" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Buyer</th>
                        <th>Price</th>
                        <th>Payment Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($enroll as $item)
                    <tr>
                        <td> {{ $item->created_at }} </td>
                        <td> {{ $item->buyer_name }} </td>
                        <td> {{ $item->grand_total }} </td>
                        <td> {{ $item->payment_status }} </td>
                        {{-- <td>{{ $item->title }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->title }}</td> --}}
                    </tr>  
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    </div>
    </div>

@push('bottom')
<script>
$(document).ready(function() {
    $('#course').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    $('#user').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    $('#enroll').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
} );  
</script>      
@endpush

@endsection