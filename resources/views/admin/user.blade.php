@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
    
{{-- @dd($users); --}}
 <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">All User</h1>
        </div>
        <div class="box-body">
            <table id="user" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Status</th>
                        <th>Role</th>
                        <th>Uploaded</th>
                        <th>Revenue</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($users as $item)
                      <tr>
                          <td>{{ $item->created_at }}</td>
                          <td>{{ $item->name }}</td>
                          <td>{{ $item->email }}</td>
                          <td>{{ $item->status }}</td>
                          <td>{{ $item->role }}</td>
                          <td>{{ $item->upload }}</td>
                          <td><?php echo  "Rp " . number_format($item->sum,0,',','.');  ?></td>
                          <td>
                            <?php
                                if($item->status == "Aktif")
                                {
                                    echo
                                    '
                                         <a href="'?>{{ url('/user/user/'.$item->id.'/disable') }}<?php echo'"><button class="btn btn-danger btn-sm">Disable</button></a> 
                                    ';
                                }
                                else
                                {
                                    echo
                                    '
                                         <a href="'?>{{ url('/user/user/'.$item->id.'/enable') }}<?php echo'"><button class="btn btn-success btn-sm">Enable</button></a> 
                                    ';
                                }


                                ?>

                          </td>
                      </tr>
                  @endforeach
                  
                </tbody>
            </table>
        </div>
    </div>

@push('bottom')
<script>
$(document).ready(function() {
    $('#course').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    $('#user').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
    $('#enroll').DataTable( {
        "order": [[ 0, "desc" ]]
    } );
} );  
</script>      
@endpush


@endsection