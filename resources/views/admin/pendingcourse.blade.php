@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
    

 <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Pending Course</h1>
        </div>
        <div class="box-body">
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Title</th>
                        <th>Mentor</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($course as $item)
                        <tr>
                            <td>{{ $item->course_created }}</td>
                            <td>{{ $item->course_title }}</td>
                            <td>{{ $item->mentor_name }}</td>
                            <td>
                                <a href="{{ url('user/course/'.$item->course_id.'/content') }}"><button class="btn btn-sm btn-warning">Content</button></a>
                                &nbsp;
                                
                                <a href="{{ url('user/course/'.$item->course_id.'/approve') }}"><button class="btn btn-sm btn-success">Approve</button></a>                                
                                <!--<form action="{{ url('user/course/change-status')  }}" method="POST">-->
                                <!--    @csrf-->
                                    <!--<input type="text" name="course_id" hidden value="{{ $item->course_id }}">-->
                                    <!--<input type="text" name="status" hidden value="Approved">-->
                                    <!--<button class="btn btn-sm btn-success" type="submit">Approve</button>-->
                                <!--</form>-->
                            </td>
                        </tr>
                    @endforeach
                  
                </tbody>
            </table>
        </div>
    </div>



@endsection