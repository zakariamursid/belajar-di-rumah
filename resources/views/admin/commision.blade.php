@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')

      <div class="row">
        <div class="col-md-12 blog-main" >

<div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Pembagian Penghasil</h1>
        </div>
        <div class="box-body">
           <form action="{{ url($post) }}" method="POST">
            @csrf
                <div class="form-group">
                <label for="">Admin</label>
            {{-- <input type="text" name="admin" hidden value="1"> --}}
                <input required type="number" placeholder="%" name="admin" class="form-control" value="{{ $commission->admin }}">
            </div>
           <div class="form-group">
                <label for="">Pengajar</label>
                <input type="number" name="mentor" class="form-control" placeholder="%"  value="{{ $commission->mentor }}">
            </div>
            <div class="pull-right">
                <button class="btm btn-primary">Save</button>    
            </div>         
           </form>
        </div>
    </div>

    </div>
    </div>



@endsection