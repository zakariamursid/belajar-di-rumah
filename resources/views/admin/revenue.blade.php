@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
    

 <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">All Revenue</h1>
        </div>
        <div class="box-body">
            <table id="revenue" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>User</th>
                        <th>Course</th>
                        <th>Status</th>
                        <th>Pay</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $item)
                        <tr>
                            <td>{{ $item->created_at  }}</td>
                            <td>{{ $item->buyer_name }}</td>
                            <td>{{ $item->course_title }}</td>
                            <td <?php if($item->status == "Success") { echo "bgcolor=#b9ebcc"; }   else if($item->status == "Failed") { echo "bgcolor=#ffd6d6"; } else if($item->status == "Pending") { echo "bgcolor=#fcf7bb"; }   ?>>{{ $item->status }}</td>
                            <td><?=  "Rp " . number_format($item->grand_total,0,',','.');  ?></td>
                        </tr>
                    @endforeach
                  
                  
                </tbody>
            </table>
        </div>
    </div>

@push('bottom')
<script>
$(document).ready(function() {

    $('#revenue').DataTable( {
        "order": [[ 0, "desc" ]]
    } );

} );  
</script>      
@endpush


@endsection
