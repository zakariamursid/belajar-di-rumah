@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
    

 <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">All Course</h1>
        </div>
        <div class="box-body">
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Course</th>
                        <th>Pengajar</th>
                        <th>Status</th>
                        <th>Enroll</th>
                        <th>Earn</th>
                        <th>Action</th>
                    </tr>
                </thead>
                {{-- @dd($course) --}}
                <tbody>
                      @foreach($course as $item)
                    <tr>
                        <td>{{ $item->course_created }}</td>
                        <td>{{ $item->course_title }}</td>
                        <td>{{ $item->mentor_name }}</td>
                        <td>{{ $item->course_status }}</td>
                        <td>{{ $item->enroll }}</td>
                        <td><?=  "Rp " . number_format($item->sum,0,',','.');  ?></td>
                        <td><a href="{{ url("user/course/".$item->course_id."/content") }}"><button type="submit" class="btn btn-sm btn-info">Content</button></a>
                        @if($item->course_status == "Approved")
                            <a href="{{ url("user/course/".$item->course_id."/takedown") }}"><button type="submit" class="btn btn-sm btn-danger" style="height:30px;width:70px">Takedown</button></a>
                        @elseif($item->course_status == "Takedown")
                            <a href="{{ url("user/course/".$item->course_id."/approve") }}"><button type="submit" class="btn btn-sm btn-success"  style="height:30px;width:70px">Publish</button></a>
                        @endif
                    </td>
                    </tr>
                    @endforeach
                  
                </tbody>
            </table>
        </div>
    </div>

@endsection