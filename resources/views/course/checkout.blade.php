{{-- @dd($data); --}}
@extends('layouts.topbar')
@section('content')
 <main role="main">

     <main role="main" class="container">
      <div class="row">
        <div class="col-md-8 blog-main" >
<div class="media">
  <div class="media-body">
    <b> Course to Buy </b>
    <br>
    <br>
    <h5 class="mt-0">{{ $data['course']->title }}</h5>
    Pengajar : {{ $data['course']->mentor_name }}
    <hr>

    <div>
       <b> Please Following this instruction to complete payment </b>
        <br>
        <br>
        <table>
            <tr>
                <td>Atas Nama </td>
                <td>: PT Taman Media Indonesia</td>
            </tr>
            <tr>
                <td>Nomor Rekening </td>
                <td>: 123456789</td>
            <tr>
                <td>Bank </td>
                <td>: Mandiri</td>
            </tr>
            <tr>
                <td>Cabang </td>
                <td>: Banyumanik</td>
            </tr>
        </table>

        <br>
        After Your payment process, please fill this Confirmation Form below
        <br>
        <br>
        <b>Your Account</b>
         <table>
            <tr>
                <td>Nama </td>
                <td>: PT Taman Media Indonesia</td>
            </tr>
            <tr>
                <td>E-mail </td>
                <td>: 123456789</td>
            </tr>
        </table>
        
        <br>
        <b>Informasi Transfer</b>
        <table>
            <tr>
                <td>Dari Bank </td>
                <td>: </td>
            </tr>
            <tr>
                <td>Cabang </td>
                <td>: <input type="text" name="branch"></td>
            <tr>
                <td>No. Rekening </td>
                <td>: <input type="number" name="account_number"></td>
            </tr>
            <tr>
                <td>Nominal </td>
                <td>: <input type="number" name="nominal"></td>
            </tr>
            <tr>
                <td>Bukti Transfer </td>
                <td><input type="file" name="proof"></td>
            </tr>
        </table>
        <br>
            <button type="submit" class="btn-primary">SUBMIT</button>

        <br>
        
    </div>

</div> 





</div>

        </div><!-- /.blog-main -->

        <aside class="col-md-4 blog-sidebar">
            <div>
                <b>Summary</b>
                <br>
        <table>
            <tr>
                <td>Price </td>
                <td>: </td>
            </tr>
            <tr>
                <td>Coupon Discounts </td>
                <td>: </td>
            <tr>
                <td>Bank </td>
                <td>: </td>
            </tr>
            <tr>
                <td>Unique Code </td>
                <td>: </td>
            </tr>
        </table>  
        <br>
            <button class="btn-danger" style="width: 100%">Complete Payment</button>
              
            </div>
        </aside><!-- /.blog-sidebar -->
        

      </div><!-- /.row -->
    </div>


        </div>
      </div>

    </main>
    @endsection