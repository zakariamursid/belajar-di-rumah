{{-- @dd($data); --}}
@extends(getThemePath("layout.layout"))
@section('content')
 <main role="main">

     <main role="main" class="container">
      <div class="row">
        <div class="col-md-8 blog-main" >
<div class="media">
  <div class="media-body">
     <aside class="col-md-12 blog-sidebar">
          <div class="p-3 mb-3 bg-light rounded">
            <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
</div>
            {{-- <h4 class="font-bold">Rp. {{ $data['course']->price }}</h4> --}}
            <p class="mb-0">
              <div>
                {{-- <button class="btn-danger" style="width: 100%">Buy Now</button> --}}
              </div>

            </p>
          </div>

       
        </aside><!-- /.blog-sidebar -->

    <h5 class="mt-0"><b>{{ $course->title }}</b></h5>
    {{ $course->short_desc }}
    <br>
    <br>

<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Description</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Prepare</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Course Content</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">Consequat occaecat ullamco amet non eiusmod nostrud dolore irure incididunt est duis anim sunt officia. Fugiat velit proident aliquip nisi incididunt nostrud exercitation proident est nisi. Irure magna elit commodo anim ex veniam culpa eiusmod id nostrud sit cupidatat in veniam ad. Eiusmod consequat eu adipisicing minim anim aliquip cupidatat culpa excepteur quis. Occaecat sit eu exercitation irure Lorem incididunt nostrud.

</div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">Consequat occaecat ullamco amet non eiusmod nostrud dolore irure incididunt est duis anim sunt officia. Fugiat velit proident aliquip nisi incididunt nostrud exercitation proident est nisi. Irure magna elit commodo anim ex veniam culpa eiusmod id nostrud sit cupidatat in veniam ad. Eiusmod consequat eu adipisicing minim anim aliquip cupidatat culpa excepteur quis. Occaecat sit eu exercitation irure Lorem incididunt nostrud.

</div>
  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">Consequat occaecat ullamco amet non eiusmod nostrud dolore irure incididunt est duis anim sunt officia. Fugiat velit proident aliquip nisi incididunt nostrud exercitation proident est nisi. Irure magna elit commodo anim ex veniam culpa eiusmod id nostrud sit cupidatat in veniam ad. Eiusmod consequat eu adipisicing minim anim aliquip cupidatat culpa excepteur quis. Occaecat sit eu exercitation irure Lorem incididunt nostrud.

</div>
</div> 


  </div>



</div>

        </div><!-- /.blog-main -->

        <aside class="col-md-4 blog-sidebar">
<div class="panel-group" id="accordion2">
		<div class="card">
            @foreach ($content as $item)
                
			<div class="card-header" id="subheadingOne">
				<h5 class="mb-0">
					<button class="btn btn-link" data-toggle="collapse" data-target="#s{{ $item->id }}" aria-expanded="true" aria-controls="collapseOne">
						{{ $item->title }}
					</button>
				</h5>
			</div>

			<div id="s{{ $item->id }}" class="collapse" aria-labelledby="subheadingOne" data-parent="#accordion2">
				<div class="card-body">
                {{-- {{ $item->syllabusitem->title }} --}}
                @foreach ($item->section as $itm)
                <a href="">{{ $itm->title }}</a>
                <br>
                @endforeach
				</div>
            </div>
            
            @endforeach

		</div>


	</div>
       
        </aside><!-- /.blog-sidebar -->
        

      </div><!-- /.row -->
    </div>


        </div>
      </div>

    </main>
    @endsection