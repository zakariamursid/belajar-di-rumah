@extends(getThemePath("layout.layout"))
@section("content")


    <p>
        <a href=""><i class="fa fa-arrow-left"></i> Back To List</a>
    </p>

    <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Add Course</h1>
        </div>
        <form enctype="multipart/form-data" action="{{ url($url) }}" method="POST">

    {{-- <form action="{{  }}" method="POST" enctype="multipart/form-data" class="box no-padding"> --}}
    {{-- <form action="{{module()->addSaveURL()}}" method="POST" enctype="multipart/form-data" class="box no-padding"> --}}
            {!! csrf_field() !!}
        <div class="box-body">
            <div class="form-group">
                <label for="">Course Title</label>
            <input type="text" name="mentor_id" hidden value="1">
                <input required type="text" placeholder="E.g : John Doe" name="title" class="form-control">
            </div>
           <div class="form-group">
                <label for="">Price</label>
                <input type="number" min="0" name="price" class="form-control" placeholder="Price">
            </div>            
            <div class="form-group">
                 <label for="">Short Desc</label>
                  <textarea name="short_desc" id="short_desc" required class='form-control' placeholder="Short Desc"
                  rows='2'></textarea>
            </div>
            <div class="form-group">
                 <label for="">Full Desc</label>
                  <textarea name="desc" id="desc" required class='form-control' placeholder="Full Desc"
                  rows='5'></textarea>
            </div>
           {{-- <b> Basic Info </b> --}}
            <div class="form-group">
                <label for="">Category</label>
                <select name="topic_id" class="form-control">
                    <option value="">** Select a Category</option>
                    @foreach($topics as $role)
                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="">Skill Level</label>
                <select name="level" class="form-control">
                    <option value="">** Select a Skill Level</option>
                    @foreach($levels as $level)
                        <option value="{{ $level }}">{{ $level }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="">Banner</label>
                <input type="file" name="banner" accept="image/jpeg,image/png,image/jpg" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Demo Video</label>
                <input type="file" name="banner" class="form-control">
            </div>
 
        </div>
        <div class="box-footer">
            <input type="submit" class="btn btn-success" value="Next">
        </div>
        </form>
    </div>


@endsection