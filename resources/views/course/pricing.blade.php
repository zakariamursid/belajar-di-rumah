@extends("crudbooster::dev_layouts.layout")
@section("content")


    <p>
        <a href=""><i class="fa fa-arrow-left"></i> Back To List</a>
    </p>

    <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Add Pricing</h1>
        </div>
        {{-- <form enctype="multipart/form-data" action="{{ url($url) }}" method="POST"> --}}
        <form enctype="multipart/form-data" action="{{ url('/mentor/course/'.$course->id.'/pricing/add') }}" method="POST">

    {{-- <form action="{{  }}" method="POST" enctype="multipart/form-data" class="box no-padding"> --}}
    {{-- <form action="{{module()->addSaveURL()}}" method="POST" enctype="multipart/form-data" class="box no-padding"> --}}
            {!! csrf_field() !!}
        <div class="box-body">
            <input type="text" name="mentor_id" value="{{ $course->mentor_id }}" name="mentor_id">
            <div class="form-group">
                <label for="">Price</label>
                <input required type="number" name="price" class="form-control" value="{{ $course->price }}">
            </div>

            <div class="form-group">
                <label for="">Coupon</label>
                <input  required type="text" maxlength="10" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="code">
</div>

<div class="form-check">
  <input class="form-check-input" type="radio" name="discount" id="discount" value="fix" checked>
  <label class="form-check-label-fix" for="discount">
    Fix Cost
  </label>
    <input class="form-check-input" type="radio" name="discount" id="discount" value="percentage" checked>

  <label class="form-check-label-persentage" for="discount">
    Persentage
  </label>
      <input required type="number" name="amount" class="form-control" placeholder="Amount">
      <br>

    </div>

         <div class="form-group">
                <label for="">QTY</label>
                <input required type="number" name="qty" class="form-control">
            </div>

         <div class="form-group">
                <label for="">Expired</label>
                <input required type="date" name="valid_until" class="form-control">
            </div>
    
    <br>
<div class="form-group pull-right">
                        <input type="submit" class="btn btn-success" value="Generate">

        </div>

        </form>

     {{-- <div class="box-footer">
            <input type="submit" class="btn btn-success" value="Next">
        </div> --}}
        
    </div>
    </div>
    

    
 <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Coupon Exist</h1>
        </div>
        <div class="box-body">
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>Date Created</th>
                        <th>Coupon Code</th>
                        <th>Quantity</th>
                        <th>Expired Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($coupons as $coupon)
                    <tr>
                        <td>{{ $coupon->created_at }}</td>
                        <td>{{ $coupon->code }}</td>
                        <td>{{ $coupon->qty }}</td>
                        <td>{{ $coupon->valid_until }}</td>
                        <td>
                            {{-- <a href="{{ cb()->getDeveloperUrl("users/edit/".$row->id) }}" class="btn btn-warning btn-sm">Edit</a>
                            <a href="{{ cb()->getDeveloperUrl("users/delete/".$row->id)}}" onclick="if(!confirm('Are you sure want to delete?')) return false" class="btn btn-danger btn-sm">Delete</a> --}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


    

@endsection