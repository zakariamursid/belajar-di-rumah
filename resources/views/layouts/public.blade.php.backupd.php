<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *Must* come first in the head; any other head content must come *after* these tags -->
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Title -->
    <title>Crocodic Course</title>

    <!-- Favicon -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="{{ asset('clever_asset/style.css') }}">

</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="spinner"></div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Navbar Area -->
        <div class="clever-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="cleverNav">

                    <!-- Logo -->
                    {{-- <a class="nav-brand" href="index.html"><img src="img/core-img/logo.png" alt=""></a> --}}
                    <a class="nav-brand" href="{{ url('/') }}"><img src="{{ asset('clever_asset/img/core-img/logo.png')}}" alt="" width="5%"></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">

                        <!-- Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>

                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul>

                                <li><a href="{{ url('/') }}">Home</a></li>
                                <li><a href="{{ url('/course') }}">Course</a></li>
                            </ul>



                        @guest
                        
                            <!-- Register / Login -->
                            <div class="register-login-area">
                                <a href="{{ url('user/login') }}" class="btn">Register</a>
                                <a href="{{ url('user/login') }}"class="btn active">Login</a>
                            </div>
                            @else


                            <!-- Register / Login -->
                            <div class="login-state d-flex align-items-center">
                                <div class="user-name mr-30">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" href="#" role="button" id="userName" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> {{ cb()->session()->name() }} </a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userName">
                                            {{-- <a class="dropdown-item" href="#">Profile</a> --}}
                               <?php 
                             if (cb()->session()->roleId() == 1)
                             { 
                              echo'<a class="dropdown-item" href="'?> {{ url('/user/') }} <?php echo'">Dashboard</a>'  ;
                             }

                             else if (cb()->session()->roleId() == 2)
                             { 
                              echo'<a class="dropdown-item" href="'?> {{ url('/user/') }} <?php echo'">Dashboard</a>'  ;
                             }

                             else if (cb()->session()->roleId() == 3)
                             { 
                              echo'<a class="dropdown-item" href="'?> {{ url('/bementor?user_id='.$session->id) }} <?php echo'">Menjadi Pengajar</a>'  ;
                             }

                              ?>                                                                               
                                            <a class="dropdown-item" href="{{ url('/order') }}">My Course</a>
                                            <a class="dropdown-item" href="{{ cb()->getLogoutUrl() }}">Logout</a>                         
                                        </div>
                                    </div>
                                </div>
{{--                                 <div class="userthumb">
                                    <img src="img/bg-img/t1.png" alt="">
                                </div> --}}
                            </div>
                            @endguest


                        </div>
                        <!-- Nav End -->
    

                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->

                @yield('content')


    {{-- <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <!-- Top Footer Area -->
        <div class="top-footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">

                        <!-- Copywrite -->
                        <p><a href="#">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>
                                    document.write(new Date().getFullYear());
                                </script> All rights reserved | This template is made with <i class="fa fa-heart-o"
                                    aria-hidden="true"></i> by <a href="https://colorlib.com"
                                    target="_blank">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
 --}}

    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- ##### All Javascript Script ##### -->
    <!-- jQuery-2.2.4 js -->
    <script src="{{asset('clever_asset/js/jquery/jquery-2.2.4.min.js')}}"></script>
    <!-- Popper js -->
    <script src="{{asset('clever_asset/js/bootstrap/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('clever_asset/js/bootstrap/bootstrap.min.js')}}"></script>
    <!-- All Plugins js -->
    <script src="{{asset('clever_asset/js/plugins/plugins.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('clever_asset/js/active.js')}}"></script>

        @stack('bottom')

</body>

</html>