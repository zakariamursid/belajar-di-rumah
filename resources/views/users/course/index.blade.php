@extends('layouts.public')
@section('content')
  <!-- ##### Blog Area Start ##### -->
    <section class="blog-area blog-page section-padding-100">
        <div class="container-fluid">
 

            <div class="row">
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-9">
                    <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="250ms">
                        <div class="blog-content">
                        <div class="embed-responsive embed-responsive-16by9">

                              <video controls>
                                      <source src="<?php if($section_id != '') { echo asset($section->video); } else { echo asset($course->teaser);} ?>" type="video/mp4">
                                      Your browser does not support the video tag.
                                  </video>   
                        </div>
                        </div>
                      
                        <!-- Blog Content -->
                        <div class="blog-content">

                                <h4>{{ $course->title }}</h4>
                            <div class="meta d-flex align-items-center">
                                <a href="#">{{ $course->mentor_name }}</a>
                                {{-- <span><i class="fa fa-circle" aria-hidden="true"></i></span> --}}
                                {{-- <a href="#">Art &amp; Design</a> --}}
                            </div>

                                                            <br>
                            <?php if($section_id != '') { echo "Content : " .$section->title; }?><br>
                            <p><?php if($section_id != '') { echo "Deskripsi : " . $section->desc; } else { echo $course->desc;} ?></p>
                        </div>
                    </div>
                </div>

                <!-- Single Blog Area -->
                <div class="col-12 col-lg-3 ">
                    @if($validasi_order == 0)
                    <div class="row align-items-center justify-content-center">
                                <a href="{{ url('course/'.$course->id.'/checkout') }}" class="btn clever-btn mb-30" style="width:80%">Beli <?=  "Rp " . number_format($course->price,0,',','.');  ?></a>
                        </div>
                    @endif

                    <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="500ms">
                        <!-- Blog Content -->
                        <div class="blog-content">
                                <h4>Content</h4>
                            <br>
                                                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis</p> --}}
                            @foreach ($content as $item)
                                        
                                    <div class="card-header" id="subheadingOne">
                                        <h5 class="mb-0">
                                                {{ $item->title }}
                                
                                            </h5>
                                    </div>

                                    <div id="s{{ $item->id }}" class="collapse show" aria-labelledby="subheadingOne" data-parent="#accordion2">
                                        <div class="card-body">
                                        {{-- @dd($section_id); --}}

                                        {{-- {{ $item->syllabusitem->title }} --}}
                                        @foreach ($item->section as $itm)
                                            
                                            @if($item->access == 'PUBLIC' || $item->access == 'PRIVATE' && $validasi_order == 1) 
                                            <a href="{{ url('course/'.$course->id.'/watch?content=').$itm->id }}"><font color="<?php if($section_id != '') { if($itm != null) { if($itm->title == $section->title) { echo 'FF00CC'; } else { echo '000000';} } }  ?>">{{ $itm->title }}
                                            @else
                                            <a href="#"><font color="<?php if($section_id != '') { if($itm != null) { if($itm->title == $section->title) { echo 'FF00CC'; } else { echo '000000';} } }  ?>">{{ $itm->title }}
                                            @endif

                                        
                                        @if($item->access != 'PUBLIC' && $validasi_order == 0) <i class="fa fa-lock" aria-hidden="true"></i>@endif
                                        </font></a> <br>
                                        {{-- <a href="{{ url('users/orders/detail/'.$course->id.'/content?id=') }}">{{ $itm->title }}</a> --}}
                                        <br>
                                        @endforeach
                                        </div>
                                    </div>
                                    
                            @endforeach                            
                        </div>
                    </div>
                </div>
                
            </div>

   
        </div>
    </section>
    <!-- ##### Blog Area End ##### -->

@endsection