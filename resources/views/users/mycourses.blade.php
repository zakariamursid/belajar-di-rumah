{{-- @dd($order_history); --}}
@extends('layouts.public')
@section('content')

    <!-- ##### Blog Area Start ##### -->
    <section class="blog-area blog-page section-padding-100">
        <div class="container-fluid">

            <div class="row">
                <!-- Single Blog Area -->
                            <div class="col-12 col-lg-12">


                <h2>Purchased Course</h2>
                <br>
                
                <div class="row">

            @foreach ($success_order as $item)
                
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-3">
                    <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="250ms">
                       <a href="{{ url('course/'.$item->id.'/watch') }}"> <img src="{{ asset($item->poster) }}" alt=""></a>
                       <br>
                        <div class="blog-content">

                    <div style="min-height: 80px">
                      <b> {{ $item->title }} </b>
                    </div>
                    {{ $item->mentor_name }} <br>
                        <?php echo  "Rp " . number_format($item->price,0,',','.');  ?> <br>
                        {{ $item->order_status }}
                    </div>
                    </div>
                </div>

            @endforeach
  
                

               
        </div>

                <h2>Orders</h2>
                <br>
                
                <div class="row">

                @foreach ($pending_order as $item)
                
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-3">
                    <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="250ms">
                       <a href="{{ url('order/'.$item->order_id) }}"> <img src="{{ asset($item->poster) }}" alt=""></a>
                       <br>
                        <div class="blog-content">

                    <div style="min-height: 80px">
                      <b> {{ $item->title }} </b>
                    </div>
                    {{ $item->mentor_name }} <br>
                        <?php echo  "Rp " . number_format($item->price,0,',','.');  ?> <br>
                        {{ $item->order_status }}
                    </div>
                    </div>
                </div>

            @endforeach
            
                

               
        </div>
    </section>
   
    <!-- ##### Popular Courses End ##### -->




   
@endsection