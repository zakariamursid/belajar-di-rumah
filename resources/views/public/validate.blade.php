{{-- @dd($order); --}}
@extends('layouts.topbar')
@section('content')
 <main role="main">

     <main role="main" class="container">
      <div class="row">
        <div class="col-md-8 blog-main" >
<div class="media">
  <div class="media-body">
    <b> Course to Buy </b>
    <br>
    <br>
    <h5 class="mt-0">{{ $order_detail->title }}</h5>
    Pengajar : {{ $order_detail->mentor_name }}
    <hr>

    <div>
       <b> Please Following this instruction to complete payment </b>
        <br>
        <br>
        <table>
            <tr>
                <td>Grand Total </td>
                <td>:  <?=  "Rp " . number_format($order->grand_total,2,',','.'); ?> </td>
            </tr>
            <tr>
                <td>Atas Nama </td>
                <td>: PT Taman Media Indonesia</td>
            </tr>
            <tr>
                <td>Nomor Rekening </td>
                <td>: 123456789</td>
            <tr>
                <td>Bank </td>
                <td>: Mandiri</td>
            </tr>
            <tr>
                <td>Cabang </td>
                <td>: Banyumanik</td>
            </tr>
        </table>

        <br>
        After Your payment process, please fill this Confirmation Form below
        <br>
        <br>
        <b>Your Account</b>
         <table>
            <tr>
                <td>Nama </td>
                <td>: PT Taman Media Indonesia</td>
            </tr>
            <tr>
                <td>E-mail </td>
                <td>: 123456789</td>
            </tr>
        </table>
        
        <br>
        <b>Informasi Transfer</b>
                <form action="{{ url('/order/'.$order->id.'/validate') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="text" name="order_id" value="{{ $order->id }}" hidden>
        <table>
            <tr>
                <td>Dari Bank </td>
                <td>: <select name="bank" id="">
                    @foreach ($bank as $item)
                        <option value="{{ $item->id }}"> {{ $item->name }} </option>
                    @endforeach    
                </select> </td>
            </tr>
            <tr>
                <td>Cabang </td>
                <td>: <input type="text" name="branch"></td>
            <tr>
                <td>No. Rekening </td>
                <td>: <input type="number" name="account_number"></td>
            </tr>
            <tr>
                <td>Atas Nama </td>
                <td>: <input type="text" name="account_name"></td>
            </tr>
            <tr>
                <td>Nominal </td>
                <td>: <input type="number" name="nominal"></td>
            </tr>
            <tr>
                <td>Bukti Transfer </td>
                <td><input type="file" name="proof"></td>
            </tr>
        </table>
        <br>
            <button type="submit" class="btn-primary">SUBMIT</button>
        <br>
                </form>
        
    </div>

</div> 





</div>

        </div><!-- /.blog-main -->

     

      </div><!-- /.row -->
    </div>


        </div>
      </div>

    </main>
    @endsection