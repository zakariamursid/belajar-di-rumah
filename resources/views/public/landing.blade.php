@extends('layouts.front')
@section('content')
@push('top')
 <style type="text/css">
.bgconnected {
    background-image: url('{{ asset('clever_asset/img/landing/bg_connected.png')}}');
}


.bgImgCenter {
    background-image: url('clever_asset/img/landing/bg_connected.png');

            background-repeat: no-repeat;
            background-position: center;
            position: relative;
    }
</style>   
@endpush
<br>
    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area bg-img " style="background-image: url({{ asset('asset/img/landing/bg_image_slider.png')}});">
        <div class="container h-100">
            {{-- <div class="row h-100 align-items-center"> --}}
                <div class="col-12">
                    <!-- Hero Content -->
                    <div class="hero-content">
                        <h4>Kami membantu kamu untuk belajar langsung dari ahlinya dengan materi yang sesuai dengan standar Industri.</h4>
                       <div class="row align-items-center justify-content-center">
                            <a href="{{ url('/user/login') }}" class="btn register-btn">Register</a>
                            <a href="#learn" class="btn learn-btn">Learn More</a>
                        </div>
 
                        

                    </div>
                </div>

            </div>
            
        </div>
    <div id="learn">

    </section>

    <div class="row align-items-center justify-content-center" style="margin-top: 100px;">
         <h3 class="text-center">
                Alasan kenapa harus Belajar dirumah
         </h3>    
    </div>
    </div>


<div class="container" style="padding-top: 100px;">
  <div class="row" style="margin-left:10px; margin-right:10px;">
    <div class="col-sm" style="margin-bottom:100px">
        <div class="row align-items-center justify-content-center" style="padding-bottom: 50px">
        <img src="{{ asset('asset/img/landing/ic_ilustrasi_1.png') }}" alt="">
        </div>
    <h6 class="text-center">Kamu dapat memilih pengajar, metode, dan materi yang diminati sesuai dengan kebutuhan Kamu</h6>
    </div>
    <div class="col-sm" style="margin-bottom:100px">
        <div class="row align-items-center justify-content-center" style="padding-bottom: 50px">
        <img src="{{ asset('asset/img/landing/ic_ilustrasi_2.png') }}" alt="">
        </div>
    <h6 class="text-center">Kamu dapat memilih kegiatan pembelajaran dengan siapapun tanpa dibatasi tempat, umur, waktu, dan jumlah teman belajar</h6>
    </div>
    <div class="col-sm" style="margin-bottom:100px">
        <div class="row align-items-center justify-content-center" style="padding-bottom: 50px">
        <img src="{{ asset('asset/img/landing/ic_ilustrasi_3.png') }}" alt="">
        </div>
    <h6 class="text-center">Kurikulum dan pembelajaran berorientasi life skill, sehingga pelajar dapat mencapai kemandirian sejak dini</h6>
    </div>
  </div>
</div>


<div style=" background-color:#78A1D4;">
    <div class="row align-items-center justify-content-center" style="padding-top: 100px;">
         <h3 class="text-center" style="color: white; padding-left:50px; padding-right:50px;">
           Kami siap menjadi teman belajar Kamu
         </h3>    
    </div>
 
 
<div class="container" style="margin-top: 100px;  padding-bottom:100px">
  <div class="row">

    
    <div class="col-sm" style="margin:10px;">
        <div class="row align-items-center justify-content-center">
        <img src="{{ asset('asset/img/landing/ic_mas_ferry_course.png') }}" alt="">
        </div>
    </div>
    <div class="col-sm" style="margin:10px">
        <div class="row align-items-center justify-content-center">
        <img src="{{ asset('asset/img/landing/ic_mas_yasid_course.png') }}" alt="">
        </div>
    </div>
    <div class="col-sm" style="margin:10px">
        <div class="row align-items-center justify-content-center">
        <img src="{{ asset('asset/img/landing/ic_mas_david_course.png') }}" alt="">
        </div>
    </div>
</div>
  </div>
</div>
 <div class="row align-items-center justify-content-center" style="margin-top: 100px;  margin-left:10px; margin-right:10px;">
         <h3 class="text-center">
            Anda seorang Guru atau Pengajar? <br> Punya konten/video/materi pembelajaran?
         </h3>    
    </div>
    </div>


<div class="container" style="margin-bottom:100px">
  <div class="row">
    <div class="col-sm" style="padding-top: 100px; ">
        <div class="row align-items-center justify-content-center" style="padding-bottom: 50px">
        <img src="{{ asset('asset/img/landing/ic_sharing.png') }}" alt="">
        </div>
    <h6 class="text-center">Bagikan Pengetahuan dan Keterampilan Anda</h6>
    </div>
    <div class="col-sm" style="padding-top: 100px; ">
        <div class="row align-items-center justify-content-center" style="padding-bottom: 50px">
        <img src="{{ asset('asset/img/landing/ic_join.png') }}" alt="">
        </div>
    <h6 class="text-center">Gabung ke komunitas sesuai dengan bidang Anda</h6>
    </div>
    <div class="col-sm" style="padding-top: 100px; ">
        <div class="row align-items-center justify-content-center" style="padding-bottom: 50px">
        <img src="{{ asset('asset/img/landing/ic_earning.png') }}" alt="">
        </div>
    <h6 class="text-center">Dapatkan penghasilan dari konten Anda</h6>
    </div>
  </div>
</div>


    <div class="row align-items-center justify-content-center" style="margin-bottom:100px">
                        <a href="{{ url('/user/login') }}" class="btn register-btn">Register</a> &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="#learn" class="btn learn-btn">Learn More</a>
    </div>

<!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <!-- Top Footer Area -->
        <div class="top-footer-area">
            <!-- Footer Logo -->
                        <div class="pull-right" style="padding-right: 200px;">
                                <a href="">About Us</a> <br>
                                <a href="">Terms & Condition</a> <br>
                                <a href="">Service</a> <br>
                        </div>              
                        <div class="pull-left" style="padding-left: 200px;">
                            <a href="index.html"><img src="{{ asset('clever_asset/img/landing/ic_logo.png') }}" alt=""></a>
 
                        </div>  

                                     <br>
                                     <br>
                                     <br>
        
        </div>
    </div>
    <footer class="footer-area">

        <div class="top-footer-area">
            <!-- Footer Logo -->

            <div class="container">
                <div class="row">
                    <div class="col-12">
                        
                        <!-- Copywrite -->
                        <p><a href="#"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
{{-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> --}}
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved Crocodic 
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- ##### Footer Area End ##### -->

@endsection