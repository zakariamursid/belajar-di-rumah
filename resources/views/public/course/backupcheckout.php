{{-- @dd($coupon) --}}
@extends('layouts.public')
@section('content')
    

<div class="regular-page-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-8">

                    <div class="page-content">
                        <h4>Your Course to Buy </h4>
                            <hr>

                            <p>
                            {{ $course->title }} <br>
                            Pengajar : {{ $course->mentor_name }} <br><br>
                            You have a Coupon? insert below <br><br>
                            </p>

                                        <table>
                                            <tr>
                                                <td>
                                            <input type="text" id="coupon" maxlength="10" onkeyup="this.value = this.value.toUpperCase();" class="form-control" <?php if(!empty($coupon)) { echo "value=".$coupon->code; } ?>>        
                                            </td>
                                                <td>

                                                    &nbsp;
                                                    <button class="btn clever-btn" onclick="redirect();">Check Coupon</button>
                                                    {{-- <input type="button" value="Check Coupon"  /> --}}
                                                </td>
                                            </tr>

                                        </table>
                                  
                                            

                        {{-- <p>Sed elementum lacus a risus luctus suscipit. Aenean sollicitudin sapien neque, in fermentum lorem dignissim a. Nullam eu mattis quam. Donec porttitor nunc a diam molestie blandit. Maecenas quis ultrices ex. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nullam eget vehicula lorem, vitae porta nisi. Ut vel quam erat. Ut vitae erat tincidunt, tristique mi ac, pharetra dolor. In et suscipit ex. Pellentesque aliquet velit tortor, eget placerat mi scelerisque a. Aliquam eu dui efficitur purus posuere viverra. Proin ut elit mollis, euismod diam et, fermentum enim.</p> --}}
                    </div>
                    </div>
                <div class="col-4">
                    <div class="page-content">

                    <h4>Summary</h4>
                            <hr>

                           <table>
                
                                    <tr>
                                        <td><p>Price</p> </td>
                                        <td><p> &nbsp; : &nbsp;<?=  "Rp " . number_format($course->price,0,',','.');  ?> </p></td>

                                    </tr>
                                    <tr>
                                        <td><p>Coupon Discount </p> </td>
                                        <td><p> &nbsp; : &nbsp;
                                            <?php if($coupon->discount == null) { echo "";} else if($coupon->discount == "NOTFOUND") { echo "Invalid Coupon";} else if($coupon->type == "-") { echo "- Rp " . number_format($coupon->discount,0,',','.') ;}  else if($coupon->type == "%") { $disc = $course->price * ($coupon->discount/100); echo "- Rp " . number_format($disc,0,',','.') ;} ?>                                        </p> </td>
                                    <tr>
                                        <td><p>Unique Code </p> </td>
                                        <td><p> &nbsp; : &nbsp;
                                        Rp. {{ $unique_code }}    
                                        </p> </td>
                                    </tr>
                                    <tr>
                                        <td><b>Grand Total</b></td>
                                        <td><b><p> &nbsp; : &nbsp; </b>
                             <?php if($coupon->discount == "NOTFOUND") { $discount = 0; }
                             else if($coupon->discount == null) { $discount = 0; }
                            //  dd($discount)
                             else {
                                if($coupon->type == "%")
                                { $discount = $course->price * ($coupon->discount/100);
                                }
                                else if($coupon->type == "-")
                                {  $discount = $coupon->discount;
                                }
                                }
                                $grand_total = $course->price - $discount + $unique_code;
                                
                                echo "<b>Rp " . number_format($grand_total,0,',','.') ."</b>"; ?> 
                                    </p> </td>
                                    </tr>
                                </table>  
                                <p>
                                    
                                <br>
                                </p>      
                        <div class="row align-items-center justify-content-center">
                        <form action="{{ url('/course/'.$course->id.'/order') }}" method="POST">
                            @csrf
                            <input type="number" name="course_id" required value="{{ $course->id }}" hidden>
                            <input type="text" name="coupon"  hidden <?php if(!empty($coupon)) { echo "value=".$coupon->code; } ?> >
                            <input  type="text" hidden name="price" required <?php if($coupon->type = null) { $discount = 0; }
                            else if($coupon->discount == "NOTFOUND") { $discount = 0; }
                            else {
                                if($coupon->type == "%")
                                {
                                    $discount = $course->price * ($coupon->discount/100);
                                }
                                else if($coupon->type == "-")
                                {  
                                    $discount = $coupon->discount;
                                }
                                    $grand_total = $course->price - $discount + $unique_code;
                                } 
                                echo "value='$grand_total'"; ?> >
                            <button class="btn clever-btn" type="submit">COMPLETE ORDER</button>

                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
    </div>

@push('bottom')
<script type="text/javascript" language="javascript">
    function redirect()
    {
        var coupon = document.getElementById("coupon").value;
        console.log(coupon)
        location.href = "?coupon=" +coupon;
    }
</script>    
@endpush

@endsection