@extends('layouts.public')
@section('content')
@push('top')
  <link href="https://vjs.zencdn.net/7.6.5/video-js.css" rel="stylesheet">

  <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
  <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>

@endpush
  <!-- ##### Blog Area Start ##### -->
    <section class="blog-area blog-page section-padding-100">
        <div class="container-fluid">
 

            <div class="row">
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-8">
                    <div class="course-content">
                <h1>{{ $course->title }} </h1> <br>
                <p>
                    {!! $course->short_desc !!}
                    <br>
                    Pengajar : {{ $course->mentor_name }}
                </p>

            <div class="row">
                <div class="col">
                View : {{ $count_visitor }} <br>
                {{-- Created : {{ $course->created_at }} --}}
                Created : <?php $timestamp = strtotime($course->created_at); echo date('d/m/Y', $timestamp); ?>
                </div>
                <div class="col">
                Enrol : {{ $course->enroll }} <br>
                Last Updated  : <?php $timestamp = strtotime($course->updated_at); echo date('d/m/Y', $timestamp); ?>

                </div>
                </div>
                </div>
                <br>     

       
                      
                        <!-- Blog Content -->
                        <div class="clever-tabs-content">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="tab--1" data-toggle="tab" href="#tab1" role="tab"
                                        aria-controls="tab1" aria-selected="false">Description</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab--2" data-toggle="tab" href="#tab2" role="tab"
                                        aria-controls="tab2" aria-selected="true">Curriculum</a>
                                </li>
                            
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <!-- Tab Text -->
                                <div class="tab-pane fade show active" id="tab1" role="tabpanel"
                                    aria-labelledby="tab--1">
                                    <div class="clever-description">

                                        <!-- About Course -->
                                        <div class="about-course mb-30">
                                            <h4>About this course</h4>
                                            <p>
                                                {!!$course->desc!!}
</p>
                                        </div>

                                    

                                        <!-- FAQ -->
                                        <div class="clever-faqs">
                                            <h4>FAQs</h4>

                                            <div class="accordions" id="accordion" role="tablist"
                                                aria-multiselectable="true">
                    @foreach ($faq as $faq_item)
    

                                                <!-- Single Accordian Area -->
                                                <div class="panel single-accordion">
                                                    <h6>
                                                        <a role="button" class="collapsed" aria-expanded="true"
                                                            aria-controls="collapse{{ $faq_item->id }}" data-parent="#accordion"
                                                            data-toggle="collapse" href="#collapse{{ $faq_item->id }}">{{ $faq_item->title }}
                                                            <span class="accor-open"><i class="fa fa-plus"
                                                                    aria-hidden="true"></i></span>
                                                            <span class="accor-close"><i class="fa fa-minus"
                                                                    aria-hidden="true"></i></span>
                                                        </a>
                                                    </h6>
                                                    <div id="collapse{{ $faq_item->id }}" class="accordion-content collapse">
                                                        <p>{{$faq_item->content}}</p>
                                                    </div>
                                                </div>
                    @endforeach


                                    </div>
                                    </div>
                                    </div>
                                </div>

                                <!-- Tab Text -->
                                <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab--2">
                                    <div class="clever-curriculum">



                                        <!-- Curriculum Level -->
                                        <div class="curriculum-level mb-30">
                                        {{-- @if(!empty($content))
                                            <div class="float-right">
                                                <button class="btn btn-info btn-sm"><i class="fa fa-play" aria-hidden="true"> </i> Free Content</button>
                                            </div>
                                        @endif --}}
                                        @foreach ($content as $item)
                                    

                                            <h4 class="d-flex justify-content-between"><span>{{ $item->title }}</span>
                                                {{-- <span>0/4</span> --}}
                                            </h4>
                                            {{-- <h5>Beginners Level</h5> --}}
                                            {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim
                                                nulla, mollis eu metus in, sagittis fringilla.</p> --}}
      
                                            <ul class="curriculum-list">
                                                <li>
                                                    <ul>
                                          @foreach ($item->section as $itm)

                                                        <li>

                                                            @if($item->access != 'PUBLIC' || $itm->access != 'PUBLIC')
                                                            {{ $itm->title }}
                                                                <span>  <i class="fa fa-lock" aria-hidden="true"> &nbsp; </i><i class="fa fa-clock" aria-hidden="true"> &nbsp; </i>  
                                                            @else
                                                            <a href="{{ url('/course/'.$course->id.'/watch?content='.$itm->id) }}"> {{ $itm->title }} </a>
                                                            
        <span>                                                       <i class="fa fa-clock" aria-hidden="true"> &nbsp; </i>                                                           

                                                            @endif

                                                            
   <?php
require_once('getID3/getid3/getid3.php');
$getID3 = new getID3;
$file = $itm->video;
if(file_exists($file) == 1)
{
    // echo "ada";
    $file = $getID3->analyze("$itm->video");

    if (array_key_exists('playtime_string', $file)) {
		echo("Durasi: ".$file['playtime_string'] ."");
	};
}
else {
    echo "Content belum diupload";
}


                                                            ?>

                                                                </span>
                                                        </li>
                                            @endforeach                                            

                                                    </ul>
                                                </li>
                      
                                            </ul>
                                            <br>
                                            @endforeach
                                            
                                        </div>

                                     
                                    </div>
                                    </div>
                                    </div>
                                </div>



                        
                </div>

                       
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-4">
                        <div class="row align-items-center justify-content-center">
                        <div class="embed-responsive embed-responsive-16by9">

                        <video controls>
                            <source src="{{ asset($course->teaser) }}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                           
                        
                        </div>
                        </div>
                        <br>

                        <div class="row align-items-center justify-content-center">
           
                        <h1><?=  "Rp " . number_format($course->price,0,',','.');  ?></h1>
                        </div>

                        <div class="row align-items-center justify-content-center">

                        <?php
                                if($session != null)
                                {
                                    if($order != null)
                                    {
                                        // dd($order);
                                        if($order->status == "Success")
                                        {
                                        echo '<a href="'?>{{ url('course/'.$course->id.'/watch') }} <?php echo'" class="btn clever-btn mb-30">Play</a>';                                
                                        }
                                        else {
                                            $t=time();
                                            $now = date("Y-m-d h:i:s",$t);                                        
                                            if($now > $order->void_at)
                                            {
                                        echo '<a href="'?>{{ url('course/'.$course->id.'/checkout') }} <?php echo'" class="btn clever-btn mb-30">Buy course</a>';
                                            }
                                            else
                                            {
                                                echo '<a href="'?>{{ url('order/'.$order->id) }} <?php echo'" class="btn clever-btn mb-30">Complete Order</a>';
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if($course->mentor_id == $session->id )
                                        {
                                            echo '<a href="'?>{{ url('user/course/edit/'.$course->id) }} <?php echo'" class="btn clever-btn mb-30">Edit Course</a>';
                                        }
                                        
                                        else {
                                            echo '<a href="'?>{{ url('course/'.$course->id.'/checkout') }} <?php echo'" class="btn clever-btn mb-30">Buy course</a>';
                                        }
                                    }
                                }
                                    else {
                                            echo '<a href="'?>{{ url('course/'.$course->id.'/checkout') }} <?php echo'" class="btn clever-btn mb-30">Buy course</a>';
                                        }                                
                                
                                

                               
                            ?>                        
                        </div>     
                   


           {{-- <div class="sidebar-widget">
                            <h4>Course Features</h4>
                            <ul class="features-list">
                                <li>
                                    <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Duration</h6>
                                    <h6>2 Weeks</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-bell" aria-hidden="true"></i> Lectures</h6>
                                    <h6>10</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-file" aria-hidden="true"></i> Quizzes</h6>
                                    <h6>3</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-thumbs-up" aria-hidden="true"></i> Pass Percentage</h6>
                                    <h6>60</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-thumbs-down" aria-hidden="true"></i> Max Retakes</h6>
                                    <h6>5</h6>
                                </li>
                            </ul>
                        </div> --}}

                        <div class="sidebar-widget wow fadeInUp" data-wow-delay="500ms">
                        <!-- Blog Content -->
                            <h4>You may like</h4>
                            <br>
                            @foreach ($simmiliar_course as $item)
                                
                            <!-- Single Courses -->
                            <div class="single--courses d-flex align-items-center">
                                <div class="thumb">
                                    <img src="{{ asset($item->poster) }}" alt="">
                                </div>
                                <div class="content">
                                    <a href="{{ url('/course/'.$item->id) }}">
                                    {{ $item->title }}
                                    </a>
                                    <p><?=  "Rp " . number_format($item->price,0,',','.');  ?></h6>
                                </div>
                            </div>
                            @endforeach

                           </div>
            
                        </div>
                </div>

                
            </div>

   
        </div>
    </section>
    <!-- ##### Blog Area End ##### -->


<div id="uploadModal" class="modal fade" role="dialog">
<div class="modal-dialog modal-lg">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          {{-- <h4 class="modal-title">Add New Video</h4> --}}
        </div>
        <div class="modal-body">
         <span id="form_section"></span>
 
    <br>
              {{-- <div class="form-group row">
            <label class="control-label col-md-2" >Title </label>
            <div class="col-md-10">
             <input type="text" readonly name="upload_section_title" id="upload_section_title" class="form-control" />
            </div>
           </div> --}}
             {{-- <input type="text" name="curriculum_id"   id="curriculum_id"  /> --}}

            <div>

<video id='my-video' class='video-js' controls preload='auto' width='640' height='264'>
    <source id="source" src="" type='video/mp4'>
    <p class='vjs-no-js'>
      To view this video please enable JavaScript, and consider upgrading to a web browser that
      <a href='https://videojs.com/html5-video-support/' target='_blank'>supports HTML5 video</a>
    </p>
  </video>

            </div>
           

                </div>


        </div>
     </div>
    </div>
</div>


@push('bottom')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>

<script>

          $('#uploadModal').on('hidden.bs.modal', function () {
      $(this).find("input").val('').end();
            });
            


  $(document).on('click', '.sectionupload', function(){
  var id = $(this).attr('id');
//   $('#form_section').html('');
  
  var url =  '{{ route('section-ajax.edit', ":id") }}';
url = url.replace(':id', id);
  $.ajax({
        url: url,

   dataType:"json",
   success:function(html){
    $('#id').val(html.data.id);
    $('#upload_section_title').val(html.data.title);
$("#source").attr("src", "{{ asset('/') }}" + html.data.video);
    console.log(html.data)
//    $('.modal-title').text("Upload Video");
    $('#section_action_button').val("Update");
    $('#section_action').val("Edit");
    // $('#uploadModal').modal('show');
		$('#uploadModal').modal({
			backdrop: 'static'
		});
            $('#uploadModal').modal('show');

    
   }
  })
 });

  </script>
 
  <script src='https://vjs.zencdn.net/7.6.5/video.js'></script>

@endpush


@endsection