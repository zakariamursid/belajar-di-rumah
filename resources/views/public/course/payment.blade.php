{{-- @dd($bank_account) --}}

@extends('layouts.public')
@section('content')
    
{{-- @dd($order); --}}
<div class="regular-page-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="page-content">
                        <h4> Course to Buy </h4>
                        <table border="0">
	<tr>
		<td>
		</td>
	</tr>
	<tr>
		<td>
			<script>
				CountDownTimer('{{$order->created_at}}', 'countdown');
				function CountDownTimer(dt, id)
				{
					var end = new Date('{{$order->void_at}}');
					var _second = 1000;
					var _minute = _second * 60;
					var _hour = _minute * 60;
					var _day = _hour * 24;
					var timer;
					function showRemaining() {
						var now = new Date();
						var distance = end - now;
						if (distance < 0) {

							clearInterval(timer);
							document.getElementById(id).innerHTML = '<b>WAKTU PEMBAYARAN SUDAH BERAKHIR</b> ';
							return;
						}
						var days = Math.floor(distance / _day);
						var hours = Math.floor((distance % _day) / _hour);
						var minutes = Math.floor((distance % _hour) / _minute);
						var seconds = Math.floor((distance % _minute) / _second);

						document.getElementById(id).innerHTML = days + 'days ';
						document.getElementById(id).innerHTML += hours + 'hrs ';
						document.getElementById(id).innerHTML += minutes + 'mins ';
						document.getElementById(id).innerHTML += seconds + 'secs';
					}
					timer = setInterval(showRemaining, 1000);
				}
			</script>
			<div id="countdown"> 
			</td>
		</tr>
    </table>
                            <hr>



                            <p>
                            Title : {{ $order_detail->title }} <br>
                            Mentor : {{ $order_detail->mentor_name }}
                            
                            @if(date("Y-m-d H:i:s") <  $order->void_at)

                            <br>
                            <p>
                            <b> Please Following this instruction to complete payment </b>
                            </p>
                                <table>
                                    <tr>
                                        <td><p> Total  </p></td>
                                        <td><p> &nbsp; : &nbsp; <?=  "Rp " . number_format($order->grand_total,0,',','.'); ?>  </p> </td>
                                    </tr>
                                    <tr>
                                        <td><p> Atas Nama  </p></td>
                                        <td><p> &nbsp; : &nbsp; {{ $bank_account->account_name }}</p></td>
                                    </tr>
                                    <tr>
                                        <td><p> Nomor Rekening</p></td>
                                        <td><p> &nbsp; : &nbsp; {{ $bank_account->account_number }}</p></td>
                                    <tr>
                                        <td><p> Bank  </p></td>
                                        <td><p> &nbsp; : &nbsp; {{ $bank_account->bank_name }}</p></td>
                                    </tr>
                                    <tr>
                                        <td><p> Cabang </p></td>
                                        <td><p> &nbsp; : &nbsp; {{ $bank_account->branch }}</p></td>
                                    </tr>
                                </table>
                                <br>
                            <p>                                
                            <b>After Your payment process, please fill this Confirmation Form below </b>
                            </p>
         
                            <form action="{{ url('/order/'.$order->id.'/validate') }}" method="post" enctype="multipart/form-data">
                            @csrf
                                    <input required type="text" name="order_id" hidden required value="{{ $order->id }}">
                                <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <input required type="text" name="bank" class="form-control" id="bank" placeholder="Nama Bank">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <input required type="text" name="branch" class="form-control" id="branch" placeholder="Cabang">
                                    </div>
                                </div>

                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <input required type="number" name="account_number" class="form-control" id="account_number" placeholder="Dari Nomer Rekening">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <input required type="text" name="account_name" class="form-control" id="account_name" placeholder="Atas Nama">
                                    </div>
                                </div>                                
                                {{-- <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <input required type="number" name="nominal" class="form-control" id="nominal" placeholder="Nominal">
                                    </div>
                                </div>                                 --}}
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <input required type="file" name="proof" class="form-control" id="proof" placeholder="Bukti Transfer">
                                    </div>
                                </div>                                
                            </div>

                                <div class="pull-right">
                                    <button class="btn clever-btn">Konfirmasi Pembayaran</button>
                                </div>
                        </form>                                
                                <br>

                                @endif

                            </p>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection