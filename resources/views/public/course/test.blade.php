@extends('layouts.public')
@section('content')
    
  <!-- ##### Blog Area Start ##### -->
    <section class="blog-area blog-page section-padding-100">
        <div class="container-fluid">
 

            <div class="row">
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-8">
                    <div class="course-content">
                <h1>{{ $course->title }} </h1> <br>
                <p>
                    {{ $course->short_desc }}
                    <br>
                    Pengajar : {{ $course->mentor_name }}
                </p>

            <div class="row">
                <div class="col">
                View : x <br>
                Created : {{ $course->created_at }}
                </div>
                <div class="col">
                Enrol : x <br>
                Last Updated : {{ $course->updated_at }}
                </div>
                </div>
                </div>
                <br>     

       
                      
                        <!-- Blog Content -->
                        <div class="clever-tabs-content">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="tab--1" data-toggle="tab" href="#tab1" role="tab"
                                        aria-controls="tab1" aria-selected="false">Description</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab--2" data-toggle="tab" href="#tab2" role="tab"
                                        aria-controls="tab2" aria-selected="true">Curriculum</a>
                                </li>
                            
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                <!-- Tab Text -->
                                <div class="tab-pane fade show active" id="tab1" role="tabpanel"
                                    aria-labelledby="tab--1">
                                    <div class="clever-description">

                                        <!-- About Course -->
                                        <div class="about-course mb-30">
                                            <h4>About this course</h4>
                                            <p>
                                                {!!$course->desc!!}
</p>
                                        </div>

                                    

                                        <!-- FAQ -->
                                        <div class="clever-faqs">
                                            <h4>FAQs</h4>

                                            <div class="accordions" id="accordion" role="tablist"
                                                aria-multiselectable="true">

                                                <!-- Single Accordian Area -->
                                                <div class="panel single-accordion">
                                                    <h6><a role="button" class="" aria-expanded="true"
                                                            aria-controls="collapseOne" data-toggle="collapse"
                                                            data-parent="#accordion" href="#collapseOne">Can I just
                                                            enroll in a single course? I'm not interested in the entire
                                                            Specialization?
                                                            <span class="accor-open"><i class="fa fa-plus"
                                                                    aria-hidden="true"></i></span>
                                                            <span class="accor-close"><i class="fa fa-minus"
                                                                    aria-hidden="true"></i></span>
                                                        </a></h6>
                                                    <div id="collapseOne" class="accordion-content collapse show">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                            Fusce enim nulla, mollis eu metus in, sagittis fringilla
                                                            tortor.</p>
                                                    </div>
                                                </div>

                                                <!-- Single Accordian Area -->
                                                <div class="panel single-accordion">
                                                    <h6>
                                                        <a role="button" class="collapsed" aria-expanded="true"
                                                            aria-controls="collapseTwo" data-parent="#accordion"
                                                            data-toggle="collapse" href="#collapseTwo">What is the
                                                            refund policy?
                                                            <span class="accor-open"><i class="fa fa-plus"
                                                                    aria-hidden="true"></i></span>
                                                            <span class="accor-close"><i class="fa fa-minus"
                                                                    aria-hidden="true"></i></span>
                                                        </a>
                                                    </h6>
                                                    <div id="collapseTwo" class="accordion-content collapse">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed
                                                            vel lectus eu felis semper finibus ac eget ipsum. Lorem
                                                            ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                                                            vulputate id justo quis facilisis.</p>
                                                    </div>
                                                </div>

                                                <!-- Single Accordian Area -->
                                                <div class="panel single-accordion">
                                                    <h6>
                                                        <a role="button" aria-expanded="true"
                                                            aria-controls="collapseThree" class="collapsed"
                                                            data-parent="#accordion" data-toggle="collapse"
                                                            href="#collapseThree">What background knowledge is
                                                            necessary?
                                                            <span class="accor-open"><i class="fa fa-plus"
                                                                    aria-hidden="true"></i></span>
                                                            <span class="accor-close"><i class="fa fa-minus"
                                                                    aria-hidden="true"></i></span>
                                                        </a>
                                                    </h6>
                                                    <div id="collapseThree" class="accordion-content collapse">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed
                                                            vel lectus eu felis semper finibus ac eget ipsum. Lorem
                                                            ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                                                            vulputate id justo quis facilisis.</p>
                                                    </div>
                                                </div>

                                                <!-- Single Accordian Area -->
                                                <div class="panel single-accordion">
                                                    <h6>
                                                        <a role="button" aria-expanded="true"
                                                            aria-controls="collapseFour" class="collapsed"
                                                            data-parent="#accordion" data-toggle="collapse"
                                                            href="#collapseFour">Do i need to take the courses in a
                                                            specific order?
                                                            <span class="accor-open"><i class="fa fa-plus"
                                                                    aria-hidden="true"></i></span>
                                                            <span class="accor-close"><i class="fa fa-minus"
                                                                    aria-hidden="true"></i></span>
                                                        </a>
                                                    </h6>
                                                    <div id="collapseFour" class="accordion-content collapse">
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed
                                                            vel lectus eu felis semper finibus ac eget ipsum. Lorem
                                                            ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                                                            vulputate id justo quis facilisis.</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!-- Tab Text -->
                                <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab--2">
                                    <div class="clever-curriculum">



                                        <!-- Curriculum Level -->
                                        <div class="curriculum-level mb-30">
                                        @foreach ($content as $item)
                                    

                                            <h4 class="d-flex justify-content-between"><span>{{ $item->title }}</span>
                                                {{-- <span>0/4</span> --}}
                                            </h4>
                                            {{-- <h5>Beginners Level</h5> --}}
                                            {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim
                                                nulla, mollis eu metus in, sagittis fringilla.</p> --}}
      
                                            <ul class="curriculum-list">
                                                <li>
                                                    <ul>
                                          @foreach ($item->section as $itm)

                                                        <li>{{ $itm->title }}
                                                            <?php
$getID3 = new getID3;
$file = $getID3->analyze($itm->video);
echo("Duration: ".$file['playtime_string'].
        " / Dimensions: ".$file['video']['resolution_x']." wide by ".$file['video']['resolution_y']." tall".
        " / Filesize: ".$file['filesize']." bytes<br />");
                                                            ?>
                                                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> x
                                                                minutes</span>
                                                        </li>
                                            @endforeach                                            

                                                    </ul>
                                                </li>
                      
                                            </ul>
                                            <br>
                                            @endforeach
                                            
                                        </div>

                                     
                                    </div>
                                    </div>
                                    </div>
                                </div>



                        
                </div>

              
                                
                       
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-4">
                        <div class="row align-items-center justify-content-center">
                        <div class="embed-responsive embed-responsive-16by9">

                            <video controls>
                            <source src="{{ asset($course->teaser) }}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                           
                        
                        </div>
                        </div>
                        <br>

                        <div class="row align-items-center justify-content-center">
           
                        <h1><?=  "Rp " . number_format($course->price,0,',','.');  ?></h1>
                        </div>

                        <div class="row align-items-center justify-content-center">

                        <?php
                                if($session != null)
                                {
                                if($course->mentor_id != $session->id )
                                {
                                    echo '<a href="'?>{{ url('course/'.$course->id.'/checkout') }} <?php echo'" class="btn clever-btn mb-30">Buy course</a>';
                                }
                                else if($course->mentor_id == $session->id )
                                {
                                    echo '<a href="'?>{{ url('user/course/edit/'.$course->id) }} <?php echo'" class="btn clever-btn mb-30">Edit Course</a>';
                                }
                                }
                                else {
                                    echo '<a href="'?>{{ url('course/'.$course->id.'/checkout') }} <?php echo'" class="btn clever-btn mb-30">Buy course</a>';
                                }


                            ?>                        
                        </div>     
                   


           {{-- <div class="sidebar-widget">
                            <h4>Course Features</h4>
                            <ul class="features-list">
                                <li>
                                    <h6><i class="fa fa-clock-o" aria-hidden="true"></i> Duration</h6>
                                    <h6>2 Weeks</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-bell" aria-hidden="true"></i> Lectures</h6>
                                    <h6>10</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-file" aria-hidden="true"></i> Quizzes</h6>
                                    <h6>3</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-thumbs-up" aria-hidden="true"></i> Pass Percentage</h6>
                                    <h6>60</h6>
                                </li>
                                <li>
                                    <h6><i class="fa fa-thumbs-down" aria-hidden="true"></i> Max Retakes</h6>
                                    <h6>5</h6>
                                </li>
                            </ul>
                        </div> --}}

                        <div class="sidebar-widget wow fadeInUp" data-wow-delay="500ms">
                        <!-- Blog Content -->
                            <h4>You may like</h4>
                            <br>
                            @foreach ($simmiliar_course as $item)
                                
                            <!-- Single Courses -->
                            <div class="single--courses d-flex align-items-center">
                                <div class="thumb">
                                    <img src="{{ asset($item->poster) }}" alt="">
                                </div>
                                <div class="content">
                                    <a href="{{ url('/course/'.$item->id) }}">
                                    {{ $item->title }}
                                    </a>
                                    <p><?=  "Rp " . number_format($item->price,0,',','.');  ?></h6>
                                </div>
                            </div>
                            @endforeach

                           </div>
            
                        </div>
                </div>

                
            </div>

   
        </div>
    </section>
    <!-- ##### Blog Area End ##### -->


@endsection