@extends('layouts.public')
@section('content')
     <!-- ##### Blog Area Start ##### -->
    <section class="blog-area blog-page section-padding-100">
        <div class="container-fluid">

           
                <h2>All Course</h2>
                <br>
                
                <div class="row">

            @foreach ($course as $item)
                
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-3">
                    <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="250ms">
                       <a href="{{ url('course/'.$item->id) }}"> <img src="{{ asset($item->poster) }}" alt=""></a>
                       <br>
                        <div class="blog-content">

                    <div style="min-height: 80px">
                      <b> {{ $item->title }} </b>
                    </div>
                    {{ $item->mentor_name }} <br>
                    Dipelajari oleh : <b>{{ $item->enroll }}</b> Orang <br>
                        <?php echo  "Rp " . number_format($item->price,0,',','.');  ?>
                    </div>
                    </div>
                </div>

            @endforeach
            
            

               
        </div>
        <div class="float-right">
                	{{ $course->links() }}

            </div>
@endsection