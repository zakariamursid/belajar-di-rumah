@extends('layouts.public')
@section('content')

    <!-- ##### Blog Area Start ##### -->
    <section class="blog-area blog-page section-padding-100">
        <div class="container-fluid">

            <div class="row">
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-12">
                    <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="250ms">
                        <!-- Blog Content -->
                        <div class="blog-content">
                                <h4>What to learn next</h4>
                            <div class="meta d-flex align-items-center">
                                Topics recommended for you
                            </div>
                
                             @foreach ($topic as $c)
                                <button class="btn btn-sm btn-info">{{ $c->name }}</button> &nbsp;
                             @endforeach
                            
                            {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce enim nulla, mollis eu metus in, sagittis</p> --}}
                        </div>
                    </div>
                <div style="margin-bottom:200px;">
                    <div class="float-left">
                        <h2>Latest Course</h2>
                    </div>
                    <div class="float-right">
                        <a href="{{ url('/all-courses') }}">
                            <button class="btn btn-sm btn-info">See More</button>
                        </a>
                    </div>
                </div>

                <div class="row">

            @foreach ($course as $item)
                
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-3">
                 <a href="{{ url('course/'.$item->id) }}"> 

                    <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="250ms">
                      <img src="{{ asset($item->poster) }}" alt="">
                       <br>
                        <div class="blog-content">

                    <div style="min-height: 80px">
                      <b> {{ $item->title }} </b>
                    </div>
                    {{ $item->mentor_name }} <br>
                    Dipelajari oleh : <b>{{ $item->enroll }}</b> Orang <br>
                        <?php echo  "Rp " . number_format($item->price,0,',','.');  ?>
                    </div>
                    </div>
                </a>

                </div>

            @endforeach
            
                

               
        </div>

                <h2>Top Course</h2>
                <br>
                
                <div class="row">

            @foreach ($top_course as $item)
                
                <!-- Single Blog Area -->
                <div class="col-12 col-lg-3">
                       <a href="{{ url('course/'.$item->id) }}"> 
                    <div class="single-blog-area mb-100 wow fadeInUp" data-wow-delay="250ms">
                    <img src="{{ asset($item->poster) }}" alt="">
                       <br>
                        <div class="blog-content">

                    <div style="min-height: 80px">
                      <b> {{ $item->title }} </b>
                    </div>
                    {{ $item->mentor_name }} <br>
                    Dipelajari oleh : <b>{{ $item->enroll }}</b> Orang <br
                     <?=  "<b>Rp</b>" . number_format($item->price,0,',','.');  ?>
                    </div>
                    </div>
                    </a>
                </div>

            @endforeach
            
                

               
        </div>
    </section>
   
    <!-- ##### Popular Courses End ##### -->




   
@endsection