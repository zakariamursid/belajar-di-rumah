@extends('layouts.public')
@section('content')

    <!-- ##### Regular Page Area Start ##### -->
    <div class="regular-page-area section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="page-content">
                        <h4>Community Guidelines</h4>
                        <br>
                        <p>
                            {!! $tnc->content !!}
                       
                            <form action="{{ url('/post-bementor') }}" method="POST">
                                <br>
                            <input type="checkbox" id="checkme"/>&nbsp; Saya setuju dengan syarat dan ketentuan diatas

                            <div class="float-right">
                                @csrf
                            <input type="text" name="user_id" value="{{ $session->id }}" hidden>
                            <input type="submit" name="beMentor" class="inputButton btn btn-primary btn-sm" disabled="disabled" id="beMentor" value="Join" />
                            </div>

                            </form>   

                        </p>
                        <br>
                         
                        <div>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Regular Page Area End ##### -->

    @push('bottom')
<script>
     var checker = document.getElementById('checkme');
 var sendbtn = document.getElementById('beMentor');
 // when unchecked or checked, run the function
 checker.onchange = function(){
if(this.checked){
    sendbtn.disabled = false;
} else {
    sendbtn.disabled = true;
}

}
</script>        
    @endpush
    
@endsection