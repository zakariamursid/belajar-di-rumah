@extends('layouts.public')
@section('content')
@push('top')
 <style type="text/css">
.bgconnected {
    background-image: url('{{ asset('clever_asset/img/landing/bg_connected.png')}}');
}


.bgImgCenter {
    background-image: url('clever_asset/img/landing/bg_connected.png');

            background-repeat: no-repeat;
            background-position: center;
            position: relative;
    }
</style>   
@endpush
<br>
    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area bg-img " style="background-image: url({{ asset('clever_asset/img/bg-img/bg_tittle.png')}});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <!-- Hero Content -->
                    <div class="hero-content">
<div style="width:50%; float:left">
                        <h4>We have 5 Years Experience <br>
                            about Mobile Apps Development <br>
                            with Impact
                        </h4>
                        <a href="{{ url('/user/login') }}" class="btn register-btn">Register</a>
                        <a href="#learn" class="btn learn-btn">Learn More</a></div>


<div style="width: 50%; float:right">
    <img src="{{ asset('clever_asset/img/bg-img/ic_ilstration_tittle.png')}}" alt="">
</div>
                        

                    </div>
                </div>

            </div>
            
        </div>
    <div id="learn">

    </section>

    <div class="row align-items-center justify-content-center" style="margin-top: 100px; margin-bottom:100px">
         <h3 class="text-center">
             This is the Main Reason Why You <br> Crocodic Course
         </h3>    
    </div>
    </div>


<div class="container" style="margin-top: 100px; margin-bottom:100px">
  <div class="row">
    <div class="col-sm">
        <div class="row align-items-center justify-content-center">
        <img src="{{ asset('clever_asset/img/landing/ic_pengalaman.png') }}" alt="">
        </div>
    <h6 class="text-center">Crocodic berpengalaman dalam bidang pengembangan aplikasi selama 5 tahun mengerjakan +200 aplikasi.</h6>
    </div>
    <div class="col-sm">
        <div class="row align-items-center justify-content-center">
        <img src="{{ asset('clever_asset/img/landing/ic_dipercaya.png') }}" alt="">
        </div>
    <h6 class="text-center">Crocodic dipercaya dalam menyediakan SDM dibidang pemograman aplikasi oleh +100 perusahaan multinasional.</h6>
    </div>
    <div class="col-sm">
        <div class="row align-items-center justify-content-center">
        <img src="{{ asset('clever_asset/img/landing/ic_standarisasi.png') }}" alt="">
        </div>
    <h6 class="text-center">Crocodic memiliki standarisasi programer yang telah terbukti mampu menghasilkan kinerja SDM programer unggul.</h6>
    </div>
  </div>
</div>

    <div class="row align-items-center justify-content-center" style="margin-top: 100px; margin-bottom:100px">
         <h3 class="text-center">
             We are Ready to Become You Learning <br> Source Partner
         </h3>    
    </div>

<div class="container" style="margin-top: 100px; margin-bottom:100px">
  <div class="row">

    @foreach ($course as $item)
    <div class="col-sm" style="margin:10px">
        <a href="{{ url('/course/'.$item->id) }}">
            <div class="row align-items-center justify-content-center">
                <img src="{{ asset($item->poster) }}" alt="">
            </div>
        </a>
    </div>        
    @endforeach



  </div>
</div>


    <div class="row align-items-center justify-content-center" style="margin-top: 100px; margin-bottom:100px">
         <h3 class="text-center">
             Are You Instructor or Mentor <br> You Have a Content?
         </h3>    
    </div>
 
 
<div class="container" style="margin-top: 100px; margin-bottom:100px">
  <div class="row">

    
    <div class="col-sm" style="margin:10px">
        <div class="row align-items-center justify-content-center">
        <img src="{{ asset('clever_asset/img/landing/bg_share_knowledge.png') }}" alt="">
        </div>
    </div>
    <div class="col-sm" style="margin:10px">
        <div class="row align-items-center justify-content-center">
        <img src="{{ asset('clever_asset/img/landing/bg_connected.png') }}" alt="">
        </div>
    </div>
    <div class="col-sm" style="margin:10px">
        <div class="row align-items-center justify-content-center">
        <img src="{{ asset('clever_asset/img/landing/bg_earn_revenue.png') }}" alt="">
        </div>
    </div>

  </div>
</div>



    <div class="row align-items-center justify-content-center" style="margin-top: 100px; margin-bottom:100px">
                        <a href="{{ url('/user/login') }}" class="btn register-btn">Register</a> &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="#learn" class="btn test-btn">Learn More</a>
    </div>

<!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <!-- Top Footer Area -->
        <div class="top-footer-area">
            <!-- Footer Logo -->
                        <div class="pull-right" style="padding-right: 200px;">
                                <a href="">About Us</a> <br>
                                <a href="">Terms & Condition</a> <br>
                                <a href="">Service</a> <br>
                        </div>              
                        <div class="pull-left" style="padding-left: 200px;">
                            <a href="index.html"><img src="{{ asset('clever_asset/img/landing/ic_logo.png') }}" alt=""></a>
 
                        </div>  

                                     <br>
                                     <br>
                                     <br>
        
        </div>
    </div>
    <footer class="footer-area">

        <div class="top-footer-area">
            <!-- Footer Logo -->

            <div class="container">
                <div class="row">
                    <div class="col-12">
                        
                        <!-- Copywrite -->
                        <p><a href="#"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
{{-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> --}}
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved Crocodic 
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- ##### Footer Area End ##### -->

@endsection