{{-- @dd($course); --}}
@extends('layouts.topbar')
@section('head')
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('js/custom.css') }}" rel="stylesheet">

@endsection
@section('content')
 <main role="main">

     <main role="main" class="container">
      <div class="row">
        <div class="col-md-8 blog-main" >
<div class="media">
  <div class="media-body">
               <div class="card mb-4 box-shadow">
                                 <div class="card-body">

    <h5 class="mt-0"><b>{{ $course->title }}</b></h5>
    {{ $course->short_desc }}
    <br>
    <br>
   <b> Pengajar : {{ $course->mentor_name }} </b>
   <br>
   <br>

  <div class="row">
    <div class="col">
      View : 542 <br>
      Created : {{ $course->created_at }}
    </div>
    <div class="col">
      Enrol : 424 <br>
      Last Updated : {{ $course->updated_at }}
    </div>
    <br>
    <br>
  </div>
  <br>
  <div>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-description-tab" data-toggle="pill" href="#pills-description" role="tab" aria-controls="pills-description" aria-selected="true">Description</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" id="pills-content-tab" data-toggle="pill" href="#pills-content" role="tab" aria-controls="pills-content" aria-selected="false">Course Content</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-description" role="tabpanel" aria-labelledby="pills-description-tab">
    {{ $course->desc }}
</div>

  <div class="tab-pane fade" id="pills-content" role="tabpanel" aria-labelledby="pills-content-tab">
    {{-- @dd($content) --}}
    @foreach ($content as $item)
     <b>   {{ $item->title }} </b> :
     @foreach ($item->section as $s)
      {{ $s->title }} 
     @endforeach
     <br>
    @endforeach
</div>
</div>
  </div></div>
  </div>
  </div>



</div>

        </div><!-- /.blog-main -->

        <aside class="col-md-4 blog-sidebar">
          <div class="p-3 mb-3 bg-light rounded">
                    <video width="320" height="240" controls>
                            {{-- <source src="{{ route('getVideo', $video->id)  }}" type="video/mp4"> --}}
                            <source src="{{ asset($course->teaser) }}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>            
            {{-- <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
</div> --}}
            {{-- <h4 class="font-bold">Rp. {{ $course->price }}</h4> --}}
            <p class="mb-0">
              <div>
                {{-- <button class="btn-danger" style="width: 100%">Buy Now</button> --}}
              </div>
            <h4 class="font-bold"> <?=  "Rp " . number_format($course->price,2,',','.');  ?></h4>
            <p class="mb-0">
              <div>
                <a href="{{ url('/course/'.$course->id.'/checkout') }}">
                <button class="btn-danger" style="width: 100%">
                  Buy Now</button>
                </a>
              </div>

            </p>
          </div>

       
        

      </div><!-- /.row -->

      
      <div class="album py-5 bg-light">
        <div class="container">
            <b> Other also </b>
            <br>
            <br>

          <div class="row">
            @foreach ($course_also as $item)
                
            <div class="col-md-3">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text"><b>{{ $item->title }}</b><br>{{ $item->user_name}}<br>Dipelajari oleh 165 orang<br>{{ $item->price }}</p>

                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                      {{-- <button type="button" class="btn btn-sm btn-outline-secondary">Fav</button> --}}
                    </div>
                    <small class="text-muted">Dipelajari oleh <b>9</b> Orang</small>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            
       
            
            </div>
          </div>
        </div>
      </div>

    </main>
    @endsection