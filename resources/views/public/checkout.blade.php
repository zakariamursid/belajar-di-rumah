{{-- @dd($course); --}}
@extends('layouts.topbar')
@section('content')
 <main role="main">

     <main role="main" class="container">
      <div class="row">
        <div class="col-md-12 blog-main" >
<div class="media">
  <div class="media-body">
    <b> Course to Buy </b>
    <br>
    <br>
    <h5 class="mt-0">{{ $course->title }}</h5>
    Pengajar : {{ $course->mentor_name }}
    <hr>

    <div>
        <form action="{{ url('/course/'.$course->id.'/order') }}" method="POST">
          @csrf
        <b>Summary</b>
                <br>
        <table>
            <tr>
                <td>Price </td>
                <td>: <?=  "Rp " . number_format($course->price,2,',','.');  ?></td>
                <input type="number" name="course_id" value="{{ $course->id }}" hidden>
            </tr>
            <tr>
                <td>Coupon</td>
                <td>: <input type="text" name="coupon" id=""> </td>
        </table>  
        <br>
            <button class="btn-danger" style="width: 100%">Place Order</button>
            </div>
            </form>
    </div>

</div> 



</div>

        </div><!-- /.blog-main -->

      </div><!-- /.row -->
    </div>


        </div>
      </div>

    </main>
    @endsection