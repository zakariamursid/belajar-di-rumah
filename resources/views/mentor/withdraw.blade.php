@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
  <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Show Data</h1>
        </div>
        <div class="box-body">
Please fill this form to withdraw Your Earned (min Rp 500k)
<br>
<br>
        <div>
        <table>
            <tr>
                <td><b>Your Balance</b></td>
                <td> : <?= "Rp " . number_format($balance,0,',','.'); ?>  </td>
            </tr>
           
        </table>  
              
            </div>
        <div>

<br>
        <div>
         <b>Your Account</b>
        <table>
            <tr>
                <td>Nama </td>
                <td>: {{ $session->name }} </td>
            </tr>
 
            <tr>
                <td>E-Mail </td>
                <td>: {{ $session->email }} </td>
            </tr>
 
        </table>  
              
            </div>
<br>

        <div <?php if($balance <= 500000) { echo 'style="display: none"'; } else if(empty($pending)) { echo '';}  else if(!empty($pending)) { echo 'style="display: none"';} ?>>

         <b>Your Bank Account</b>
<br>
<br>
           <form action="{{ url('user/withdrawrequest') }}" method="POST">

<div class="form-group row">
    <label for="inputBranch" class="col-sm-2 col-form-label">Bank</label>
    <div class="col-sm-10">
                <select name="bank" id="bank" class="form-control">   
                @foreach ($bank as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
                </select>    </div>
  </div>

@csrf
<div class="form-group row">
    <label for="inputBranch" class="col-sm-2 col-form-label">Cabang</label>
    <div class="col-sm-10">
      <input required type="text" name="branch" class="form-control" id="inputBranch" placeholder="Cabang">
    </div>
  </div>
<div class="form-group row">
    <label for="inputAccountNumber" class="col-sm-2 col-form-label">Nomer Rekening</label>
    <div class="col-sm-10">
      <input required type="number" name="account_number" class="form-control" id="inputAccountNumber" placeholder="Nomer Rekening">
    </div>
  </div>
<div class="form-group row">
    <label for="inputAccountName" class="col-sm-2 col-form-label">Nama Pemilik Rekening</label>
    <div class="col-sm-10">
      <input required type="text" name="account_name" class="form-control" id="inputAccountName" placeholder="Nama Pemilik Rekening">
    </div>
  </div>
<div class="form-group row">
    <label for="inputAmount" class="col-sm-2 col-form-label">Jumlah</label>
    <div class="col-sm-10">
      <input required min="500000" type="number" name="amount" class="form-control" id="inputAmount" placeholder="Jumlah Nominal Yang Ingin ditarik">
    </div>
  </div>
        <br>
        <div class="pull-right">
            <button class="btn btn-danger">Withdraw</button>
        </div>
              
            </div>
        </form>
        </div>
        </div>
        </div>
        <br>
  <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Show Data</h1>
        </div>
        <div class="box-body">
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>Amount</th>
                        <th>Account Name</th>
                        <th>Account Number</th>
                        <th>Bank</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($withdraw as $item)
                        <tr>
                        <td> <?=  "Rp " . number_format($item->amount,0,',','.');  ?></td>
                            <td>{{ $item->account_name }}</td>
                            <td>{{ $item->account_number }}</td>
                            <td>{{ $item->bank_name }}</td>
                            <td>{{ $item->status }}</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection