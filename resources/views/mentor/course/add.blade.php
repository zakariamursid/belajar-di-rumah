@extends(getThemePath("layout.layout"))
@section("content")

    <p>
        <a href=""><i class="fa fa-arrow-left"></i> Back To List</a>
    </p>

    <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Add Course</h1>
        </div>
        <form enctype="multipart/form-data" action="{{ url('user/course/add') }}" method="POST">

            {!! csrf_field() !!}
        <div class="box-body">
            <div class="form-group">
                <label for="">Course Title</label>
            <input required type="text" name="mentor_id" hidden value="1">
                <input required required type="text" placeholder="E.g : VUE JS From Zero To Hero" name="title" class="form-control">
            </div>
           <div class="form-group">
                <label for="">Price (IDR)</label>
                <input required type="number" name="price" class="form-control" placeholder="Rp. ">
            </div>            

           {{-- <b> Basic Info </b> --}}
            <div class="form-group">
                <label for="">Topic</label>
                <select required name="topic[]" id="topic" class="js-example-basic-multiple" multiple="multiple" style="width:100%">
                {{-- <select name="topic_id" class="form-control"> --}}
                    <option value="">** Select a Category</option>

                            @foreach($topics as $key => $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                            {{-- <option value="{{ $value->id }}" @if($data->md_divisi_id==$value->id) selected @elseif(oldW('divisi')==$value->id) selected @endif>{{ $value->kode.' - '.$value->nama }}</option> --}}
                            @endforeach                    
                </select>
            </div>
            <div class="form-group">
                <label for="">Skill Level</label>
                <select required name="level" class="form-control">
                    <option value="">** Select a Skill Level</option>
                    @foreach($levels as $level)
                        <option value="{{ $level->id }}">{{ $level->content }}</option>
                    @endforeach                    
                </select>
            </div>
            <div class="form-group">
                <label for="">Poster</label>
                <input required type="file" name="poster" accept="image/jpeg,image/png,image/jpg" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Teaser</label>
                Format (.mp4), durasi 1 menit
                <input required type="file" name="teaser" accept="video/mp4" class="form-control">
            </div>
            <div class="form-group">
                 <label for="">Short Desc</label>
                  <textarea name="short_desc" id="short_desc" required class='form-control' placeholder="Short Desc"
                  rows='4'></textarea>
            </div>
            <div class="form-group">
                 <label for="">Full Desc</label>
                 
                  <textarea name="desc" id="desc" required class='form-control' placeholder="Full Desc"
                  rows='5'></textarea>
            </div> 
        </div>
        <div class="box-footer">
            <div class="pull-right">
            <input type="submit" class="btn btn-success" value="Save">
            </div>
        </div>
        </form>
    </div>



@push('bottom')
  <script src="{{ asset('/cb_asset/adminlte/plugins/summernote/summernote-bs4.min.js') }}"> </script>
<script>
$(document).ready(function(){
    $('.js-example-basic-multiple').select2(
        {
            tags: true,
            placeholder: "   ** Select Topic",
            // theme: "classic",
            // allowClear: true,     
         }
    );

});

</script>    

<script type="text/javascript">
  $(document).ready(function() {
    $('#desc').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 

</script>
@endpush

@endsection