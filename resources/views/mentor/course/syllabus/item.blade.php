@extends(getThemePath("layout.layout"))
    @section("content")


        <p>
            <button onclick="goBack()"><i class="fa fa-arrow-left"></i> Back To List</button>
        </p>

        <div class="box box-default">
            <div class="box-header">
                <h1 class="box-title">{{ $page_title }} Content</h1>
            </div>
  <div class="box-body">
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Slug</th>
                        <th>Video</th>
                        <th>Desc</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($syllabus_items as $item)
                        <tr>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->slug }}</td>
                            <td>{{ $item->video_url }}</td>
                            <td>{{ $item->desc }}</td>
                            <td>-</td>
                            <td></td>
                           
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>            
          
        </div>

@push('bottom')
<script>
function goBack() {
  window.history.back();
}
</script>    
@endpush
    @endsection