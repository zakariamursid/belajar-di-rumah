@extends(getThemePath("layout.layout"))
    @section("content")


        <p>
            <a href="{{ url($backlink) }}"><i class="fa fa-arrow-left"></i> Back To List</a>
        </p>

        <div class="box box-default">
            <div class="box-header">
                <h1 class="box-title">Add Syllabus Item</h1>
            </div>
            <form method="post" action="{{module()->addSaveUrl()}}">
                {!! csrf_field() !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="">Course Syllabus</label>
                    <select name="" id="" class="form-control">
                        @foreach ($syllabus as $item)
                        <option value="">{{ $item->title }}</option>
                            
                        @endforeach
                    </select>

                </div>
                <div class="form-group">
                    <label for="">Title</label>
                    <input required type="text" name="course_syllabus_id" value="{{ $course_id }}">
                    <input required type="text" placeholder="E.g : John Doe" name="title" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Slug</label>
                    <input required type="text" placeholder="E.g : John Doe" name="slug" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Desc</label>
                    <input required type="text" placeholder="E.g : John Doe" name="desc" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Video</label>
                    <input required type="file" placeholder="E.g : John Doe" name="video_url" class="form-control">
                </div>
            </div>
            <div class="box-footer">
                <input type="submit" class="btn btn-success" value="Add">
            </div>
            </form>
        </div>


    @endsection