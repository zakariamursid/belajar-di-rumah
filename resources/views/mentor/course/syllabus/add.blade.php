@extends(getThemePath("layout.layout"))
    @section("content")


        <p>
            <a href="{{ url($backlink) }}"><i class="fa fa-arrow-left"></i> Back To List</a>
        </p>

        <div class="box box-default">
            <div class="box-header">
                <h1 class="box-title">Add Syllabus</h1>
            </div>
            <form method="post" action="{{url($post)}}">
                {!! csrf_field() !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="">Title</label>
                    <input required type="text" placeholder="E.g : John Doe" name="course_id" hidden value="{{ $course_id }}">
                    <input required type="text" placeholder="E.g : John Doe" name="title" class="form-control">
                </div>
    

            </div>
            <div class="box-footer">
                <input type="submit" class="btn btn-success" value="Add">
            </div>
            </form>
        </div>


    @endsection