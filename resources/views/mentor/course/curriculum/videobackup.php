
@extends(getThemePath("layout.layout"))

@section("content")
    <style>
        .progress { position:relative; width:100%; }
        .bar { background-color: #008000; width:0%; height:20px; }
         .percent { position:absolute; display:inline-block; left:50%; color: #7F98B2;}
   </style>
{{-- @dd($content); --}}
      {{-- <a onclick="{{ url('/user/course/'.$content->course_id.'/content') }}"><i class="fa fa-arrow-left"></i> Back To List</a> --}}
            <a href="{{ url('/user/course/'.$content->course_id.'/content') }}"><i class="fa fa-arrow-left"></i> Back To List</a>

<br>
<br>
     <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Video Detail</h1>
</div>
        <div class="box-body">
                <div class="col-12 col-lg-12">
                    
                    <?php 
                        if($content->video != null)
                        {
                   echo "<video width='640' height='480' controls>
                            <source src='"?>{{ asset($content->video)}}<?php echo"'type='video/mp4'>
                            Your browser does not support the video tag.
                        </video>
                         ";
                        }
                        else {
                            echo '
                             Anda belum mengupload video untuk content ini
                            <br><br>                    
                                                    
                                                       <button class="btn btn-info btn-sm launch-modal">
                                Upload</button>

                            ';
                        }
                    ?>

                        <br>
                        <br>

      <?php 
                        if($content->video != null)
                        {                   
   
   echo '
   <button type="button"  name="delete" id="'?>{{ $id }}<?php echo'" class="delete btn btn-danger btn-sm">Delete</button>            

                            ';                    
                        }
                        ?>
                </div>

           

</div>
</div>


<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Confirmation</h2>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload A Video</h4>
      </div>
      <div class="modal-body">
{{-- <div class="container"> --}}
            <form method="POST" action="{{ action('FileUploadController@fileStore') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                <input type="text" value="{{ $id }}" name="id" hidden>
                    <input name="file" type="file" class="form-control"><br/>
                    <div class="progress">
                        <div class="bar"></div >
                        <div class="percent">0%</div >
                    </div>
                    <br>
                    <input type="submit"  value="Submit" class="btn btn-primary">
                </div>
            </form>    
</div>      
{{-- </div> --}}

    </div>

  </div>
</div>


@push('bottom')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
 
<script>
$(document).ready(function(){
	$('.launch-modal').click(function(){
		$('#myModal').modal({
			backdrop: 'static'
		});
	}); 
});

</script>

<script type="text/javascript">
    $(function() {
         $(document).ready(function()
         {
            var bar = $('.bar');
            var percent = $('.percent');

      $('form').ajaxForm({
        beforeSend: function() {
            var percentVal = '0%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        complete: function(xhr) {
            // alert('File Uploaded Successfully');
            location.reload();

            // window.location.href = "44/fileupload";
        }
      });
   }); 
 });

 
 $(document).on('click', '.delete', function(){
  id = $(this).attr('id');

  $('#confirmModal').modal('show');
 });

 $('#ok_button').click(function(){

  var url =  '{{ route('section-video.destroy', ":id") }}';
url = url.replace(':id', id);

  $.ajax({
        method: "delete",
  data: {'id': id, '_token': "{{ csrf_token() }}", '_method': 'delete'},
          url: url,

   success:function(data)
   {
            location.reload(); // then reload the page.(3)

   }
  })
 });
</script>
    
@endpush

@endsection