
@extends(getThemePath("layout.layout"))
@section("content")
    <style>
        .progress { position:relative; width:100%; }
        .bar { background-color: #008000; width:0%; height:20px; }
         .percent { position:absolute; display:inline-block; left:50%; color: #7F98B2;}
   </style>
{{-- <h1>Content</h1> --}}
        <a onclick="goBack()"><i class="fa fa-arrow-left"></i> Back To List</a>
                    {{-- <button onclick="goBack()"><i class="fa fa-arrow-left"></i> Back To List</button> --}}

<br>
<br>
     <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Course Detail</h1>
            <div class="pull-right">
{{-- <button class="btn-success">Add Curriculum</button> --}}
<button type="button" name="create_record" id="create_record" class="btn btn-primary btn-sm"> New Section</button>
<button type="button" name="create_section" id="create_section" class="btn btn-primary btn-sm"> New Content</button>
{{-- <a href="{{ url('user/course/'.$course->id.'/sort-content') }}"><button type="button" name="create_section" id="create_section" class="btn btn-info btn-sm">
    <li class="fa fa-sort"></li> Sort</button></a> --}}

</div>
                    <div class="box-body">

<table>
    <tr>
        <td  width="200"><b>Title</b> </td>
        <td>: {{ $course->title }}</td>
    </tr>
    <tr>
        <td  width="200"><b>Level</b> </td>
        <td>: {{ $settings->content }}</td>

    </tr>
    <tr>
        <td  width="200"><b>Short Desc</b> </td>
        <td>: {{ $course->short_desc }}</td>
    </tr>
</table>


                    </div>
        </div>
        </div>


                        <h3>Content</h3>
<datagrid></datagrid>
    <p>
        {{-- <a href=""><i class="fa fa-arrow-right"></i> Next to Syllabus</a> --}}
    </p>
    {{-- @dump($curriculums) --}}

        {{-- @dump($curriculums) --}}

    @foreach ($curriculums as $curriculum)
    {{-- @dd($curriculum) --}}

     <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">{{ $curriculum->title }}</h1>
            {{-- <button class="btn-warning">Edit</button> --}}
<div class="pull-right">
<a href="{{ url('user/curriculum/move-up/'.$curriculum->id) }}"><button class="btn btn-sm btn-warning"><li class="fa fa-arrow-up"></li></button></a>            
<a href="{{ url('user/curriculum/move-down/'.$curriculum->id) }}"><button class="btn btn-sm btn-warning"><li class="fa fa-arrow-down"></li></button></a>            
<button type="button" name="edit" id="{{ $curriculum->id }}" class="edit btn btn-success btn-sm">Edit</button>            
<button type="button"  name="delete" id="{{ $curriculum->id }}" class="delete btn btn-danger btn-sm">Delete</button>


        
        </div>
        </div>
        <div class="box-body">

            {{-- <table class="table table-bordered" id="example"> --}}
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>Sort Number</th>
                        <th>Title</th>
                        <th>Video</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    {{-- @dd($curriculum->section) --}}
@foreach ($curriculum->section as $key => $item)
                <tr>
                    <td>{{ $item->sort_number }}</td>
                    <td>{{ $item->title }}</td>
                    {{-- <td>{{ $item->video }}</td> --}}
                    <td>
    {{-- {{ $item->sort_number }} --}}

                        <?php 
                        if($item->video != null)
                        {
                            echo '<a href="'?>{{ url('/user/section/'.$item->id.'/video') }} <?php echo'"><button class="btn btn-info btn-sm">
                                Watch</button></a>';
                        }

                        else if ($item->video == null) {
                            echo "Anda belum mengupload video";
                            echo "&nbsp;&nbsp;&nbsp;";
                            echo '
                                <button type="button" name="sectionupload" id="'?>{{$item->id}}<?php echo'"class="sectionupload btn btn-info btn-sm">Upload</button>    
                                ';
                         
                        }

                        ?>
                        



                    </td>

                    <td>{{ $item->access }}</td>
                    <td>
                        <a href="{{ url('user/curriculum-section/move-up/'.$item->id) }}"><button class="btn btn-sm btn-warning"><li class="fa fa-arrow-up"></li></button></a>            
                        <a href="{{ url('user/curriculum-section/move-down/'.$item->id) }}"><button class="btn btn-sm btn-warning"><li class="fa fa-arrow-down"></li></button></a>                                    
                        <button type="button" name="sectionedit" id="{{ $item->id }}" class="sectionedit btn btn-success btn-sm">Edit</button>            
                        <button type="button" name="sectiondelete" id="{{ $item->id }}" class="sectiondelete btn btn-danger btn-sm">Delete</button>            
                    </td>
                </tr>
@endforeach 
                </tbody>
            </table>
        </div>
    </div>        
    @endforeach



<div id="curriculumModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Section</h4>
        </div>
        <div class="modal-body">
         <span id="form_result"></span>
         <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
          @csrf
          <input name="_method" id="_method" hidden value="POST">
    <br>
    
         <input type="text" name="curriculum_id"   id="curriculum_id" hidden />
        <input type="text" hidden name="course_id"  id="course_id" value="{{ $course->id }}" />

          <div class="form-group row">
            <label class="control-label col-md-2" >Title </label>
            <div class="col-md-10">
             <input type="text" name="title" id="title" class="form-control" />
            </div>
           </div>
   <div class="form-group row">
            <label class="control-label col-md-2" >Access  </label>
                      <div class="col-md-10">
                          <select name="access" id="access" class="form-control">
                              @foreach ($access as $item)
                                  <option value="{{ $item['access'] }}">{{ $item['access'] }}</option>                                  
                              @endforeach
                          </select>
                    </div>
                </div>
                <!-- /.form-group --> 
            
           <br />
           <div class="form-group" align="center">
            <input type="hidden" name="action" id="action" />
            <input type="hidden" name="hidden_id" id="hidden_id" />
            <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add" />
           </div>
         </form>
        </div>
     </div>
    </div>
</div>

<div id="sectionModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Content</h4>
        </div>
        <div class="modal-body">
         <span id="form_section"></span>
         <form method="post" id="section_form" class="form-horizontal" enctype="multipart/form-data">
          @csrf
          <input name="_method" id="_method" hidden value="POST">
    <br>
             {{-- <input type="text" name="curriculum_id"   id="curriculum_id"  /> --}}

        <input type="text" hidden name="section_course_id" id="section_course_id" value="{{ $course->id }}" />
        <input type="text" hidden name="section_id" id="section_id" />
   <div class="form-group row">
            <label class="control-label col-md-2" >Section</label>
                      <div class="col-md-10">
                          <select name="section_curriculum" id="section_curriculum" class="form-control">
                              @foreach ($curriculums as $curriculum)
                                  <option value="{{ $curriculum->id }}">{{ $curriculum->title }}</option>                                  
                              @endforeach
                          </select>
                    </div>
                </div>
                <!-- /.form-group --> 
          <div class="form-group row">
            <label class="control-label col-md-2" >Title </label>
            <div class="col-md-10">
             <input type="text" name="section_title" id="section_title" class="form-control" />
            </div>
           </div>

        <div class="form-group row">
            <label class="control-label col-md-2" >Desc  </label>
                      <div class="col-md-10">
                        <textarea name="section_desc" id="section_desc" cols="30" rows="10" class="form-control"></textarea>

                    </div>
                </div>
                <!-- /.form-group -->            
         <div class="form-group row">
            <label class="control-label col-md-2" >Access  </label>
                      <div class="col-md-10">
                          <select name="section_access" id="section_access" class="form-control">
                              @foreach ($access as $item)
                                  <option value="{{ $item['access'] }}">{{ $item['access'] }}</option>                                  
                              @endforeach
                          </select>
                    </div>
                </div>
                <!-- /.form-group --> 

            
           <br />
           <div class="form-group" align="center">
            <input type="hidden" name="section_action" id="section_action" />
            <input type="hidden" name="hidden_id" id="hidden_id" />
            <input type="submit" name="section_action_button" id="section_action_button" class="btn btn-warning" value="Add" />
           </div>
         </form>
        </div>
     </div>
    </div>
</div>

<div id="uploadModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Video</h4>
        </div>
        <div class="modal-body">
         <span id="form_section"></span>
 
    <br>
              <div class="form-group row">
            <label class="control-label col-md-2" >Title </label>
            <div class="col-md-10">
             <input type="text" readonly name="upload_section_title" id="upload_section_title" class="form-control" />
            </div>
           </div>
             {{-- <input type="text" name="curriculum_id"   id="curriculum_id"  /> --}}

            
         {{-- <div class="form-group row"> --}}
          <form method="POST" action="{{ action('FileUploadController@fileStore') }}" enctype="multipart/form-data">
                @csrf
        {{-- <input type="text" name="sectionid" id="sectionid" /> --}}

                <input type="hidden" name="id" id="sectionid"/>
              <div class="form-group row">
            <label class="control-label col-md-2" >File </label>
            <div class="col-md-10">                
                    <input required name="file" type="file" class="form-control" accept="video/mp4,video/x-m4v,video/*"><br/>
                    </div>
                    </div>
                   
                    <div class="progress">
                        <div class="bar"></div >
                        <div class="percent">0%</div >
                    </div>
                    <br>
            <div class="modal-footer">
                    <button onclick="var e=this;setTimeout(function(){e.disabled=true;},0);return true;" class="btn btn-primary">
                    Upload</Button>
            </div>       
            </form>            

                </div>


        </div>
     </div>
    </div>
</div>



<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Confirmation</h2>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div id="sectionConfirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Confirmation</h2>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="section_ok_button" id="section_ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


@push('bottom')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>

<script>

          $('#uploadModal').on('hidden.bs.modal', function () {
      $(this).find("input").val('').end();
            });
            
          $('#curriculumModal').on('hidden.bs.modal', function () {
   
                $("#curriculum_id").val('');
                // $("#course_id").val('');
                $("#title").val('');
                $("#access").val('');
            });

          $('#sectionModal').on('hidden.bs.modal', function () {
   
                $("#section_id").val('');
                // $("#section_course_id").val('');
                $("#section_title").val('');
                $("#section_desc").val('');
                $("#section_desc").val('');
                $("#section_access").val('');
            });

     $('#create_record').click(function(){
  $('.modal-title').text("Add New Section");
     $('#action_button').val("Add");
     $('#action').val("Add");
     $('#curriculumModal').modal('show');
 });

     $('#create_section').click(function(){
  $('.modal-title').text("Add New Content");
     $('#section_action_button').val("Add");
     $('#section_action').val("Add");
     $('#sectionModal').modal('show');
 });


  $('#sample_form').on('submit', function(event){
  event.preventDefault();
  if($('#action').val() == 'Add')
  {
    //    console.log(';d');

   $.ajax({
       
    url:"{{ route('curriculum-ajax.store') }}",
    // url:"/test",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
           location.reload(); // then reload the page.(3)
    }
   })
  }
 
 });

  $('#section_form').on('submit', function(event){
  event.preventDefault();
  if($('#section_action').val() == 'Add')
  {

   $.ajax({
       
    url:"{{ route('section-ajax.store') }}",
    // url:"/test",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
           location.reload(); // then reload the page.(3)
    }
   })
  }
 
 });


  $('#sample_form').on('submit', function(event){
  event.preventDefault();
  if($('#action').val() == 'Edit')
  {
       console.log(';ddsa');

    $.ajax({
    url:"{{ route('curriculum-ajax.update') }}",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
        console.log(data)
    
     if(data.success)
     {
            location.reload(); // then reload the page.(3)

     }
    }
   });  
 
 }})

  $('#section_form').on('submit', function(event){
  event.preventDefault();
  if($('#section_action').val() == 'Edit')
  {
       console.log(';ddsa');

    $.ajax({
    url:"{{ route('section-ajax.update') }}",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
        console.log(data)
    
     if(data.success)
     {
            location.reload(); // then reload the page.(3)

     }
    }
   });
 
 }})



  $(document).on('click', '.edit', function(){
  var id = $(this).attr('id');
  $('#form_result').html('');
  
  var url =  '{{ route('curriculum-ajax.edit', ":id") }}';
url = url.replace(':id', id);

  $.ajax({
        url: url,

   dataType:"json",
   success:function(html){
    $('#curriculum_id').val(html.data.id);
    $('#course_id').val(html.data.course_id);
    $('#title').val(html.data.title);
    $('#access').val(html.data.access);
    // $("div.country select").val(html.data.countries.name);

    console.log(html.data)
    // $('#desc').val(html.data.desc);
    $('.modal-title').text("Edit Curriculum");
    $('#action_button').val("Update");
    $('#action').val("Edit");
    $('#curriculumModal').modal('show');
   }
  })
 });

  $(document).on('click', '.sectionedit', function(){
  var id = $(this).attr('id');
//   $('#form_section').html('');
  
  var url =  '{{ route('section-ajax.edit', ":id") }}';
url = url.replace(':id', id);
  $.ajax({
        url: url,

   dataType:"json",
   success:function(html){
    $('#section_id').val(html.data.id);
    $('#section_curriculum').val(html.data.curriculum_id);
    $('#section_title').val(html.data.title);
    $('#section_desc').val(html.data.desc);
    $('#section_access').val(html.data.access);



    console.log(html.data)
   $('.modal-title').text("Edit Section");
    $('#section_action_button').val("Update");
    $('#section_action').val("Edit");
    $('#sectionModal').modal('show');
   }
  })
 });


  $(document).on('click', '.sectionupload', function(){
  var id = $(this).attr('id');
//   $('#form_section').html('');
  
  var url =  '{{ route('section-ajax.edit', ":id") }}';
url = url.replace(':id', id);
  $.ajax({
        url: url,

   dataType:"json",
   success:function(html){
    $('#sectionid').val(html.data.id);
    $('#section_curriculum').val(html.data.curriculum_id);
    $('#upload_section_title').val(html.data.title);
    $('#section_desc').val(html.data.desc);
    $('#section_access').val(html.data.access);



    console.log(html.data)
   $('.modal-title').text("Upload Video");
    $('#section_action_button').val("Update");
    $('#section_action').val("Edit");
    // $('#uploadModal').modal('show');
		$('#uploadModal').modal({
			backdrop: 'static'
		});
            $('#uploadModal').modal('show');

    
   }
  })
 });




 $(document).on('click', '.delete', function(){
  id = $(this).attr('id');

  $('#confirmModal').modal('show');
 });

 $('#ok_button').click(function(){

  var url =  '{{ route('curriculum-ajax.destroy', ":id") }}';
url = url.replace(':id', id);

  $.ajax({
        method: "delete",
  data: {'id': id, '_token': "{{ csrf_token() }}", '_method': 'delete'},
          url: url,

   success:function(data)
   {
            location.reload(); // then reload the page.(3)

   }
  })
 });



 $(document).on('click', '.sectiondelete', function(){
  id = $(this).attr('id');

  $('#sectionConfirmModal').modal('show');
 });

 $('#section_ok_button').click(function(){

  var url =  '{{ route('section-ajax.destroy', ":id") }}';
url = url.replace(':id', id);

  $.ajax({
        method: "delete",
  data: {'id': id, '_token': "{{ csrf_token() }}", '_method': 'delete'},
          url: url,

   success:function(data)
   {
            location.reload(); // then reload the page.(3)

   }
  })
 });
function goBack() {
  window.history.back();
}


 </script>
 

<script type="text/javascript">
    $(function() {
         $(document).ready(function()
         {
            var bar = $('.bar');
            var percent = $('.percent');

      $('form').ajaxForm({
        beforeSend: function() {
            var percentVal = '0%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        complete: function(xhr) {
            // alert('File Uploaded Successfully');
            location.reload();

            // window.location.href = "44/fileupload";
        }
      });
   }); 
 });

$(document).ready(function() {
    $('#example').DataTable( {
        // "order": [[ 1, "DESC" ]]
    } );
} );

</script>

@endpush

@endsection