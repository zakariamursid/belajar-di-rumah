{{-- @dd($test) --}}
@extends(getThemePath("layout.layout"))
@section("content")

<datagrid></datagrid>
    <p>
    {{-- <a href="{{ url('/mentor/course/'.$course->id.'/content') }}"><i class="fa fa-arrow-right"></i> Next to Syllabus</a> --}}
    </p>

    
 <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Syllabus</h1>
        </div>
        <div class="box-body">
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Content</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($test as $item)
                        <tr>
                            <td>{{ $item->title }}</td>
                            <td>
                                @foreach ($item->syllabusitem as $row)
                                    {{ $row->title }} ,
                                @endforeach

                                <button class="btn-primary">See</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

      <div class="box box-default">
            <div class="box-header">
                <h1 class="box-title">Add Syllabus</h1>
            </div>
            <form method="post" action="">
                {!! csrf_field() !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="">Title</label>
                    {{-- <input required type="text" placeholder="E.g : John Doe" name="course_id" hidden value="{{ $course_id }}"> --}}
                    <input required type="text" placeholder="E.g : John Doe" name="title" class="form-control">
                </div>
    

            </div>
            <div class="box-footer">
                <input type="submit" class="btn btn-success" value="Add">
            </div>
            </form>
        </div>



    

@endsection