@extends(getThemePath("layout.layout"))
@section("content")

<datagrid></datagrid>
    <p>


        <a href="{{ url($backlink) }}"><i class="fa fa-arrow-left"></i> Back To List</a>
        {{-- <a href="{{ url('/mentor/course/'.$course->id.'/content') }}"><i class="fa fa-arrow-right"></i> Next to Syllabus</a> --}}
    </p>

    <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Coupon</h1>
        </div>
        {{-- <form enctype="multipart/form-data" action="{{ url($url) }}" method="POST"> --}}
        <form enctype="multipart/form-data" action="{{ url('/user/course/'.$course->id.'/pricing/add') }}" method="POST">

    {{-- <form action="{{  }}" method="POST" enctype="multipart/form-data" class="box no-padding"> --}}
    {{-- <form action="{{module()->addSaveURL()}}" method="POST" enctype="multipart/form-data" class="box no-padding"> --}}
            {!! csrf_field() !!}
        <div class="box-body">
            <input type="text" name="mentor_id" value="{{ $course->mentor_id }}" name="mentor_id" hidden>
            <div class="form-group">
                <label for="">Price</label>
                <input required type="text" id="course_price" required readonly class="form-control" value="Rp. {{ $course->price }}">
                <input required type="text" id="price" required  class="form-control hidden" value="{{ $course->price }}">
                <input required type="text" required readonly hidden name="id" value="{{ $course->id }}">
            </div>

            <div class="form-group">
                <label for="">Coupon</label>
                <input  required type="text" maxlength="10" onkeyup="this.value = this.value.toUpperCase();" class="form-control" name="code"  placeholder="Kode kupon">
</div>

<div class="form-check">
               <div class="form-group">
                <label for="">Discount</label>
                <select required name="type" id="type" class="form-control">
                    <option selected disabled>Pilih Tipe Diskon</option>
                    <option value="-">Fix Cost</option>
                    <option value="%">Persentage</option>
                </select>
                <br>

                <input required type="number" name="amount" id="amount" class="form-control validate_number" placeholder="Pilih tipe diskon terlebih dahulu">
            </div>
            <br>

    </div>

         <div class="form-group">
                <label for="">QTY</label>
                <input required type="number" name="qty" class="form-control" placeholder="Ex : 10" min="0">
            </div>

         <div class="form-group">
                <label for="">Expired</label>
       <input placeholder="Kupon berlaku hingga" type="text" class="form-control datepicker" name="valid_until" data-format="d/m/Y">

            </div>
    
    <br>
<div class="form-group pull-right">
                        <input type="submit" class="btn btn-success" value="Generate">

        </div>

        </form>

     {{-- <div class="box-footer">
            <input type="submit" class="btn btn-success" value="Next">
        </div> --}}
        
    </div>
    </div>
    

    
 <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Coupon Exist</h1>
        </div>
        <div class="box-body">
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>Date Created</th>
                        <th>Coupon Code</th>
                        <th>Discount</th>
                        <th>Status</th>
                        <th>Quantity</th>
                        <th>Expired Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($coupons as $coupon)
                    <tr>
                        <td>{{ $coupon->created_at }}</td>
                        <td>{{ $coupon->code }}</td>
                        <td><?php if($coupon->type == "%") { echo $coupon->discount .' ' .$coupon->type ;} else if($coupon->type == "-") { echo $coupon->type .' Rp '.number_format($coupon->discount,0,",",".");} ?></td>
                        <td>{{ $coupon->status }}</td>
                        <td>{{ $coupon->coupon_used }} terpakai dari {{ $coupon->qty }}</td>
                        <td>{{ $coupon->valid_until }}</td>
                        <td>
<button type="button" name="edit" id="{{ $coupon->id }}" class="edit btn btn-success btn-sm">Edit</button>            
<button type="button"  name="delete" id="{{ $coupon->id }}" class="delete btn btn-danger btn-sm">Delete</button>            
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>


<div id="sectionModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Coupon</h4>
        </div>
        <div class="modal-body">
         <span id="form_section"></span>
         <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
          @csrf
          <input name="_method" id="_method" hidden value="POST">
    <br>
             {{-- <input type="text" name="curriculum_id"   id="curriculum_id"  /> --}}

        <input type="text" name="id" id="id" hidden />

  
          <div class="form-group row">
            <label class="control-label col-md-2" >Code </label>
            <div class="col-md-10">
             <input type="text" name="code" id="code" class="form-control" />                     
            </div>
           </div>
          <div class="form-group row">
            <label class="control-label col-md-2" >Quantity </label>
            <div class="col-md-10">
             <input type="text" name="qty" id="qty" class="form-control" />                     
            </div>
           </div>
        <div class="form-group row">
            <label class="control-label col-md-2" >Status</label>
                      <div class="col-md-10">
                          <select name="status" id="status" class="form-control">
                              @foreach ($status as $item)
                                        <option value="{{ $item['status'] }}">{{ $item['status'] }}</option>
                              @endforeach
                          </select>
                    </div>
                </div>

        <div class="form-group row">
            <label class="control-label col-md-2" >Expired </label>
            <div class="col-md-10">
             {{-- <input type="date" name="valid_until" id="valid_until" class="form-control" /> --}}
       <input placeholder="masukkan tanggal akhir" type="text" class="form-control datepicker" id="expired" name="expired" data-format="Y-m-d">             
                {{-- <input type="text" name="tanggal_input" id="tanggal_input" class="form-control" placeholder="Tanggal" > --}}

            </div>
           </div>           
            
           <br />
           <div class="form-group" align="center">
            <input type="hidden" name="action" id="action" />
            <input type="hidden" name="hidden_id" id="hidden_id" />
            <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add" />
           </div>
         </form>
        </div>
     </div>
    </div>
</div>



<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Confirmation</h2>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

    
@push('bottom')
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>

          $('#sectionModal').on('hidden.bs.modal', function () {
   
                $("#id").val('');
                // $("#section_course_id").val('');
                $("#qty").val('');
                $("#status").val('');
                $("#expired").val('');
            });



  $('#sample_form').on('submit', function(event){
  event.preventDefault();
  if($('#action').val() == 'Edit')
  {
       console.log(';ddsa');

    $.ajax({
    url:"{{ route('coupon-ajax.update') }}",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
        console.log(data)
    
     if(data.success)
     {
            location.reload(); // then reload the page.(3)

     }
    }
   });  
 
 }})

 

  $(document).on('click', '.edit', function(){
  var id = $(this).attr('id');
  $('#form_result').html('');
  
  var url =  '{{ route('coupon-ajax.edit', ":id") }}';
url = url.replace(':id', id);

  $.ajax({
        url: url,

   dataType:"json",
   success:function(html){
    $('#id').val(html.data.id);
    $('#status').val(html.data.status);
    $('#code').val(html.data.code);
    $('#qty').val(html.data.qty);
    $('#valid_until').val(html.data.valid_until);
    $('#expired').val(html.data.valid_until);
    // $("div.country select").val(html.data.countries.name);
//   $('#tanggal_input').val(html.data.valid_until);

    console.log(html.data)
    // $('#desc').val(html.data.desc);
    $('.modal-title').text("Edit Coupon");
    $('#action_button').val("Update");
    $('#action').val("Edit");
    $('#sectionModal').modal('show');
   }
  })
 });

 
 $(document).on('click', '.delete', function(){
  id = $(this).attr('id');

  $('#confirmModal').modal('show');
 });

 $('#ok_button').click(function(){

  var url =  '{{ route('coupon-ajax.destroy', ":id") }}';
url = url.replace(':id', id);

  $.ajax({
        method: "delete",
  data: {'id': id, '_token': "{{ csrf_token() }}", '_method': 'delete'},
          url: url,

   success:function(data)
   {
            location.reload(); // then reload the page.(3)

   }
  })
 });

          

</script>

<script>
document.querySelector(".validate_number").addEventListener("keypress", function (evt) {
    if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
    {
        evt.preventDefault();
    }
})

$("#type").on('change', function() {
    var price = $('#price').val();

    if ($(this).val() == '%'){
    $("#amount").attr("placeholder", "Potongan sekian % dari harga course. Ex : 30%");
    $("#amount").attr("min", "1");
    $("#amount").attr("max", "100");
    $("#amount").attr("pattern", "/^-?\d+\.?\d*$/");
    $("#amount").attr("onKeyPress", "if(this.value.length==3) return false;");

    } else if ($(this).val() == '-'){
        // console.log('Fix')
    $("#amount").attr("placeholder", "Potongan sekian IDR dari harga course. Ex : Rp. 100.000");
    $("#amount").attr("min", "1");
    $("#amount").attr("max", price);
    $("#amount").attr("onKeyPress", "if(this.value.length==3) return true;");

    }
});
</script>
@endpush

@endsection