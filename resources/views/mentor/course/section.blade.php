
@extends(getThemePath("layout.layout"))
@section("content")
    <style>
        .progress { position:relative; width:100%; }
        .bar { background-color: #008000; width:0%; height:20px; }
         .percent { position:absolute; display:inline-block; left:50%; color: #7F98B2;}
   </style>
        <a onclick="goBack()"><i class="fa fa-arrow-left"></i> Back To List</a>
        
     <div class="box box-default" style="margin-top: 20px;">
        <div class="box-header">
                <h1 class="box-title">Course Detail</h1>
                <div class="pull-right">
                    <button type="button" name="create_section" id="create_section" class="btn btn-primary btn-sm"> New Section</button>
                    <button type="button" name="create_content" id="create_content" class="btn btn-primary btn-sm"> New Content</button>
                    <a href="{{ url('user/course/'.$course->getId().'/sort-content') }}"><button type="button" name="create_content" id="create_content" class="btn btn-info btn-sm">
                    <li class="fa fa-sort"></li> Sort</button></a>      
                </div>
            <div class="box-body">
                <table>
                    <tr>
                        <td  width="200"><b>Title</b> </td>
                        <td>: {{ $course->getTitle() }}</td>
                    </tr>
                    <tr>
                        <td  width="200"><b>Level</b> </td>
                        <td>: {{ $settings->getContent() }}</td>
                    </tr>
                    <tr>
                        <td  width="200"><b>Short Desc</b> </td>
                        <td>: {{ $course->getShortDesc() }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

        <h3>Content</h3>
        @foreach ($course_sections as $course_section)

        <div class="box box-default">
            <div class="box-header">
                <h1 class="box-title">{{ $course_section->title }}</h1>
                <div class="pull-right">
                    <button type="button" name="edit" id="{{ $course_section->id }}" class="edit btn btn-success btn-sm">Edit</button>            
                    <button type="button"  name="delete" id="{{ $course_section->id }}" class="delete btn btn-danger btn-sm">Delete</button>            
                </div>
            </div>
            
            <div class="box-body">
                <table class="table table-bordered datatable">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Video</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($course_contents as $item)
                        @if($course_section->id == $item->course_section_id)
                            <tr>
                                <td>{{ $item->title }}</td>
                                <td>
                                    @if($item->video != null)
                                        <a href="{{ url('/user/course/content/'.$item->id.'') }}"><button class="btn btn-info btn-sm">Watch</button></a>
                                    @elseif($item->video == null)
                                        Anda belum mengupload video
                                        &nbsp;&nbsp;&nbsp;
                                        <button type="button" name="contentupload" id="{{$item->id}}"class="contentupload btn btn-info btn-sm">Upload</button> 
                                    @endif
                                </td>

                                <td>{{ $item->access }}</td>
                                <td>
                                    <button type="button" name="contentedit" id="{{ $item->id }}" class="contentedit btn btn-success btn-sm">Edit</button>            
                                    <button type="button" name="contentdelete" id="{{ $item->id }}" class="contentdelete btn btn-danger btn-sm">Delete</button>            
                                </td>
                            </tr>
                                    @endif
                    @endforeach                 
                    </tbody>
                </table>
            </div>
        </div>        
    @endforeach

     


    <div id="sectionModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add New Section</h4>
            </div>
            
            <div class="modal-body">
                <span id="form_result"></span>
                <form method="post" id="section_form" class="form-horizontal" enctype="multipart/form-data">
                @csrf
            <input name="_method" id="_method" hidden value="POST">
            <input type="text" name="section_course_section_id" id="section_course_section_id" hidden />
            <input type="text"  name="section_course_id"  id="section_course_id" value="{{ $course->getId() }}" hidden />

            <div class="form-group row">
                <label class="control-label col-md-2" >Title </label>
                <div class="col-md-10">
                <input type="text" name="section_title" id="section_title" class="form-control" />
                </div>
            </div>

        <div class="form-group row">
                <label class="control-label col-md-2" >Access</label>
                        <div class="col-md-10">
                            <select name="section_access" id="section_access" class="form-control">
                                @foreach ($access as $item)
                                    <option value="{{ $item['access'] }}">{{ $item['access'] }}</option>                                  
                                @endforeach
                            </select>
                        </div>
                    </div>
                
            <br />
            <div class="form-group" align="center">
                <input type="hidden" name="section_action" id="section_action" />
                <input type="hidden" name="hidden_id" id="hidden_id" />
                <input type="submit" name="section_action_button" id="section_action_button" class="btn btn-warning" value="Add" />
            </div>
            </form>
            </div>

        </div>
        </div>
    </div>

<div id="contentModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Content</h4>
        </div>
        <div class="modal-body">
         <span id="form_section"></span>
         <form method="post" id="content_form" class="form-horizontal" enctype="multipart/form-data">
          @csrf
          <input name="_method" id="_method" hidden value="POST">
    <br>

        <input type="text" hidden name="content_course_id" id="content_course_id" value="{{ $course->getId() }}" />
        <input type="text" hidden name="content_id" id="content_id" />
   <div class="form-group row">
            <label class="control-label col-md-2" >Section</label>
                      <div class="col-md-10">
                          <select name="content_course_section" id="content_course_section" class="form-control">
                              @foreach ($course_sections as $course_section)
                                  <option value="{{ $course_section->id }}">{{ $course_section->title }}</option>                                  
                              @endforeach
                          </select>
                    </div>
                </div> 
          <div class="form-group row">
            <label class="control-label col-md-2" >Title </label>
            <div class="col-md-10">
             <input type="text" name="content_title" id="content_title" class="form-control" />
            </div>
           </div>

        <div class="form-group row">
            <label class="control-label col-md-2" >Desc  </label>
                      <div class="col-md-10">
                        <textarea name="content_desc" id="content_desc" cols="30" rows="10" class="form-control"></textarea>

                    </div>
                </div>
         <div class="form-group row">
            <label class="control-label col-md-2" >Access  </label>
                      <div class="col-md-10">
                          <select name="content_access" id="content_access" class="form-control">
                              @foreach ($access as $item)
                                  <option value="{{ $item['access'] }}">{{ $item['access'] }}</option>                                  
                              @endforeach
                          </select>
                    </div>
                </div>
           <br />
           <div class="form-group" align="center">
            <input type="hidden" name="content_action" id="content_action" />
            <input type="hidden" name="hidden_id" id="hidden_id" />
            <input type="submit" name="content_action_button" id="content_action_button" class="btn btn-warning" value="Add" />
           </div>
         </form>
        </div>
     </div>
    </div>
</div>

<div id="uploadModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Video</h4>
        </div>
        <div class="modal-body">
         <span id="form_section"></span>
 
    <br>
              <div class="form-group row">
            <label class="control-label col-md-2" >Title </label>
            <div class="col-md-10">
             <input type="text" readonly name="upload_section_title" id="upload_section_title" class="form-control" />
            </div>
           </div>
          <form method="POST" section_action="{{ route('course-content.store') }}" enctype="multipart/form-data">
                @csrf

                <input type="hidden" name="id" id="sectionid"/>
              <div class="form-group row">
            <label class="control-label col-md-2" >File </label>
            <div class="col-md-10">                
                    <input required name="file" type="file" class="form-control"><br/>
                    </div>
                    </div>
                   
                    <div class="progress">
                        <div class="bar"></div >
                        <div class="percent">0%</div >
                    </div>
                    <br>
            <div class="modal-footer">
                    <button onclick="var e=this;setTimeout(function(){e.disabled=true;},0);return true;" class="btn btn-primary">
                    Upload</Button>
            </div>       
            </form>            

                </div>


        </div>
     </div>
    </div>



<div id="sectionConfirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Confirmation</h2>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div id="contentConfirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Confirmation</h2>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="section_ok_button" id="section_ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


@push('bottom')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>

<script>

          $('#uploadModal').on('hidden.bs.modal', function () {
      $(this).find("input").val('').end();
            });
            
          $('#sectionModal').on('hidden.bs.modal', function () {
   
                $("#section_course_section_id").val('');
                $("#section_title").val('');
                $("#section_access").val('');
            });

          $('#contentModal').on('hidden.bs.modal', function () {
   
                $("#content_id").val('');
                $("#content_title").val('');
                $("#content_desc").val('');
                $("#content_access").val('');
            });

     $('#create_section').click(function(){
  $('.modal-title').text("Add New Section");
     $('#section_action_button').val("Add");
     $('#section_action').val("Add");
     $('#sectionModal').modal('show');
 });

     $('#create_content').click(function(){
  $('.modal-title').text("Add New Content");
     $('#content_action_button').val("Add");
     $('#content_action').val("Add");
     $('#contentModal').modal('show');
 });


  $('#section_form').on('submit', function(event){
  event.preventDefault();
  if($('#section_action').val() == 'Add')
  {
    //    console.log(';d');

   $.ajax({
       
    url:"{{ route('section-ajax.store') }}",
    // url:"/test",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
           location.reload(); // then reload the page.(3)
    }
   })
  }
 
 });

  $('#content_form').on('submit', function(event){
  event.preventDefault();
  if($('#content_action').val() == 'Add')
  {
    console.log('verghasuk');
   $.ajax({
       
    url:"{{ route('content-ajax.store') }}",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache:false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
           location.reload(); // then reload the page.(3)
    }
   })
  }
 
 });


  $('#section_form').on('submit', function(event){
  event.preventDefault();
  if($('#section_action').val() == 'Edit')
  {
       console.log(';ddsa');

    $.ajax({
    url:"{{ route('section-ajax.update') }}",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
        console.log(data)
    
     if(data.success)
     {
            location.reload(); // then reload the page.(3)

     }
    }
   });  
 
 }})

  $('#content_form').on('submit', function(event){
  event.preventDefault();
  if($('#content_action').val() == 'Edit')
  {
       console.log('Update Content');

    $.ajax({
    url:"{{ route('content-ajax.update') }}",
    method:"POST",
    data:new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    dataType:"json",
    success:function(data)
    {
        console.log(data)
    
     if(data.success)
     {
            location.reload(); // then reload the page.(3)

     }
    }
   });
 
 }})



  $(document).on('click', '.edit', function(){
  var id = $(this).attr('id');
  $('#form_result').html('');
  
var url =  '{{ route('section-ajax.edit', ":id") }}';
url = url.replace(':id', id);
// console.log('afsf');

  $.ajax({
        url: url,

   dataType:"json",
   success:function(html){
    $('#section_course_section_id').val(html.data.id);
    $('#section_course_id').val(html.data.course_id);
    $('#section_title').val(html.data.title);
    $('#section_access').val(html.data.access);

    console.log(html.data)

    $('.modal-title').text("Edit Section");
    $('#section_action_button').val("Update");
    $('#section_action').val("Edit");
    $('#sectionModal').modal('show');
   }
  })
 });

  $(document).on('click', '.contentedit', function(){
  var id = $(this).attr('id');
  var url =  '{{ route('content-ajax.edit', ":id") }}';
  url = url.replace(':id', id);
  $.ajax({
        url: url,

   dataType:"json",
   success:function(html){

    console.log(html.data)

    $('#content_id').val(html.data.id);
    $('#content_course_section').val(html.data.course_section_id);
    $('#content_title').val(html.data.title);
    $('#content_desc').val(html.data.desc);
    $('#content_access').val(html.data.access);

   $('.modal-title').text("Edit Section");
    $('#content_action_button').val("Update");
    $('#content_action').val("Edit");
    $('#contentModal').modal('show');
   }
  })
 });


  $(document).on('click', '.contentupload', function(){
  var id = $(this).attr('id');
//   $('#form_section').html('');
  
  var url =  '{{ route('content-ajax.edit', ":id") }}';
url = url.replace(':id', id);
  $.ajax({
        url: url,

   dataType:"json",
   success:function(html){
    $('#sectionid').val(html.data.id);
    $('#section_curriculum').val(html.data.curriculum_id);
    $('#upload_section_title').val(html.data.title);
    $('#content_desc').val(html.data.desc);
    $('#content_access').val(html.data.access);



    console.log(html.data)
   $('.modal-title').text("Upload Video");
    $('#content_action_button').val("Update");
    $('#content_action').val("Edit");
    // $('#uploadModal').modal('show');
		$('#uploadModal').modal({
			backdrop: 'static'
		});
            $('#uploadModal').modal('show');

    
   }
  })
 });




 $(document).on('click', '.delete', function(){
  id = $(this).attr('id');

  $('#sectionConfirmModal').modal('show');
 });

 $('#ok_button').click(function(){

  var url =  '{{ route('section-ajax.destroy', ":id") }}';
url = url.replace(':id', id);

  $.ajax({
        method: "delete",
  data: {'id': id, '_token': "{{ csrf_token() }}", '_method': 'delete'},
          url: url,

   success:function(data)
   {
            location.reload(); // then reload the page.(3)

   }
  })
 });



 $(document).on('click', '.contentdelete', function(){
  id = $(this).attr('id');

  $('#contentConfirmModal').modal('show');
 });

 $('#section_ok_button').click(function(){

  var url =  '{{ route('content-ajax.destroy', ":id") }}';
url = url.replace(':id', id);

  $.ajax({
        method: "delete",
  data: {'id': id, '_token': "{{ csrf_token() }}", '_method': 'delete'},
          url: url,

   success:function(data)
   {
            location.reload(); // then reload the page.(3)

   }
  })
 });
function goBack() {
  window.history.back();
}


 </script>
 

<script type="text/javascript">
    $(function() {
         $(document).ready(function()
         {
            var bar = $('.bar');
            var percent = $('.percent');

      $('form').ajaxForm({
        beforeSend: function() {
            var percentVal = '0%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        complete: function(xhr) {
            // alert('File Uploaded Successfully');
            location.reload();

            // window.location.href = "44/fileupload";
        }
      });
   }); 
 });

</script>

@endpush

@endsection