@extends(getThemePath("layout.layout"))
@section("content")
    <p>
        <a href="{{ url('/user/course') }}"><i class="fa fa-arrow-left"></i> Back To List</a>
    </p>

    <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Edit Course</h1>
        </div>
        <form enctype="multipart/form-data" action="{{ url('user/course/'.$course->id.'/update') }}" method="POST">

    {{-- <form action="{{  }}" method="POST" enctype="multipart/form-data" class="box no-padding"> --}}
    {{-- <form action="{{module()->addSaveURL()}}" method="POST" enctype="multipart/form-data" class="box no-padding"> --}}
            {!! csrf_field() !!}
        <div class="box-body">
            <div class="form-group">
                <label for="">Course Title</label>
            <input type="text" name="mentor_id" hidden value="1">
                <input required type="text" placeholder="E.g : VUE JS From Zero To Hero" name="title" class="form-control" value="{{$course->title }}">
            </div>
           <div class="form-group">
                <label for="">Price (IDR)</label>
                <input type="number" min="0" name="price" class="form-control" placeholder="Rp. " value="{{$course->price }}">
            </div>            

           {{-- <b> Basic Info </b> --}}
            <div class="form-group">
                <label for="">Topic</label>
                <select  name="topic[]" id="topic" class="js-example-basic-multiple" multiple="multiple" style="width:100%">
                {{-- <select name="topic_id" class="form-control"> --}}
                    <option value="">** Select topics</option>
                            @foreach($topics as $key => $value)
                                <option <?php foreach($tags as $tag) { if($value->id == $tag->topic_id) { echo 'selected'; }  } ?> value="{{ $value->id }}">{{ $value->name }}</option>
                            @endforeach                    
                </select>
            </div>
            <div class="form-group">
                <label for="">Skill Level</label>
                <select name="level" class="form-control">
                    <option value="">** Select a Skill Level</option>
                    @foreach($levels as $level)
                        <option <?php if($level->id == $course->level) { echo 'selected'; }  ?> value="{{ $level->id }}">{{ $level->content }}</option>
                    @endforeach                    
                </select>
            </div>

            <div class="form-group">
                <label for="">Poster</label>
                @if($course->poster != null || $course->poster != '')   
                    <img src="{{ asset($course->poster) }}" alt="Poster" style="width: 30%; height: 30%"/>
                    <br>
                    <br>
                    <button type="button"  name="delete" id="{{ $course->id }} "class="deleteThumbnail btn btn-danger btn-sm">Delete</button>
                @else
                    <input type="file" name="poster" accept="image/jpeg,image/png,image/jpg" class="form-control">
                @endif
            </div>
            <div class="form-group">
                <label for="">Teaser</label>
                @if($course->teaser != null || $course->teaser != '')
                    <video width='352' height='240' controls>
                        <source src='{{ asset($course->teaser)}}'type='video/mp4'>
                        Your browser does not support the video tag.
                    </video>
                    <br>
                    <br>
                    <button type="button"  name="delete" id="{{ $course->id }} "class="deleteTeaser btn btn-danger btn-sm">Delete</button>
                @else
                    <input type="file" name="teaser" accept="video/mp4" class="form-control">
                @endif
            </div>


          
            {{-- <div class="form-group">
                <label for="">Teaser</label>
            </div> --}}

            <div class="form-group">
                 <label for="">Short Desc</label>
                  <textarea name="short_desc" id="short_desc" required class='form-control' placeholder="Short Desc"
                  rows='4'>{{ $course->short_desc }}</textarea>
            </div>
            <div class="form-group">
                 <label for="">Full Desc</label>
                 
                  <textarea name="desc" id="desc" required class='form-control' placeholder="Full Desc"
                  rows='5'>{{ $course->desc }}</textarea>
            </div> 
        </div>
        <div class="box-footer">
            <div class="pull-right">
            <input type="submit" class="btn btn-success" value="Save">
            </div>
        </div>
        </form>
    </div>

<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Confirmation</h2>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this poster?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

@push('bottom')
  <script src="{{ asset('/cb_asset/adminlte/plugins/summernote/summernote-bs4.min.js') }}"> </script>
<script>
$(document).ready(function(){
    $('.js-example-basic-multiple').select2(
        {
            tags: true,
            placeholder: "   ** Select Topic",
            // theme: "classic",
            // allowClear: true,     
         }
    );

});
 $(document).on('click', '.deleteThumbnail', function(){
  id = $(this).attr('id');

  $('#confirmModal').modal('show');
  
 $('#ok_button').click(function(){

  var url =  '{{ route('course.destroy.thumbnail', ":id") }}';
url = url.replace(':id', id);

  $.ajax({
        method: "delete",
  data: {'id': id, '_token': "{{ csrf_token() }}", '_method': 'delete'},
          url: url,

   success:function(data)
   {
            location.reload(); // then reload the page.(3)

   }
  })
 });
 });

 $(document).on('click', '.deleteTeaser', function(){
  id = $(this).attr('id');

  $('#confirmModal').modal('show');
 $('#ok_button').click(function(){

  var url =  '{{ route('course.destroy.teaser', ":id") }}';
url = url.replace(':id', id);

  $.ajax({
        method: "delete",
  data: {'id': id, '_token': "{{ csrf_token() }}", '_method': 'delete'},
          url: url,

   success:function(data)
   {
            location.reload(); // then reload the page.(3)

   }
  })
 });

 });



</script>    

<script type="text/javascript">
  $(document).ready(function() {
    $('#desc').summernote({
      height: "300px",
      styleWithSpan: false
    });
  }); 

</script>
@endpush

@endsection