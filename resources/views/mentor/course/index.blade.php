
@extends(getThemePath("layout.layout"))
@section("content")

<datagrid></datagrid>
    <p>
        {{-- <a href="{{ url('/mentor/course/'.$course->id.'/content') }}"><i class="fa fa-arrow-right"></i> Next to Syllabus</a> --}}
    </p>

     <div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Show Data</h1>
        </div>
        <div class="box-body">
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Level</th>
                        <th>Topic</th>
                        <th>Poster</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($courses as $course)
                    <tr>
                        <td>{{ $course->title }}</td>
                        <td>{{ $course->level }}</td>
                        <td>{{ $course->topic }}</td>
                        <td><img src="{{ asset($course->poster) }}"height="42" width="42" /></td>
                        {{-- <td>{{ $course->poster }}</td> --}}
                        <td>Rp. {{ $course->price }}</td>
                        <td>
                        <a href="{{ url('/mentor/course/'.$course->id.'/coupon') }}"><button class="btn-warning">Coupon</button></a>
                        <a href="{{ url('/mentor/course/'.$course->id.'/content') }}"><button class="btn-success">Content</button></a>
                        <a href="{{ url('/user/course/edit/'.$course->id.'/coupon') }}"><button class="btn-primary">Edit</button></a>
                        <a href="{{ url('/mentor/course/'.$course->id.'/coupon') }}"><button class="btn-danger">Delete</button></a>
                        </td>
                    </tr>
                        
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>



    

@endsection