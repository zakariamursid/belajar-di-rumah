{{-- @dd($data); --}}
@extends('layouts.topbar')
@section('head')
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('js/custom.css') }}" rel="stylesheet">

@endsection
@section('content')
 <main role="main">

     <main role="main" class="container">
      <div class="row">
        <div class="col-md-8 blog-main" >
<div class="media">
  <div class="media-body">
    <h5 class="mt-0"><b>{{ $data['course']->title }}</b></h5>
    {{ $data['course']->short_desc }}
    <br>
    <br>
   <b> Pengajar : {{ $data['course']->user_name }} </b>
   <br>
   <br>

  <div class="row">
    <div class="col">
      View : 542 <br>
      Created : {{ $data['course']->created_at }}
    </div>
    <div class="col">
      Enrol : 424 <br>
      Last Updated : {{ $data['course']->updated_at }}
    </div>
  </div>

  </div>



</div>

        </div><!-- /.blog-main -->

        <aside class="col-md-4 blog-sidebar">
          <div class="p-3 mb-3 bg-light rounded">
            <div class="embed-responsive embed-responsive-16by9">
  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
</div>
            {{-- <h4 class="font-bold">Rp. {{ $data['course']->price }}</h4> --}}
            <p class="mb-0">
              <div>
                {{-- <button class="btn-danger" style="width: 100%">Buy Now</button> --}}
              </div>

            </p>
          </div>

       
        </aside><!-- /.blog-sidebar -->
        
        <div class="col-md-8 blog-main" >
          <br>
<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Description</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Prepare</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Course Content</a>
  </li>
</ul>
<div class="tab-content" id="pills-tabContent">
  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">Consequat occaecat ullamco amet non eiusmod nostrud dolore irure incididunt est duis anim sunt officia. Fugiat velit proident aliquip nisi incididunt nostrud exercitation proident est nisi. Irure magna elit commodo anim ex veniam culpa eiusmod id nostrud sit cupidatat in veniam ad. Eiusmod consequat eu adipisicing minim anim aliquip cupidatat culpa excepteur quis. Occaecat sit eu exercitation irure Lorem incididunt nostrud.

</div>
  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">Consequat occaecat ullamco amet non eiusmod nostrud dolore irure incididunt est duis anim sunt officia. Fugiat velit proident aliquip nisi incididunt nostrud exercitation proident est nisi. Irure magna elit commodo anim ex veniam culpa eiusmod id nostrud sit cupidatat in veniam ad. Eiusmod consequat eu adipisicing minim anim aliquip cupidatat culpa excepteur quis. Occaecat sit eu exercitation irure Lorem incididunt nostrud.

</div>
  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">Consequat occaecat ullamco amet non eiusmod nostrud dolore irure incididunt est duis anim sunt officia. Fugiat velit proident aliquip nisi incididunt nostrud exercitation proident est nisi. Irure magna elit commodo anim ex veniam culpa eiusmod id nostrud sit cupidatat in veniam ad. Eiusmod consequat eu adipisicing minim anim aliquip cupidatat culpa excepteur quis. Occaecat sit eu exercitation irure Lorem incididunt nostrud.

</div>
</div>        
        </div><!-- /.blog-main -->

        <aside class="col-md-4 blog-sidebar">
          <div class="p-3 mb-3 bg-light rounded">
            <h4 class="font-bold">Rp. {{ $data['course']->price }}</h4>
            <p class="mb-0">
              <div>
                <button class="btn-danger" style="width: 100%">Buy Now</button>
              </div>
              <br>
              <div>
                <b> This Course also include </b>
                <br>
                Consequat occaecat ullamco amet non eiusmod nostrud dolore irure incididunt est duis anim sunt officia. Fugiat velit proident aliquip nisi incididunt nostrud exercitation proident est nisi. Irure magna elit commodo anim ex veniam culpa eiusmod id nostrud sit cupidatat in veniam ad. Eiusmod consequat eu adipisicing minim anim aliquip cupidatat culpa excepteur quis. Occaecat sit eu exercitation irure Lorem incididunt nostrud.
              </div>
            </p>
          </div>

       
        </aside><!-- /.blog-sidebar -->

      </div><!-- /.row -->
    </div>

      
      <div class="album py-5 bg-light">
        <div class="container">
            <b> Other also </b>
            <br>
            <br>

          <div class="row">
            @foreach ($data['course_also'] as $item)
                
            <div class="col-md-3">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text"><b>{{ $item->title }}</b><br>{{ $item->user_name}}<br>Dipelajari oleh 165 orang<br>{{ $item->price }}</p>

                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                      {{-- <button type="button" class="btn btn-sm btn-outline-secondary">Fav</button> --}}
                    </div>
                    <small class="text-muted">Dipelajari oleh <b>9</b> Orang</small>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            
            {{-- Hapus --}}
             <div class="col-md-3">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text"><b>{{ $item->title }}</b><br>{{ $item->user_name}}<br>Dipelajari oleh 165 orang<br>{{ $item->price }}</p>

                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                      {{-- <button type="button" class="btn btn-sm btn-outline-secondary">Fav</button> --}}
                    </div>
                    <small class="text-muted">Dipelajari oleh <b>9</b> Orang</small>
                  </div>
                </div>
              </div>
            </div>
             <div class="col-md-3">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text"><b>{{ $item->title }}</b><br>{{ $item->user_name}}<br>Dipelajari oleh 165 orang<br>{{ $item->price }}</p>

                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                      {{-- <button type="button" class="btn btn-sm btn-outline-secondary">Fav</button> --}}
                    </div>
                    <small class="text-muted">Dipelajari oleh <b>9</b> Orang</small>
                  </div>
                </div>
              </div>
            </div>
             <div class="col-md-3">
              <div class="card mb-4 box-shadow">
                <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&bg=55595c&fg=eceeef&text=Thumbnail" alt="Card image cap">
                <div class="card-body">
                  <p class="card-text"><b>{{ $item->title }}</b><br>{{ $item->user_name}}<br>Dipelajari oleh 165 orang<br>{{ $item->price }}</p>

                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                      <button type="button" class="btn btn-sm btn-outline-secondary">View</button>
                      {{-- <button type="button" class="btn btn-sm btn-outline-secondary">Fav</button> --}}
                    </div>
                    <small class="text-muted">Dipelajari oleh <b>9</b> Orang</small>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </main>
    @endsection