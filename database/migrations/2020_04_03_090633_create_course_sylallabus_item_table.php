<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseSylallabusItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_sylallabus_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('course_syllabus_item_id')->unsigned();
            $table->string('title');
            $table->string('video_url');
            $table->longText('desc');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_sylallabus_item');
    }
}
