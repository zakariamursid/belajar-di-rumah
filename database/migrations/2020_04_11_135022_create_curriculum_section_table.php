<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurriculumSectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('curriculum_section');

        Schema::create('curriculum_section', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('curriculum_id')->unsigned();
            $table->string('title');            
            $table->string('video');            
            $table->longText('desc')->nullable();           
            $table->string('access')->default("UNPUBLISHED");            
            $table->string('permission')->default("WAITING ADMIN");            
            $table->integer('short_number')->nullable();                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculum_section');
    }
}
