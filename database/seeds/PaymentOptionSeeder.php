<?php

use Illuminate\Database\Seeder;

class PaymentOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_options')->delete();

        $payment_options = array(
            array('bank_id' => '1', 'account_name' => 'PT. Taman Media Indonesia','branch' => '', 'account_number' => '1515253253255' , 'status' => 'Aktif'),
            array('bank_id' => '2', 'account_name' => 'PT. Taman Media Indonesia','branch' => '', 'account_number' => '658458732459874' , 'status' => 'Aktif'),
            array('bank_id' => '3', 'account_name' => 'PT. Taman Media Indonesia','branch' => '', 'account_number' => '4267654735' , 'status' => 'Aktif'),
            // array('bank_id' => '', 'account_name' => '','branch' => '', 'status' => ''),
        );
        
        DB::table('payment_options')->insert($payment_options);        
    }
}
