<?php

use Illuminate\Database\Seeder;

class BankTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bank')->delete();

        $bank = array(
            array('name' => 'Bank Central Asia', 'code' => '014'),
            array('name' => 'Mandiri', 'code' => '008'),
            array('name' => 'Bank Rakyat Indonesia', 'code' => '002'),
        );
        
        DB::table('bank')->insert($bank);
        

    }
}
