@extends('crudbooster::themes.adminlte.layout.layout')
@section('content')
 <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ $count_courses->count }}</h3>

                <p>Courses</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                {{-- <h3>53<sup style="font-size: 20px">%</sup></h3> --}}
                <h3>{{ $count_enroll->count }}</h3>

                <p>Enroll</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                        <h1><?=  "Rp " . number_format($earn,0,',','.');  ?></h1>

                <p>Earn</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>

            </div>
          </div>


      <div class="row">
        <div class="col-md-12 blog-main" >
                <h3 class="mt-0"><b>Last Enroll</b></h3>

<div class="box box-default">
        <div class="box-header">
            <h1 class="box-title">Show Data</h1>
        </div>
        <div class="box-body">
            <table class="table table-bordered datatable">
                <thead>
                    <tr>
                        <th>Date Purchase</th>
                        <th>Buyer Name</th>
                        <th>Course</th>
                        <th>Earn</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($order as $item)
                    <tr>
                        <td> {{ $item->order_created }} </td>
                        <td> {{ $item->buyer_name }} </td>
                        <td> {{ $item->course_title }} </td>
                        <td> Rp. {{ $item->order_total }} </td>
                        {{-- <td>{{ $item->title }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->title }}</td> --}}
                    </tr>  
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    </div>
    </div>



@endsection