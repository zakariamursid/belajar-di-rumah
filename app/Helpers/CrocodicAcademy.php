<?php

namespace App\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Image;
use DateTime;

class CrocodicAcademy
{
    public static function postEditUser($input)
    {
        $client = new \GuzzleHttp\Client();

        $params =
        [
            [
                'name'     => 'name',
                'contents'     => $input['name'],
            ],
            [
                'name'     => 'email',
                'contents'     => $input['email'],
            ],
        ];    
 
           
        if(array_key_exists('password',$input))
        {
            $password =
            [
                [
                    'name'     => 'password',
                    'contents'     => $input['password'],
                ],
            ];   
    
            $params = array_merge($params,$password);
        }

        // dd($params);

                
        $ca_sync = DB::table('crocodic_academy_sync')->where('status','Aktif')->get();


        foreach ($ca_sync as $key => $value)
        {
            $client->request('POST', $value->url .'api-crocodic-academy/postEditUser',
            [
                'multipart' => $params
            ]);
        }

    }

    public static function postTryLoginCA($params)
    {
        $client = new \GuzzleHttp\Client();
        $params =
        [
            [
                'name'     => 'email',
                'contents'     => $params['email'],
            ],
            [
                'name'     => 'password',
                'contents'     => $params['password'],
            ],
        ];
        

        $ca_sync = DB::table('crocodic_academy_sync')->where('status','Aktif')->get();
                
        foreach ($ca_sync as $key => $value)
        {
            $response = $client->request('POST', $value->url .'api-crocodic-academy/postTryLoginCA', [ 'multipart' => $params ]);
            $response = $response->getBody()->getContents();
            $response = json_decode($response, true);

            if($response['api_status'] == 1)
            {
                $input = $response['data'];

                if(!array_key_exists("cb_roles_id",$input))
                {
                    $input["cb_roles_id"] = 2;
                }
                    
                    
                $response = DB::table("users")
                        ->insert([
                            "created_at"=>$input["created_at"],
                            "name"=> $input["name"],
                            "email"=> $input["email"],
                            "password"=>$input["password"],
                            "cb_roles_id"=> $input["cb_roles_id"]
                        ]);
                return response()->json($response);
            }
        }           
    }

    public static function postAddUser($params)
    {
        $client = new \GuzzleHttp\Client();

        $params =
        [
            [
                'name'     => 'name',
                'contents'     => $params['name'],
            ],
            [
                'name'     => 'email',
                'contents'     => $params['email'],
            ],
            [
                'name'     => 'password',
                'contents'     => $params['password'],
            ],
        ];

                $ca_sync = DB::table('crocodic_academy_sync')->where('status','Aktif')->get();
                
                foreach ($ca_sync as $key => $value)
                {
                    $client->request('POST', $value->url .'api-crocodic-academy/postAddUser',
                    [
                        'multipart' => $params
                    ]);
                }        
    }

   
  
}