<?php
namespace App\Models;

use DB;
use Crocodicstudio\Cbmodel\Core\Model;

class CurriculumSection extends Model
{
    public static $tableName = "curriculum_section";

    public static $connection = "mysql";

    
	private $id;
	private $curriculum_id;
	private $title;
	private $video;
	private $desc;
	private $access;
	private $permission;
	private $sort_number;
	private $created_at;
	private $updated_at;


    
	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public static function findAllByCurriculumId($value) {
		return static::simpleQuery()->where('curriculum_id',$value)->get();
	}

	/**
	* @return Curriculum
	*/
	public function getCurriculumId() {
		return Curriculum::findById($this->curriculum_id);
	}

	public function setCurriculumId($curriculum_id) {
		$this->curriculum_id = $curriculum_id;
	}

	public static function findAllByTitle($value) {
		return static::simpleQuery()->where('title',$value)->get();
	}

	public static function findByTitle($value) {
		return static::findBy('title',$value);
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public static function findAllByVideo($value) {
		return static::simpleQuery()->where('video',$value)->get();
	}

	public static function findByVideo($value) {
		return static::findBy('video',$value);
	}

	public function getVideo() {
		return $this->video;
	}

	public function setVideo($video) {
		$this->video = $video;
	}

	public static function findAllByDesc($value) {
		return static::simpleQuery()->where('desc',$value)->get();
	}

	public static function findByDesc($value) {
		return static::findBy('desc',$value);
	}

	public function getDesc() {
		return $this->desc;
	}

	public function setDesc($desc) {
		$this->desc = $desc;
	}

	public static function findAllByAccess($value) {
		return static::simpleQuery()->where('access',$value)->get();
	}

	public static function findByAccess($value) {
		return static::findBy('access',$value);
	}

	public function getAccess() {
		return $this->access;
	}

	public function setAccess($access) {
		$this->access = $access;
	}

	public static function findAllByPermission($value) {
		return static::simpleQuery()->where('permission',$value)->get();
	}

	public static function findByPermission($value) {
		return static::findBy('permission',$value);
	}

	public function getPermission() {
		return $this->permission;
	}

	public function setPermission($permission) {
		$this->permission = $permission;
	}

	public static function findAllBySortNumber($value) {
		return static::simpleQuery()->where('sort_number',$value)->get();
	}

	public static function findBySortNumber($value) {
		return static::findBy('sort_number',$value);
	}

	public function getSortNumber() {
		return $this->sort_number;
	}

	public function setSortNumber($sort_number) {
		$this->sort_number = $sort_number;
	}

	public static function findAllByCreatedAt($value) {
		return static::simpleQuery()->where('created_at',$value)->get();
	}

	public static function findByCreatedAt($value) {
		return static::findBy('created_at',$value);
	}

	public function getCreatedAt() {
		return $this->created_at;
	}

	public function setCreatedAt($created_at) {
		$this->created_at = $created_at;
	}

	public static function findAllByUpdatedAt($value) {
		return static::simpleQuery()->where('updated_at',$value)->get();
	}

	public static function findByUpdatedAt($value) {
		return static::findBy('updated_at',$value);
	}

	public function getUpdatedAt() {
		return $this->updated_at;
	}

	public function setUpdatedAt($updated_at) {
		$this->updated_at = $updated_at;
	}


}