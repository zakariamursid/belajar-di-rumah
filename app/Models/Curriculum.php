<?php
namespace App\Models;

use DB;
use Crocodicstudio\Cbmodel\Core\Model;

class Curriculum extends Model
{
    public static $tableName = "curriculum";

    public static $connection = "mysql";

    
	private $id;
	private $course_id;
	private $title;
	private $sort_number;
	private $access;
	private $created_at;
	private $updated_at;


    
	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public static function findAllByCourseId($value) {
		return static::simpleQuery()->where('course_id',$value)->get();
	}

	public static function findByCourseId($value) {
		return static::findBy('course_id',$value);
	}

	public function getCourseId() {
		return $this->course_id;
	}

	public function setCourseId($course_id) {
		$this->course_id = $course_id;
	}

	public static function findAllByTitle($value) {
		return static::simpleQuery()->where('title',$value)->get();
	}

	public static function findByTitle($value) {
		return static::findBy('title',$value);
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public static function findAllBySortNumber($value) {
		return static::simpleQuery()->where('sort_number',$value)->get();
	}

	public static function findBySortNumber($value) {
		return static::findBy('sort_number',$value);
	}

	public function getSortNumber() {
		return $this->sort_number;
	}

	public function setSortNumber($sort_number) {
		$this->sort_number = $sort_number;
	}

	public static function findAllByAccess($value) {
		return static::simpleQuery()->where('access',$value)->get();
	}

	public static function findByAccess($value) {
		return static::findBy('access',$value);
	}

	public function getAccess() {
		return $this->access;
	}

	public function setAccess($access) {
		$this->access = $access;
	}

	public static function findAllByCreatedAt($value) {
		return static::simpleQuery()->where('created_at',$value)->get();
	}

	public static function findByCreatedAt($value) {
		return static::findBy('created_at',$value);
	}

	public function getCreatedAt() {
		return $this->created_at;
	}

	public function setCreatedAt($created_at) {
		$this->created_at = $created_at;
	}

	public static function findAllByUpdatedAt($value) {
		return static::simpleQuery()->where('updated_at',$value)->get();
	}

	public static function findByUpdatedAt($value) {
		return static::findBy('updated_at',$value);
	}

	public function getUpdatedAt() {
		return $this->updated_at;
	}

	public function setUpdatedAt($updated_at) {
		$this->updated_at = $updated_at;
	}


}