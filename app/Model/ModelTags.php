<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Helpers\Web;

class ModelTags extends Model
{
        private static $table_name = "tags";

        private $topic_id;
        private $course_id;

    function setCourse($course_id) { $this->course_id = $course_id; }
    function getCourse() { return $this->course_id; }

    function setTopic($topic_id) { $this->topic_id = $topic_id; }
    function getTopic() { return $this->topic_id; }

    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->get();

        return $ret;
    }
    public static function getBy($val,$key)
    {
        $ret = DB::table(static::$table_name)
        ->where($val,$key)
            ->get();

        return $ret;
    }
    public static function getRecomendation($topic,$course)
    {
        $ret = DB::table(static::$table_name)
        ->join('course','course.id','=','tags.course_id')
        ->where("topic_id",$topic)
        ->where("course_id", "!=",$course)
        ->where('course.status','Approved')
        ->orderBy('course.created_at', 'desc')        
        ->limit(3)
        ->get();

        return $ret;
    }
    public static function check($course,$topic)
    {
        $ret = DB::table(static::$table_name)
            ->where("course_id", $course)
            ->where("topic_id", $topic)
            ->first();

        return $ret;
    }

  // TODO SAVE DATA
    public function add()
    {

        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => Web::DateNow(),
                "course_id" => $this->getCourse(),
                "topic_id" => $this->getTopic(),
            ]);

        return $save;
    }        

    // TODO DELETE DATA
    public static function remove($course, $topic)
    {
        $save = DB::table(static::$table_name)
            ->where("course_id",$course)
            ->where("topic_id",$topic)
            ->delete();
        return $save;
    }
        
}
