<?php

namespace App\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ModelFAQ extends Model
{
   private static $table_name = "faq";
   public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->get();

        return $ret;
    }

}
