<?php

namespace App\Model;
use Illuminate\Support\Facades\DB;
use DateTime;

use Illuminate\Database\Eloquent\Model;

class ModelSettings extends Model
{
    private static $table_name = "settings";
    private $id;
    private $name;
    private $content;

    function setName($name) { $this->name = $name; }
    function getName() { return $this->name; }
    
    function setContent($content) { $this->content = $content; }
    function getContent() { return $this->content; }

    public static function getBy($val, $key)
    {
        $ret = DB::table(static::$table_name)
            ->where($val, $key)
            ->get();

        return $ret;
    }
    public static function findBy($val, $key)
    {
        $ret = DB::table(static::$table_name)
            ->where($val, $key)
            ->first();

        return $ret;
    }


 // TODO UPDATE DATA
    public function edit($key, $id)
    {
        $data['updated_at'] = new dateTime();
  
        if ($this->getName()) {
            $data['name'] = $this->getName();
        }
       
        if ($this->getContent()) {
            $data['content'] = $this->getContent();
        }

     $save = DB::table(static::$table_name)
            ->where($key,$id)
            ->update($data);
            
        return $save;
    }

    // TODO SAVE DATA
    public function add()
    {
        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => new dateTime(),
                "name" => $this->getName(),
                "content" => $this->getContent(),
            ]);

        return $save;
    }    



}
