<?php

namespace App\Model;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class ModelBank extends Model
{
    private $name;
    private $code;
    private static $table_name = "bank";

    
    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->get();

        return $ret;
    }


}
