<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ModelCommision extends Model
{
    private static $table_name = "commission";

    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->first();

        return $ret;
    }

}
