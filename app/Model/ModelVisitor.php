<?php

namespace App\Model;
use DateTime;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class ModelVisitor extends Model
{
    private $unique_has;
    private $course_id;

    function setUniqueHash($unique_has) { $this->unique_has = $unique_has; }
    function getUniqueHash() { return $this->unique_has; }

    function setCourse($course_id) { $this->course_id = $course_id; }
    function getCourse() { return $this->course_id; }


    private static $table_name = "visitor";

     public static function cek($unique_has,$course_id)
    {
        $ret = DB::table(static::$table_name)
            ->where("unique_hash",$unique_has)
            ->where("course_id",$course_id)
            ->first();

        return $ret;
    }   
    // TODO SAVE DATA
    public function add()
    {
        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => new dateTime(),
                "unique_hash" => $this->getUniqueHash(),
                "course_id" => $this->getCourse(),
            ]);

        return $save;
    }


    public static function countBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->where($key, $val)
        ->count();
        return $ret;
    }


    
}
