<?php

namespace App\Model;
use DateTime;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ModelSyllabus extends Model
{
        private $course_id;
        private $title;

    function setCourse($course_id) { $this->course_id = $course_id; }
    function getCourse() { return $this->course_id; }

    function setTitle($title) { $this->title = $title; }
    function getTitle() { return $this->title; }

    private static $table_name = "course_syllabus";
    
    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->get();

        return $ret;
    }

    public static function getByCourseId($id)
    {
        $ret = DB::table(static::$table_name)->where('course_id',$id)
            ->get();

        return $ret;
    }

    public static function getById($id)
    {
        $ret = DB::table(static::$table_name)->where('id',$id)
            ->first();

        return $ret;
    }


  // TODO SAVE DATA
    public function add()
    {

        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => new dateTime(),
                "title" => $this->getTitle(),
                "course_id" => $this->getCourse(),
            ]);

        return $save;
    }        

}
