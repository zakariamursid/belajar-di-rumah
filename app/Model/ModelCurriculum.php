<?php

namespace App\Model;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class ModelCurriculum extends Model
{


    protected $table = "curriculum";
    private static $table_name = "curriculum";
    private $id;
    private $course_id;
    private $title;
    private $access;

    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    function setCourse($course_id) { $this->course_id = $course_id; }
    function getCourse() { return $this->course_id; }
    
    function setTitle($title) { $this->title = $title; }
    function getTitle() { return $this->title; }

    function setAccess($access) { $this->access = $access; }
    function getAccess() { return $this->access; }

    function setSort($sort_number) { $this->sort_number = $sort_number; }
    function getSort() { return $this->sort_number; }    
    
    public function section()
    { 
        return $this->hasMany('App\Model\ModelCurriculumSection','curriculum_id');
    }

    public static function getByCourseId($course_id)
    {
        // $ret = DB::table(static::$table_name)->where('course_id',$course_id)
        //     ->first();
        $sortDirection = "ASC";

        $ret = ModelCurriculum::with(['section' => function ($query) use ($sortDirection) {
            $query->orderBy('sort_number', $sortDirection);
        }])
        ->where('course_id',$course_id)
        ->orderBy('sort_number','ASC')
        ->get();



        return $ret;
    }
    public static function getById($id)
    {
        // $ret = DB::table(static::$table_name)->where('course_id',$course_id)
        //     ->first();
        $ret = ModelCurriculum::with('section')->where('id',$id)->first();

        return $ret;
    }


    public static function getBy($val, $key)
    {
        $ret = DB::table(static::$table_name)->where($val,$key)
            ->first();

        return $ret;
}
    

 // TODO SAVE DATA
    public function add()
    {

        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => new dateTime(),
                "course_id" => $this->getCourse(),
                "sort_number" => $this->getSort(),
                "title" => $this->getTitle(),
                "access" => $this->getAccess(),
            ]);

        return $save;
    }        

 // TODO UPDATE DATA
    public function edit($key, $id)
    {
        $data['updated_at'] = new dateTime();
  
        if ($this->getTitle()) {
            $data['title'] = $this->getTitle();
        }
       
        if ($this->getAccess()) {
            $data['access'] = $this->getAccess();
        }

        $save = DB::table(static::$table_name)
            ->where($key,$id)
            ->update($data);
            
        return $save;
    }

    // TODO DELETE DATA
    public static function remove($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->delete();
        return $save;
    }

}
