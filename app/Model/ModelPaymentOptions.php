<?php

namespace App\Model;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class ModelPaymentOptions extends Model
{
        private $bank_id;
        private $account_name;
        private $account_number;
        private $branch;
        private $status;

    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    function setAccountName($account_name) { $this->account_name = $account_name; }
    function getAccountName() { return $this->account_name; }

    function setAccountNumber($account_number) { $this->account_number = $account_number; }
    function getAccountNumber() { return $this->account_number; }

    function setBrach($branch) { $this->branch = $branch; }
    function getBrach() { return $this->branch; }

    function setStatus($status) { $this->status = $status; }
    function getStatus() { return $this->status; }


    private static $table_name = "payment_options";

    public static function getAktifById($key)
    {
        $ret = DB::table(static::$table_name)
            ->join('bank','bank.id','=','curriculum_section.curriculum_id')
            ->where("id", $key)
            ->get();

        return $ret;
    }
    public static function getPrimary()
    {
        $ret = DB::table(static::$table_name)
            ->select("payment_options.*", "bank.name as bank_name")
            ->join("bank","bank.id","=","payment_options.bank_id")
            ->where("status", "Primary")
            ->orderBy('updated_at', 'DESC')
            ->first();

        return $ret;
    }

    public static function checkById($id)
    {
        $ret = DB::table(static::$table_name)
            ->where("id", $id)
            ->first();

        return $ret;
    }

// TODO UPDATE DATA
    public static function updateStatus($id)
    {
 
        $save = DB::table(static::$table_name)
            ->where("id", "!=",$id)
            ->update(['status' => "Non Primary"]);
            
        return $save;
    }     
}
