<?php

namespace App\Model;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class ModelPurchasedCourse extends Model
{
    private static $table_name = "payments";


    public static function getBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->select('orders.id as order_id', 'users.name as mentor_name',
         'course.id as course_id','course.title as course_title','course.poster as course_poster')
        ->join('orders', function ($join){
        $join->on('orders.id', '=', 'payments.order_id');
        }) 
        ->join('course', function ($join){
        $join->on('course.id', '=', 'orders.course_id');
        }) 
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })
        ->where($key, $val)
        ->get();

        return $ret;
    }     
   
    public static function getWhereWaitingBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->select('orders.id as order_id', 'users.name as mentor_name',
         'course.id as course_id','course.title as course_title','course.poster as course_poster')
        ->join('orders', function ($join){
        $join->on('orders.id', '=', 'payments.order_id');
        }) 
        ->join('course', function ($join){
        $join->on('course.id', '=', 'orders.course_id');
        }) 
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })
        ->where("payments.status", "Pending")
        ->get();

        return $ret;
    }     
   
    public static function getPurchasedCourse($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->select('orders.id as order_id', 'users.name as mentor_name',
         'course.id as course_id','course.title as course_title','course.poster as course_poster')
        ->join('orders', function ($join){
        $join->on('orders.id', '=', 'payments.order_id');
        }) 
        ->join('course', function ($join){
        $join->on('course.id', '=', 'orders.course_id');
        }) 
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })
        ->where("payments.status", "Pending")
        ->get();

        return $ret;
    }     
   
}
