<?php

namespace App\Model;
use DateTime;
use App\Helpers\Web;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ModelCourse extends Model
{
    private $id;
    private $title;
    private $level;
    // private $topic_id;
    private $short_desc;
    private $desc;
    private $status;
    private $price;
    private $poster;
    private $teaser;
    private $mentor_id;
    
    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    function setTitle($title) { $this->title = $title; }
    function getTitle() { return $this->title; }

    function setPrice($price) { $this->price = $price; }
    function getPrice() { return $this->price; }

    function setLevel($level) { $this->level = $level; }
    function getLevel() { return $this->level; }

    function setTopic_id($topic_id) { $this->topic_id = $topic_id; }
    function getTopic_id() { return $this->topic_id; }

    function setShort_desc($short_desc) { $this->short_desc = $short_desc; }
    function getShort_desc() { return $this->short_desc; }

    function setStatus($status) { $this->status = $status; }
    function getStatus() { return $this->status; }

    function setDesc($desc) { $this->desc = $desc; }
    function getDesc() { return $this->desc; }

    function setPoster($poster) { $this->poster = $poster; }
    function getPoster() { return $this->poster; }

    function setTeaser($teaser) { $this->teaser = $teaser; }
    function getTeaser() { return $this->teaser; }

    function setMentor_id($mentor_id) { $this->mentor_id = $mentor_id; }
    function getMentor_id() { return $this->mentor_id; }

    private static $table_name = "course";
    
    public static function get()
    {
        $ret = DB::table(static::$table_name)
            ->select('course.id as course_id' ,'course.created_at as course_created', 'course.status as course_status', 'course.price as course_price', 'course.title as course_title', 'users.name as mentor_name'
            ,DB::raw("((SELECT COUNT(b.id) FROM orders as b WHERE b.course_id = course.id  AND b.status = 'Success')) as enroll,
            ((SELECT SUM(c.balance) FROM admin_wallet as c join orders as b on c.transaction_id = b.id WHERE b.course_id = course.id AND c.transaction_id = b.id  AND c.type = 'DEBIT')) as sum ")            
            )
            ->join('users','users.id','=','course.mentor_id')
            ->orderBy('course_created', 'desc')        
            ->get();

        return $ret;
    }

    public static function getLatest()
    {
        $ret = DB::table(static::$table_name)
            ->select('course.id as course_id' ,'course.created_at as course_created', 'course.status as course_status', 'course.price as course_price', 'course.title as course_title', 'users.name as mentor_name'
            ,DB::raw("((SELECT COUNT(b.id) FROM orders as b WHERE b.course_id = course.id  AND b.status = 'Success')) as enroll,
            ((SELECT SUM(c.balance) FROM admin_wallet as c join orders as b on c.transaction_id = b.id WHERE b.course_id = course.id AND c.transaction_id = b.id  AND c.type = 'DEBIT')) as sum ")            
            )
            ->join('users','users.id','=','course.mentor_id')
            ->orderBy('course_created', 'desc')        
            ->limit(10)      
            ->get();

        return $ret;
    }
    public static function getPending()
    {
        $ret = DB::table(static::$table_name)
            ->select('course.id as course_id','course.status as course_status','course.created_at as course_created', 'course.price as course_price', 'course.title as course_title', 'users.name as mentor_name')
            ->join('users','users.id','=','course.mentor_id')
            ->where('course.status','Pending')
            ->get();

        return $ret;
    }

    public static function getWithMentor()
    {
        $ret = DB::table(static::$table_name)
        ->select('course.*', 'users.id as user_id','users.name as mentor_name')
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })        
        ->get();   
        
        return $ret;

    }


    public static function countMentor()
    {
        $ret = DB::table('users')
        ->distinct()
        ->where('cb_roles_id', 2)
         ->count();        
        return $ret;
    }


    public static function count()
    {
        $ret = DB::table(static::$table_name)
        ->distinct()
        // ->where('cb_roles_id', 2)
         ->count();        
        return $ret;
    }

    public static function getWithMentorBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->select('course.*', 'users.id as user_id','users.name as mentor_name',DB::raw("((SELECT COUNT(b.id) FROM orders as b WHERE b.course_id = course.id  AND b.status = 'Success')) as enroll"))
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })
        ->where($key,$val)        
        ->first();  
        
        return $ret;
    }

    public static function getSimmiliarCourse($course_id,$topic_id)
    {
        $ret = DB::table(static::$table_name)
        ->select('course.*', 'users.id as user_id','users.name as mentor_name')
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })        
        ->where('course.topic_id',$topic_id)
        ->where('course.id','!=',$course_id)
        ->where('course.status','Approved')
        ->orderBy('course.created_at', 'desc')        
        ->limit(3)        
        ->get();   
        
        return $ret;
    }
    public static function getWithMentorForIndex()
    {
        $ret = DB::table(static::$table_name)
        ->select('course.*', 'users.id as user_id','users.name as mentor_name',DB::raw("((SELECT COUNT(b.id) FROM orders as b WHERE b.course_id = course.id  AND b.status = 'Success')) as enroll"))
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })        
        ->where('course.status','Approved')
        ->orderBy('course.created_at', 'desc')        
        ->limit(8)        
        ->get();   
        
        return $ret;
    }
    public static function getAllAproved()
    {
        $ret = DB::table(static::$table_name)
        ->select('course.*', 'users.id as user_id','users.name as mentor_name',DB::raw("((SELECT COUNT(b.id) FROM orders as b WHERE b.course_id = course.id  AND b.status = 'Success')) as enroll"))
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })        
        ->where('course.status','Approved')
        ->orderBy('course.created_at', 'desc')        
        ->paginate(8);   
        
        return $ret;
    }

    public static function getTopCourseForIndex()
    {
        $ret = DB::table(static::$table_name)
        ->distinct()
        ->select('course.*', 'users.id as user_id','users.name as mentor_name'
        ,DB::raw("((SELECT COUNT(b.id) FROM orders as b WHERE b.course_id = course.id AND status = 'Success')) as enroll")
        )
        ->join('orders', function ($join){
        $join->on('orders.course_id', '=', 'course.id');
        })
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })
        ->where('orders.status','Success')
        ->limit(6)        
        ->get();   

        // ->orderBy('course.created_at', 'desc')        
        // ->limit(6)        
        
        return $ret;
    }


    public static function getBySession($id)
    {
        $ret = DB::table(static::$table_name)
        ->select('course.id','course.title', 'course.level','field.name as topic', 'course.poster as poster', 'course.price as price')
           ->join('field', function ($join){
            $join->on('course.topic_id', '=', 'field.id');
            })        
        ->where('mentor_id', $id)
            ->get();

        return $ret;
    }
    public static function findBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->where($key, $val)
        ->get();

        return $ret;
    }

        public static function getIdLatestCourse($mentor_id)
    {
        $ret = DB::table(static::$table_name)->where('mentor_id',cb()->session()->id())->orderBy('created_at', 'desc')->first();

        return $ret;
    }

    public static function getById($id)
    {
        $ret = DB::table(static::$table_name)
        ->select('course.*', 'users.id as user_id','users.name as mentor_name')
        ->join('users', function ($join){
        $join->on('users.id', '=', 'course.mentor_id');
        })        
        ->where('course.id',$id)
            ->first();

        return $ret;
    }

    // TODO SAVE DATA
    public static function store($data)
    {
        $save = DB::table(static::$table_name)
            ->insert($data);
        return $save;
    }

    // TODO SAVE DATA
    public function add()
    {
        $save = DB::table(static::$table_name)
            ->insert([
                "created_at" => new dateTime(),
                "title" => $this->getTitle(),
                "level" => $this->getLevel(),
                "topic_id" => $this->getTopic_id(),
                // "banner" => $this->getImage(),
                "short_desc" => $this->getShort_desc(),
                "desc" => $this->getDesc(),
                // "preparation" => $this->getRoles_id(),
                // "include" => $this->getLatitude(),
                "mentor_id" => $this->getMentor_id(),
            ]);

        return $save;
    }

// TODO ACC PENDING COURSE
    public function updateStatus($id)
    {
        $data['updated_at'] = new dateTime();
        $data['status'] = $this->getStatus();

        $save = DB::table(static::$table_name)
            ->where("id",$id)
            ->update($data);
            
        return $save;
    }    


    public static function countByWithMentorId($key, $val, $user_id)
    {
        $ret = DB::table(static::$table_name)
        ->where("mentor_id", $user_id)
        ->where($key, $val)
        ->count();
        return $ret;
    }

    public static function countBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->where($key, $val)
        ->count();
        return $ret;
    }
      

// TODO UPDATE DATA
    public function edit($key, $id)
    {
        $data['updated_at'] = Web::DateNow();
  
        if ($this->getTitle()) {
            $data['title'] = $this->getTitle();
        }
  
        if ($this->getLevel()) {
            $data['level'] = $this->getLevel();
        }
       
        $data['price'] = $this->getPrice();
        
        if ($this->getShort_desc()) {
            $data['short_desc'] = $this->getShort_desc();
        }
        if ($this->getDesc()) {
            $data['desc'] = $this->getDesc();
        }
        if ($this->getMentor_id()) {
            $data['mentor_id'] = $this->getMentor_id();
        }
        if ($this->getPoster()) {
            $data['poster'] = $this->getPoster();
        }
        if ($this->getTeaser()) {
            $data['teaser'] = $this->getTeaser();
        }

        $save = DB::table(static::$table_name)
            ->where($key,$id)
            ->update($data);
            
        return $save;
    } 
}
