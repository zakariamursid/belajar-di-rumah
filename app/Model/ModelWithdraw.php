<?php

namespace App\Model;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class ModelWithdraw extends Model
{
    //
    private static $table_name = "withdrawal";
    private $bank;
    private $branch;
    private $users;
    private $amount;
    private $account_name;
    private $account_number;
    private $status;

   function setUsers($users) { $this->users = $users; }
   function getUsers() { return $this->users; }    
   function setBank($bank) { $this->bank = $bank; }
   function getBank() { return $this->bank; }    
   function setBranch($branch) { $this->branch = $branch; }
   function getBranch() { return $this->branch; }    

   function setAmount($amount) { $this->amount = $amount; }
   function getAmount() { return $this->amount; } 

   function setAccountName($account_name) { $this->account_name = $account_name; }
   function getAccountName() { return $this->account_name; }    

   function setAccountNumber($account_number) { $this->account_number = $account_number; }
   function getAccountNumber() { return $this->account_number; }    
  
   function setStatus($status) { $this->status = $status; }
   function getStatus() { return $this->status; }    

        // TODO SAVE DATA
    public function add()
    {
        $data['users_id'] = $this->getUsers();
        $data['amount'] = $this->getAmount();
        $data['branch'] = $this->getBranch();
        $data['bank'] = $this->getBank();
        $data['account_name'] = $this->getAccountName();
        $data['account_number'] = $this->getAccountNumber();
        $data['status'] = "Pending";
        $data['created_at'] = new DateTime();

       
            // dd($data);
        $save = DB::table(static::$table_name)
            ->insert($data);

        return $save;

    }
    
    public static function getByUsersId($id)
    {
        $ret = DB::table(static::$table_name)
        ->select('withdrawal.*', 'bank.name as bank_name')
        ->join('bank', function ($join){
        $join->on('withdrawal.bank', '=', 'bank.id');
        })          
        ->where("withdrawal.users_id", $id)
            ->get();

        return $ret;
    }
    
    public static function getByUsersIdWherePending($id)
    {
        $ret = DB::table(static::$table_name)
        ->select('withdrawal.*', 'bank.name as bank_name')
        ->join('bank', function ($join){
        $join->on('withdrawal.bank', '=', 'bank.id');
        })          
        ->where("withdrawal.users_id", $id)
        ->where("withdrawal.status","Pending")
            ->first();

        return $ret;
    }
    
    public static function getBy($key, $val)
    {
        $ret = DB::table(static::$table_name)
        ->select('withdrawal.*', 'bank.name as bank_name')
        ->join('bank', function ($join){
        $join->on('withdrawal.bank', '=', 'bank.id');
        })          
        ->where($key, $val)
            ->first();

        return $ret;
    }



}
