<?php

namespace App\Model;
use Illuminate\Support\Facades\DB;
use DateTime;

use Illuminate\Database\Eloquent\Model;

class ModelCurriculumSection extends Model
{
    protected $table = "curriculum_section";
    private static $table_name = "curriculum_section";
    private $id;
    private $title;
    private $curriculum_id;
    private $video;
    private $desc;
    private $access;
    private $permission;
    private $sort_number;

    function setId($id) { $this->id = $id; }
    function getId() { return $this->id; }

    function setCurriculum($curriculum_id) { $this->curriculum_id = $curriculum_id; }
    function getCurriculum() { return $this->curriculum_id; }
    
    function setTitle($title) { $this->title = $title; }
    function getTitle() { return $this->title; }

    function setDesc($desc) { $this->desc = $desc; }
    function getDesc() { return $this->desc; }    

    function setVideo($video) { $this->video = $video; }
    function getVideo() { return $this->video; }    

    function setAccess($access) { $this->access = $access; }
    function getAccess() { return $this->access; }    

    function setPermission($permission) { $this->permission = $permission; }
    function getPermission() { return $this->permission; }    

    function setSort($sort_number) { $this->sort_number = $sort_number; }
    function getSort() { return $this->sort_number; }    

                
    public function curriculum()
    { 
        return $this->belongsTo('App\Model\ModelCurriculum','id');
    }


 // TODO SAVE DATA
    public function add()
    {

      if ($this->getTitle()) {
            $data['title'] = $this->getTitle();
        }
       
        if ($this->getCurriculum()) {
            $data['curriculum_id'] = $this->getCurriculum();
        }
      if ($this->getTitle()) {
            $data['title'] = $this->getTitle();
        }
       
        if ($this->getDesc()) {
            $data['desc'] = $this->getDesc();
        }
       
        if ($this->getAccess()) {
            $data['access'] = $this->getAccess();
        }
       
        if ($this->getVideo()) {
            $data['video'] = $this->getVideo();
        }
       
        if ($this->getPermission()) {
            $data['permission'] = $this->getPermission();
        }
       
        if ($this->getSort()) {
            $data['sort_number'] = $this->getSort();
        }


        $save = DB::table(static::$table_name)
            ->insert($data);

        return $save;
    }        

    public static function getByCourse($val, $key)
    {
        $ret = DB::table(static::$table_name)
            ->join('curriculum','curriculum.id','=','curriculum_section.curriculum_id')
            ->where($val, $key)
            ->get();

        return $ret;
    }

    // public static function getBy($val, $key)
    // {
    //     $ret = DB::table(static::$table_name)
    //         ->join('curriculum','curriculum.id','=','curriculum_section.curriculum_id')
    //         ->where($val, $key)
    //         ->get();            ->where("id", $key)


    //     return $ret;
    // }

    public static function getById($key)
    {
        $ret = DB::table(static::$table_name)
            ->select('curriculum_section.*','curriculum.course_id as course_id','curriculum.access as curriculum_access')
            ->join('curriculum','curriculum.id','=','curriculum_section.curriculum_id')        
            ->where("curriculum_section.id", $key)
            ->first();

        return $ret;
    }

// TODO UPDATE DATA
    public function edit($key, $id)
    {
        $data['updated_at'] = new dateTime();
  
        if ($this->getCurriculum()) {
            $data['curriculum_id'] = $this->getCurriculum();
        }
        if ($this->getTitle()) {
            $data['title'] = $this->getTitle();
        }
       
        if ($this->getAccess()) {
            $data['access'] = $this->getAccess();
        }
        if ($this->getDesc()) {
            $data['desc'] = $this->getDesc();
        }
       
        if ($this->getVideo()) {
            $data['video'] = $this->getVideo();
        }
       
        if ($this->getSort()) {
            $data['sort_number'] = $this->getSort();
        }

        $save = DB::table(static::$table_name)
            ->where($key,$id)
            ->update($data);
            
        return $save;
    }    
 public static function getWithCourseBy($val, $key)
    {
        $ret = DB::table(static::$table_name)
            ->select('curriculum_section.*','curriculum.course_id as course_id')
            ->join('curriculum','curriculum.id','=','curriculum_section.curriculum_id')
            ->where($val,$key)
            ->first();
        // $ret = ModelCurriculum::with('section')->where('id',$id)->first();

        return $ret;
    }
    public static function getBy($val, $key)
    {
        $ret = DB::table(static::$table_name)->where($val,$key)
            ->first();
        // $ret = ModelCurriculum::with('section')->where('id',$id)->first();

        return $ret;
    }

    // TODO DELETE DATA
    public static function remove($key, $id)
    {
        $save = DB::table(static::$table_name)
            ->where($key, $id)
            ->delete();
        return $save;
    }

}
