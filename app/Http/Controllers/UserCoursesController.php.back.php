<?php

namespace App\Http\Controllers;
use App\Model\ModelPurchasedCourse;
use App\Model\ModelPayments;
use App\Model\ModelBank;
use App\Model\ModelCourse;
use App\Model\ModelOrders;
use App\Model\ModelAdminWallet;
use App\Model\ModelCommission;
use App\Model\ModelField;
use App\Model\ModelCoupon;
use App\Model\ModelCurriculum;
use App\Model\ModelCurriculumSection;
use Illuminate\Http\Request;
use Auth;
use DB;

class UserCoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [];
        $auth =  Auth::user();
        if($auth != null)
        {   
        $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
        $data['success_order'] = ModelOrders::getByUserAndStatus(Auth::user()->id, "Success");
        $data['pending_order'] = ModelOrders::getByUserAndStatus(Auth::user()->id,"Pending");
        return view('users.mycourses', $data);            
         }

         else {
             return redirect('/');
         }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // echo $id;
 
        $data = [];
  
        $auth =  Auth::user();
        if($auth != null)
        {   
        $data['session'] = DB::table('users')->where('id', Auth::user()->id)->first();
 $data['section_id'] = '';
        $data['order'] = ModelOrders::getById($id);
        // dd($data);
        $data['content'] = ModelCurriculum::getByCourseId($data['order']->course_id);        
        $data['course'] = ModelCourse::getWithMentorBy("course.id",$data['order']->course_id);        
        // if()
        // if()
        $data['page_title'] = $data['course']->title;
        
        if(isset($_GET['content'])) {

        $data['section_id'] = $_GET['content'];
        
        if($data['section_id'] == "")
        {
              $data['section']  = [
                  'video' => $data['course']->teaser,
              ];
        }

        else
        {
        $data['section'] = ModelCurriculumSection::getById($data['section_id']);
        }

    }
        return view('users.course.index', $data);        
         }
    
        

         else {
             return redirect('/');
         }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
