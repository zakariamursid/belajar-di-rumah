<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use App\Model\ModelCurriculum;
use App\Model\ModelOrders;
use App\Model\ModelCourse;
use App\Model\ModelPayments;
use crocodicstudio\crudbooster\controllers\partials\ButtonColor;
use Illuminate\Http\Request;
use App\Helpers\Web;

class AdminOrdersController extends CBController {


    public function cbInit()
    {
        $this->setTable("orders");
        $this->setPermalink("orders");
        $this->setPageTitle("Orders");
        $this->addSelectTable("Course","course_id",["table"=>"course","value_option"=>"id","display_option"=>"title","sql_condition"=>""]);
		$this->addSelectTable("User","users_id",["table"=>"users","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		// $this->addSelectTable("Coupon","coupon",["table"=>"coupon","value_option"=>"id","display_option"=>"code","sql_condition"=>""])->required(false);
        $this->addText("status",'status');
        $this->addMoney("Course Price","course_price");
		$this->addText("Coupon","coupon")->required(false)->strLimit(150)->maxLength(255);
		$this->addText("Coupon Discount","coupon_discount")->required(false)->strLimit(150)->maxLength(255);
		$this->addMoney("Grand Total","grand_total");
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
        $this->setButtonAdd(false);
        $this->setButtonDelete(false);
        $this->setButtonEdit(false);
        
    $this->hookIndexQuery(function($query) {

        $query->where("mentor_id", cb()->session()->id());

        return $query;
    });

        // $this->addActionButton("",function($row) {
        //     // dd($row);
        // return url("user/orders/".$row->primary_key."/content?id=");
        // }, function($row) { return true;}, "fa fa-tasks", ButtonColor::YELLOW, false);
    }

//     public function getDetail($id)
//     {
//         $data = [];
//         $data['page_title'] = "A";
//         $data['section'] =  NULL ;
// dd($data);
//         $data['order'] = ModelOrders::getById($id);
//         $data['course'] = ModelCourse::getWithMentorBy("course.id",$data['order']->course_id);
//         $data['content'] = ModelCurriculum::getByCourseId($data['order']->course_id);

         
//         // return view('public.content', $data);     
//        }


}
