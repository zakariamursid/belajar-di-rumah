<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;
use app\Helpers\CrocodicAcademy;

class AdminUsersController extends CBController {


    public function cbInit()
    {
        $this->setTable("users");
        $this->setPermalink("users");
		$this->setPageTitle("Users");
		
		$this->hookIndexQuery(function($query) {
			$query->where("cb_roles_id", "!=" , 1)
			->orderBy("users.created_at", "Desc");
			return $query;
		});

		$this->hookAfterInsert(function($last_insert_id)
		{
			$data = DB::table('users')->where('id',$last_insert_id)->first();
			$input['name'] = $data->name;
			$input['email'] = $data->email;
			$input['password'] = $data->password;
			$input['cb_roles_id'] = $data->cb_roles_id;
			CrocodicAcademy::postAddUser($input);
			// dd($input);
		});

		$this->hookAfterUpdate(function($id)
		{
			$data = DB::table('users')->where('id',$id)->first();
			$input['name'] = $data->name;
			$input['email'] = $data->email;
			$input['password'] = $data->password;
			$input['cb_roles_id'] = $data->cb_roles_id;
			CrocodicAcademy::postEditUser($input);
		});



	    // Hide delete button with a condition
		$this->setButtonDelete(false);

		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
        $this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addEmail("Email","email");
		$this->addDatetime("Email Verified At","email_verified_at")->showIndex(false)->showEdit(false);
		$this->addText("Password","password")->showIndex(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addText("Remember Token","remember_token")->showIndex(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addDatetime("Updated At","updated_at")->showIndex(false)->showEdit(false)->required(false)->showAdd(false)->showEdit(false);
		$this->addImage("Photo","photo")->required(false)->showIndex(false)->encrypt(true);
		$this->addSelectTable("Role","cb_roles_id",["table"=>"cb_roles","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addText("Ip Address","ip_address")->showIndex(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addText("User Agent","user_agent")->showIndex(false)->showEdit(false)->strLimit(150)->maxLength(255);
		$this->addDatetime("Login At","login_at")->showIndex(false)->showEdit(false);
		
$this->button_edit = false;


    }
}
