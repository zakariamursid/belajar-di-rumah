<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminBankController extends CBController {


    public function cbInit()
    {
        $this->setTable("bank");
        $this->setPermalink("bank");
        $this->setPageTitle("Bank");

        $this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addText("Code","code")->strLimit(150)->maxLength(255);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

    }
}
