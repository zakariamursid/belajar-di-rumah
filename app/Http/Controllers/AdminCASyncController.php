<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminCASyncController extends CBController {


    public function cbInit()
    {
        $this->setTable("crocodic_academy_sync");
        $this->setPermalink("ca_sync");
        $this->setPageTitle("CA Sync");

        $this->addText("Name","name")->strLimit(150)->maxLength(255);
		$this->addText("Url","url")->strLimit(150)->maxLength(255);
		$this->addSelectOption("Status","status")->options(['Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif']);
		

    }
}
