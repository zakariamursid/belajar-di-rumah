<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;
use DB;
use Illuminate\Support\Facades\Request;

use App\Repositories\CurriculumRepository;

class AdminCurriculumController extends CBController {

    public function cbInit()
    {
        $this->setTable("curriculum");
        $this->setPermalink("curriculum");
        $this->setPageTitle("Curriculum");

        $this->addSelectTable("Course","course_id",["table"=>"course","value_option"=>"id","display_option"=>"title","sql_condition"=>""]);
		$this->addText("Title","title")->strLimit(150)->maxLength(255);
		$this->addNumber("Short Number","short_number")->required(false);
		$this->addSelectOption("Access","access")->options(['PRIVATE'=>'PRIVATE','PUBLIC'=>'PUBLIC']);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

    }

	public function getNewSort($id)
	{

        $curriculum = CurriculumRepository::findById($id);

		if (Request::segment(3) === 'move-up')
			{

				if($curriculum->getSortNumber() == 1)
				{
						// CRUDBooster::redirect(CRUDBooster::adminPath('booth'),'Booth adalah booth dengan urutan teratas', 'danger');
				}

				elseif($curriculum->getSortNumber() > 1)
				{
					$above = DB::table('curriculum')->where('course_id',$curriculum->getCourseId())->where('sort_number',$curriculum->getSortNumber() - 1)->first();
                    $above = CurriculumRepository::findById($above->id);

                    $above->setSortNumber($curriculum->getSortNumber());
                    $above->save();

                    $curriculum->setSortNumber($above->getSortNumber()-1);
                    $curriculum->save();
                    
				}
			}

		elseif(Request::segment(3) === 'move-down')
			{

					$above = DB::table('curriculum')->where('course_id',$curriculum->getCourseId())->where('sort_number',$curriculum->getSortNumber() + 1)->first();

					if($above == NULL)
					{
					}

					elseif($above != NULL)
					{
                        $above = CurriculumRepository::findById($above->id);
                        
                        if($above != NULL)
                        {
                            $above->setSortNumber($curriculum->getSortNumber());
                            $above->save();
                        }

                        $curriculum->setSortNumber($above->getSortNumber()+1);
                        $curriculum->save();                        
                    }
			}
            
        return redirect()->back()->with('success', 'your message,here');   
	}

}
