<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;

use crocodicstudio\crudbooster\controllers\CBController;
use Illuminate\Support\Facades\DB;
use App\Model\ModelCoupon;
use App\Model\ModelCourse;

class AdminCouponController extends CBController {


    public function cbInit()
    {
        $this->setTable("coupon");
        $this->setPermalink("coupon");
        $this->setPageTitle("Coupon");

        $this->addSelectTable("Mentor","mentor_id",["table"=>"users","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addText("Code","code")->strLimit(150)->maxLength(255);
		$this->addNumber("Qty","qty");
		$this->addText("Discount","discount")->strLimit(150)->maxLength(255);
		$this->addNumber("Percentage","percentage")->required(false);
		$this->addSelectOption("Status","status")->options(['Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif']);
		$this->addDatetime("Val Until","valid_until");
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

	}
	
	public function getEdit($id)
	{
		    $data['page_title'] = 'Course';
			$data['course'] = ModelCourse::getById($id);
			$data['coupons'] = ModelCoupon::getCouponByCourseId($id);
			// dd($data);
			// dd($data);

		// $data['course'] = DB::table('course')->where('id',$id)->first();
		return view('course.add2', $data);
		
		// dd($data);

	}

	 public function postAdd(Request $request)
    {
		// echo $request->date;
		$expired = str_replace('/', '-', $request->valid_until);
		$expired = date('Y-m-d', strtotime($expired));
		// dd($date);
		$discount = '';
		$course = ModelCourse::getById($request->id);	
		// echo $request->discount;
		if($request->type == '-')
		{
			$discount = $request->amount;
		}
		else if($request->discount == '%')
		{
			$discount = $course	->price * ($request->amount/100);
		}

		// dd($discount);
		// $coupon = ModelCoupon::getCouponByCourseId($request->id);
		
		$check_coupon = DB::table('coupon')->where('course_id',$request->id)->where('code',$request->code)->get()->toArray();
		// dd($check_coupon);
		// if($check_coupon == null)
		if(!empty($check_coupon)) 
		{
		return redirect()->back();

		}

		else if(empty($check_coupon)) 
		{
		$save = new ModelCoupon;
		$save->setCourse($request->id);
		$save->setMentor($request->mentor_id);
		$save->setCode($request->code);
		$save->setDiscount($request->amount);
		$save->setType($request->type);
		$save->setQty($request->qty);
		$save->setStatus('Aktif');
		$save->setValid_until($expired);
		$save->add();

		return redirect()->back();
		}


		// echo $discount;


		// echo $request->code;
		//  $course = new ModelCourse;
		         
        //  DB::table('course')
        //     ->where('id', $request->id)
		// 	->update('price', $request->price);
		
		

        // return $save;

        //  $save->SetTitle($request->title);
        //  $save->setLevel($request->level);
        //  $save->setTopic_id($request->topic_id);
        //  $save->setShort_desc($request->short_desc);
        //  $save->setDesc($request->desc);
        //  $save->setMentor_id($request->mentor_id);
        //  $save->add();

	}
}
