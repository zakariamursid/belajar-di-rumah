<?php

namespace App\Http\Controllers;
use App\Model\ModelCommission;
use Illuminate\Http\Request;
use App\Model\ModelWallet;
use App\Model\ModelOrders;
use App\Model\ModelCourse;
use App\Model\ModelAdminWallet;
use App\Model\ModelPayments;

class AdminCommisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
             $session =   cb()->session()->id();
            
            if($session == null)
            {
                return redirect("/user/login");

            }
            else
            {
        $data['post'] = "user/update-commission";
        $data['commission'] = ModelCommission::get();
        // dd($data);
        $data['page_title'] = "Settings";
        return view('admin.commision', $data);
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        if($request->mentor + $request->admin <= 100)
        {

        $save = new ModelCommission();
        $save->setAdmin($request->admin);
        $save->setMentor($request->mentor);
        $save->edit();

        return redirect('user/commission');
        }

        else {
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function add($id)
    {
        // echo $id;
        $check = ModelPayments::getBy("id",$id);
        // dump($check);
        if($check->status == "Approved")
        {
            $order = ModelOrders::getById($check->order_id);
            $course = ModelCourse::getById($order->course_id);
            
            $update = new ModelOrders;
            $update->setStatus("Success");
            $update->updateStatus($order->id);
            // dd($order);


            $commission = ModelCommission::get();
            
            $admin = $course->price * ($commission->admin/100);
            $mentor = $course->price * ($commission->mentor/100);
            // dd($order);

            $mutasi_admin = ModelAdminWallet::cek($order->id, "DEBIT");
            $mutasi_mentor = ModelWallet::cek($order->id, "DEBIT");

           $mutasi_mentor_kredit = ModelWallet::cek($order->id, "KREDIT");

            if(!empty($mutasi_mentor_kredit) || $mutasi_mentor_kredit != null)
            {
                // dd($mutasi_mentor_kredit);
                ModelWallet::remove("id",$mutasi_mentor_kredit->id);
            }
            
            $mutasi_admin_kredit = ModelAdminWallet::cek($order->id, "KREDIT");
            
            if(!empty($mutasi_admin_kredit) || $mutasi_admin_kredit != null)
            {
                ModelAdminWallet::remove("id",$mutasi_admin_kredit->id);
            }


            if(empty($mutasi_mentor) || $mutasi_mentor == null)
            {
  


            $desc = "Pembelian : ".$course->title." - Grand Total : Rp. ".$order->grand_total ." Komisi : Rp. $mentor";
        
            $wallet = new ModelWallet;
            $wallet->setBalance($mentor);
            $wallet->setUser($course->user_id);
            $wallet->setOrder($order->id);
            $wallet->setDesc($desc);            
            $wallet->setType("DEBIT");
            $wallet->add();
                

            }

            if(empty($mutasi_admin) || $mutasi_admin == null)
            {

            $desc = "Pembelian : ".$course->title." - Grand Total : Rp. ".$order->grand_total ." Komisi : Rp. $admin";

            // $desc = "Pembelian ".$course->title."Sebesar $order->grand_total";

            $admin_wallet = new ModelAdminWallet;
            $admin_wallet->setBalance($admin);
            $admin_wallet->setDesc($desc);
            $admin_wallet->setType("DEBIT");
            $admin_wallet->setOrder($order->id);
            $admin_wallet->add();
            }    
             


            return redirect('/user/payments');

            // dd($order);
        }
        else
        {

         $order = ModelOrders::getById($check->order_id);
            $course = ModelCourse::getById($order->course_id);
            
            $update = new ModelOrders;
            $update->setStatus("Pending");
            $update->updateStatus($order->id);


            $commission = ModelCommission::get();
            
            $admin = $course->price * ($commission->admin/100);
            $mentor = $course->price * ($commission->mentor/100);
            // dd($order);

            $mutasi_admin = ModelAdminWallet::cek($order->id, "KREDIT");
            $mutasi_mentor = ModelWallet::cek($order->id, "KREDIT");
            $desc = "Status : "."$check->status";

            if(empty($mutasi_mentor) || $mutasi_mentor == null)
            {
            
        
            $wallet = new ModelWallet;
            $wallet->setBalance($mentor);
            $wallet->setUser($course->user_id);
            $wallet->setOrder($order->id);
            $wallet->setDesc($desc);            
            $wallet->setType("KREDIT");
            $wallet->add();
               
            }

            else
            {
                $wallet = new ModelWallet;
                $wallet->setDesc($desc);    
                $wallet->updateStatus($mutasi_mentor->id);        

            }

            if(empty($mutasi_admin) || $mutasi_admin == null)
            {


            // $desc = "Pembelian ".$course->title."Sebesar $order->grand_total";

            $admin_wallet = new ModelAdminWallet;
            $admin_wallet->setBalance($admin);
            $admin_wallet->setDesc($desc);
            $admin_wallet->setType("KREDIT");
            $admin_wallet->setOrder($order->id);
            $admin_wallet->add();
            }    

            else
            {
                $admin_wallet = new ModelAdminWallet;
                $admin_wallet->setDesc($desc);    
                $admin_wallet->updateStatus($mutasi_admin->id);    
            }
             


            return redirect('/user/payments');            

            // return redirect('/user/payments');
        }
    }
}
