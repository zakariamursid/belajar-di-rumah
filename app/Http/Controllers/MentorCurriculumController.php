<?php

namespace App\Http\Controllers;
use App\Model\ModelCurriculum;
use App\Model\ModelCourse;
use App\Model\ModelSettings;
use App\Model\ModelCurriculumSection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\CurriculumRepository;


class MentorCurriculumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        // dd($id);
        $data['backlink'] = "/user/course";
        
        $data['curriculums'] = ModelCurriculum::getByCourseId($id);
        // $data['curriculums'] = ModelCurriculum::join('curriculum_section', 'curriculum.id', '=', 'curriculum_section.curriculum_id')->orderBy('curriculum_section.sort_number', 'ASC')->select('curriculum.*')->get();
        // dd($data);

        // var_dump($data['curriculums']);

        $data['section'] = ModelCurriculumSection::getByCourse("curriculum.course_id",$id);
        $data['course'] = ModelCourse::getById($id);
        $data['settings'] = ModelSettings::findBy("id",$data['course']->level);
        // dd($data);
        $data['access'] =
        [
            array('access' => 'PUBLIC'),
            array('access' => 'PRIVATE'),
        ];
        $data['permissions'] =
        [
            array('permission' => 'WAITING ADMIN'),
            array('permission' => 'APPROVED'),
            array('permission' => 'REJECTED'),
            array('permission' => 'REPORTED'),
        ];
        // dd($data);
        $data['page_title'] = "";
        return view('mentor.course.curriculum.index',$data);  
    }

    public function sort($id)
    {
        // dd($id);
                    $data['backlink'] = "/user/course";
        
        $data['curriculums'] = ModelCurriculum::getByCourseId($id);
        $data['section'] = ModelCurriculumSection::getByCourse("curriculum.course_id",$id);
        $data['course'] = ModelCourse::getById($id);
        $data['settings'] = ModelSettings::findBy("id",$data['course']->level);
        // dd($data);
        $data['access'] =
        [
            array('access' => 'PUBLIC'),
            array('access' => 'PRIVATE'),
        ];
        $data['permissions'] =
        [
            array('permission' => 'WAITING ADMIN'),
            array('permission' => 'APPROVED'),
            array('permission' => 'REJECTED'),
            array('permission' => 'REPORTED'),
        ];
        // dd($data);
        $data['page_title'] = "";
        return view('mentor.course.curriculum.sort',$data);  
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $latest_sorting = DB::table('curriculum')->where('course_id',$request->course_id)->max('sort_number');

    	$save = new ModelCurriculum;
		$save->setCourse($request->course_id);
		$save->setTitle($request->title);
        $save->setAccess($request->access);
        $save->setSort($latest_sorting + 1);
        
		$save->add();
        
        return response()->json(['success' => 'Data Added successfully.', 'data' => $request->course_id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          if(request()->ajax())
        {
            $data = ModelCurriculum::getBy('id', $id);
            // $data = Destination::with('countries')->where('id',$id)->first();
            return response()->json(['data' => $data]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
            $save = new ModelCurriculum;
            $save->setTitle($request->title);
            $save->setAccess($request->access);
            $save->edit("id",$request->curriculum_id);

   
        return response()->json(['success' => 'Data Added successfully.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = ModelCurriculumSection::getById($id);
        
        $affected = DB::table('curriculum')->where('course_id',$delete->course_id)->where('sort_number', '>',$delete->sort_number)->get();
      
        foreach($affected as $key => $val)
        {
            $curriculum = CurriculumRepository::findById($val->id);
            $curriculum->setSortNumber($val->sort_number - 1);
            $curriculum->save();
        }



        $del = ModelCurriculum::remove("id",$id);
        // $del = ModelCurriculum::remove("id",$id);
        
        return response()->json(['success' => 'Success']);
    }
}
