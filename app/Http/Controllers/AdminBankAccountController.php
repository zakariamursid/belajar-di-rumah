<?php namespace App\Http\Controllers;

use crocodicstudio\crudbooster\controllers\CBController;

class AdminBankAccountController extends CBController {


    public function cbInit()
    {
        $this->setTable("bank_account");
        $this->setPermalink("bank_account");
        $this->setPageTitle("Bank Account");

        $this->addSelectTable("User","users_id",["table"=>"users","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addText("Account Name","account_name")->strLimit(150)->maxLength(255);
		$this->addText("Account Number","account_number")->strLimit(150)->maxLength(255);
		$this->addSelectTable("Bank","bank_id",["table"=>"bank","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
		

    }
}
