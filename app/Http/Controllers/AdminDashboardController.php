<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Auth;
use App\Model\ModelCourse;
use App\Model\ModelOrders;
use App\Model\ModelPayments;
use App\Model\ModelAdminWallet;
use App\Model\ModelWallet;
use App\Model\ModelUsers;

class AdminDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = [];
        $data['page_title'] = "Dashboard ".cb()->getAppName();

        $auth =  Auth::user();
        if($auth != null)
        {   

        if($auth->cb_roles_id == 1)
        {
            $data['count_course'] = ModelCourse::count();
            $data['count_mentor'] = ModelCourse::countMentor();
            $data['count_mentor_revenue'] = ModelWallet::countAllDebit();
            $data['count_admin_revenue'] = ModelAdminWallet::countAllDebit();
            $data['course'] = ModelCourse::getLatest();
            $data['user'] = ModelUsers::getLatest();
            $data['enroll'] = ModelOrders::getLatest();

             return view('admin.dashboard', $data);


        }
   
        else if($auth->cb_roles_id == 2)
        {
        $debit = 0; // this is store all sum value so first assign 0
        $kredit = 0; // this is store all sum value so first assign 0


        $data['count_all_course'] = ModelCourse::countBy("mentor_id",Auth::user()->id);
        $data['count_pending_course'] = ModelCourse::countByWithMentorId("status","Pending",Auth::user()->id);
        $data['count_all_enroll'] = ModelOrders::countBy("course.mentor_id",Auth::user()->id);
        $data['count_waiting_payment'] = ModelPayments::countPendingBy("course.mentor_id",Auth::user()->id);
        $balance = ModelWallet::getByUser(Auth::user()->id);
        foreach ($balance as $value) {
            // echo $value->id;
            if($value->type == "DEBIT")
            {
                $debit += $value->balance;             
            }
            else if($value->type == "KREDIT")
            {
                $kredit += $value->balance;             
            }
            
        }   
 
  

        $data['earn'] = ModelWallet::countEarn(Auth::user()->id);

        $data['saldo'] = $debit-$kredit;
        $data['earn_detail'] = ModelWallet::getEarnDetailByUser(Auth::user()->id);


        $data['order'] = ModelOrders::getByWithBuyer("course.mentor_id",Auth::user()->id);
      

                return view('users.dashboard', $data);
        }

        // dd($auth->cb_roles_id);
     

        }
        
        else if($auth->cb_roles_id == 3)
        {
            return redirect('/');
        }


        else {
            return redirect('user/login');
        }




    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
