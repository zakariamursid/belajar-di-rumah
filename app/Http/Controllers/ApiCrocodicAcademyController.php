<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;


use crocodicstudio\crudbooster\exceptions\CBValidationException;

class ApiCrocodicAcademyController extends Controller
{

    public static function postAddUser(Request $request)
    {
		$result = array();
		$validator = Validator::make($request->all(),      
			[
				'name'	 			=> 'required',
				'email'				=> 'required | email',
				'password'				=> 'required',
			]
		);
		if ($validator->fails()) 
		{
			$message = $validator->errors()->all();      
			$result['api_status'] = 0;
			$result['api_message'] = implode(', ',$message);      
			$res = response()->json($result);
			$res->send();
			exit;
        }
        
        $cek = DB::table('users')->where('email',request("email"))->get();

        if(count($cek) > 0)
        {
            $response['api_status'] = 0;
            $response['api_message'] = 'Email Sudah terdaftar';
        }
        else
        {
            $insert = DB::table("users")
                        ->insert([
                            "created_at"=>date("Y-m-d H:i:s"),
                            "name"=> request("name"),
                            "email"=> request("email"),
                            "password"=>Hash::make(request("password")),
                            "cb_roles_id"=> 2,
                        ]);
            
            if($insert == true)
            {
                $response['api_status'] = 1;
                $response['api_message'] = 'Data berhasil diinsert';
            }

        }
            
            return response()->json($response);
    }


    public function postTryLoginCA(Request $request)
    {

		$result = array();
		$validator = Validator::make($request->all(),      
			[
				'email'				=> 'required | email',
			]
		);
		if ($validator->fails()) 
		{
			$message = $validator->errors()->all();      
			$result['api_status'] = 0;
			$result['api_message'] = implode(', ',$message);      
			$res = response()->json($result);
			$res->send();
			exit;
        }

        $cek = DB::table('users')
        ->where('email',request('email'))
        ->first();

        if(!empty($cek))
        {
            $response['api_status'] = 1;
            $response['data'] = $cek;
        }
        else {
            $response['api_status'] = 0;
            $response['data'] = "Not Found";
        }

        return response()->json($response);
    }

    public static function postEditUser(Request $request)
    {

		$result = array();
		$validator = Validator::make($request->all(),      
			[
				'name'				=> 'required',
				'email'				=> 'required | email',
			]
		);
		if ($validator->fails()) 
		{
			$message = $validator->errors()->all();      
			$result['api_status'] = 0;
			$result['api_message'] = implode(', ',$message);      
			$res = response()->json($result);
			$res->send();
			exit;
        }

            $data = [];
            $data['name'] = request('name');
            $data['email'] = request('email');

            if(request('password')) {
                $data['password'] = request('password');
            }
            // if(request()->hasFile('photo')) {
            //     $data['photo'] = cb()->uploadFile('photo', true, 200, 200);
            // }

            $edit = DB::table("users")->where("email", $data['email'])->update($data);

            if($edit == true)
            {
                $response['api_status'] = 1;
                $response['api_message'] = 'Data berhasil diubah';
            }
            else
            {
                $response['api_status'] = 0;
            }

        // $response['count'] = count($cek);

        return response()->json($response);
    }

}
