<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Images;

use Image;

class InterventionController extends Controller
{
  public function getImage()
  {
    $images = Images::all();
    return view('resizeimage', compact('images'));
  }

  public function postImage(Request $request)
  {
    $this->validate($request, [
      'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
    ]);

    $image = $request->file('image');
    $nameImage = $request->file('image')->getClientOriginalName();

    $thumbImage = Image::make($image->getRealPath())->resize(100, 100);
    $thumbPath = public_path() . '/thumbnail_images/' . $nameImage;
    $thumbImage = Image::make($thumbImage)->save($thumbPath);

    $oriPath = public_path() . '/normal_images/' . $nameImage;
    $oriImage = Image::make($image)->save($oriPath);

    $mouseImage = new Images;
    $mouseImage->imgname = $nameImage;
    $mouseImage->normal_imgpath = $oriPath;
    $mouseImage->thumbnail_imgpath = $thumbPath;
    $mouseImage->save();

    return redirect()->back()->with('success', 'Berhasil');
  }
}