<?php

namespace App\Http\Controllers;
use Auth;
use App\Model\ModelUsers;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    $data = [];
            $data['session'] = Auth::user();
            $data['page_title'] = "User";
            $data['users'] = ModelUsers::get();
            // dd($data); 
            
            return view('admin.user',$data); 
            }


    public function disable($id)
    {
        $user = new ModelUsers();
        $user->disabled($id);
        return redirect('/user/user');
    } 

    public function enable($id)
    {
        $user = new ModelUsers();
        $user->setStatus("Aktif");
        $user->changeStatus($id);
        return redirect('/user/user');
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
