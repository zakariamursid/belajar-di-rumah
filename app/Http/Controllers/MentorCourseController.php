<?php namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use crocodicstudio\crudbooster\controllers\CBController;
use crocodicstudio\crudbooster\helpers\CRUDBooster;
use App\Model\ModelCourse;
use App\Model\ModelCoupon;
use App\Model\ModelSyllabus;
use App\Model\ModelSyllabusItem;
use App\Model\ModelTopic;
use App\CourseSyllabus;


class MentorCourseController extends CBController {


    public function cbInit()
    {
        $this->setTable("course");
        $this->setPermalink("course");
        $this->setPageTitle("Course");

        $this->addText("Title","title")->strLimit(150)->maxLength(255);
		$this->addSelectOption("Level","level")->options(['Beginner'=>'Beginner','Intermediate'=>'Intermediate','Expert'=>'Expert']);
		$this->addSelectTable("Topic","topic_id",["table"=>"topic","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addTextArea("Short Desc","short_desc")->strLimit(100);
		$this->addWysiwyg("Desc","desc")->strLimit(150);
		$this->addWysiwyg("Preparation","preparation")->strLimit(150);
		$this->addWysiwyg("Include","include")->strLimit(150);
		$this->addMoney("Price","price");
		$this->addSelectTable("Mentor","mentor_id",["table"=>"users","value_option"=>"id","display_option"=>"name","sql_condition"=>""]);
		$this->addDatetime("Created At","created_at")->required(false)->showAdd(false)->showEdit(false);
		$this->addDatetime("Updated At","updated_at")->required(false)->showAdd(false)->showEdit(false);
    }
    

	
	public function getAdd()
	{
        $data = [];
        $data['page_title'] = "Add Course";
		return view('course.add-step1', $data);
	}


	public function getAddStep1()
	{
    
    $data['topics'] = ModelTopic::get();
    $data['levels'] = [
        'Beginner','Medium','Expert'
    ];

    $data['url'] = 'mentor/course/add-step2';


    $data['page_title'] = 'Add Course';

    return view('mentor.course.add1', $data);  
	}



    public function postAdd(Request $request)
    {
            $save = new ModelCourse;
            $save->SetTitle($request->title);
            $save->setLevel($request->level);
            $save->setTopic_id($request->topic_id);
            $save->setShort_desc($request->short_desc);
            $save->setDesc($request->desc);
            $save->setMentor_id($request->mentor_id);
            $save->add();

            return redirect("/mentor/course/");
    }

	public function pricing($id)
	{

        $data['status'] = 
        ([
            array('status' => "Aktif"),
            array('status' => "Tidak Aktif"),
        ]);

            $session =   cb()->session()->id();
            
            if($session == null)
            {
                return redirect("/user/login");

            }
            else
            {
                $data['page_title'] = 'Coupon';
                $data['backlink'] = "/user/course";
                $data['url'] = 'Add Course';
                $data['course'] = ModelCourse::getById($id);
                $data['coupons'] = ModelCoupon::getBy("coupon.course_id",$id);

                return view('mentor.course.pricing', $data);  
            }
	}

	public function content($id)
	{

            $session =   cb()->session()->id();
            
            if($session == null)
            {
                return redirect("/user/login");

            }
            else
            {
                $data['page_title'] = 'Content';
                $data['backlink'] = "/user/course";
                $data['course'] = ModelCourse::getById($id);
                $data['coupons'] = ModelCoupon::getCouponByCourseId($id);

                $data['test'] = CourseSyllabus::with('syllabusitem')->where('course_id',$id)->get();

                return view('mentor.course.content', $data);  
            }
	}



}
