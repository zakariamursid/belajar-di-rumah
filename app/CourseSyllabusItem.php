<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseSyllabusItem extends Model
{
        protected $table = 'course_syllabus_item';

        
    public function syllabus()
    { 
        return $this->belongsTo('App\CourseSyllabusItem','id');
    }
}


