<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseSyllabus extends Model
{
    protected $table = 'course_syllabus';


    public function syllabusitem()
    { 
        return $this->hasMany('App\CourseSyllabusItem','course_syllabus_id');
    }
}
